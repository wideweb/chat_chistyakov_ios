//
//  CCDefaultContactsListViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 17.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCDefaultContactsListViewController: CCBaseContactsListViewController {

  
  //  MARK: -
  //  MARK: properties
//  private var 
  
  @IBOutlet private weak var tableView: UITableView?
  @IBOutlet private weak var separatorView0: UIView?
  @IBOutlet private weak var separatorView1: UIView?
  
  
  //  MARK: -
  //  MARK: VLC
  override func configureInterface() {
    //  Search bar
    if let textField = self.searchBar.valueForKey("_searchField") as? UITextField {
      textField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [
        NSForegroundColorAttributeName : UIColor(netHex: 0x000000).colorWithAlphaComponent(0.68),
        NSFontAttributeName : UIFont.helveticaNeueCyrRomanFontOfSize(14),
        NSKernAttributeName : -0.28 / 2,
        NSBaselineOffsetAttributeName : -1
        ])
    }
    else { self.searchBar.placeholder = "Search" }
    
  }
  
  
  
  //  MARK: -
  //  MARK: Other
  override func setupNavBar() {
    super.setupNavBar()
    
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.helveticaNeueCyrRomanFontOfSize(18)
    ]
    
  }
  
  
  // - MARK: Override
  override func setupEmptyDataSet() {
    self.tableView?.emptyDataSetSource = self
    self.tableView?.emptyDataSetDelegate = self
  }
  
  
  override func reloadEmptyDataSet() {
    self.tableView?.reloadEmptyDataSet()
  }
  
  
  override func reloadData() {
    self.tableView?.reloadData()
  }
  
  
  override func getCellForUserID(userID: UInt) -> CCContactCellInterface? {
    assertionFailure("Error: Function \(#function) not overrided")
    return nil
  }
  
  
}
