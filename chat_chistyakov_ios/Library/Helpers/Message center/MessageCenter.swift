//
//  MessageCenter.swift
//  EKOTribe
//
//  Created by Сергей Максимук on 29.10.15.
//  Copyright (c) 2015 wide-web. All rights reserved.
//

import UIKit


class MessageCenter: NSObject {
  
  
  
  //  MARK: -
  //  MARK: properties
  static let sharedInstance = MessageCenter()
  
  private var _systemAlert: UIAlertView?
  private var _pushPanel = JKNotificationPanel()
  
  
  //  MARK: -
  //  MARK: methods
  func showWait(message: String) {  }
  func showMessage(message: String) { systemAlert(message: message) }
  func showWarning(message: String) { systemAlert(message: message) }
  func showSuccess(message: String) { systemAlert(message: message) }
  func showError(message: String) { systemAlert("Error", message: message) }
  func showPush(user: String, message: String, tapAction: (()->())? = nil) { pushAlert(user, message: message, tapAction: tapAction) }
  
  func hideAlert() {}
  

}



//  MARK: -
//  MARK: alerts
extension MessageCenter: UIAlertViewDelegate {
  
  
  private func systemAlert(
    title: String = "",
    message: String = "",
    cancelTitle: String = NSLocalizedString("OK", comment: ""),
    cancelBlock: (()->())? = nil,
    okTitle: String? = nil,
    okBlock: (()->())? = nil
  ) {
    dispatch_async(dispatch_get_main_queue(), { () -> Void in
      if let okTitle = okTitle {
        self._systemAlert = UIAlertView(
          title: title,
          message: message,
          delegate: self,
          cancelButtonTitle: cancelTitle,
          otherButtonTitles: okTitle)
      } else {
        self._systemAlert = UIAlertView(
          title: title,
          message: message,
          delegate: self,
          cancelButtonTitle: cancelTitle)
      }
      
      self._systemAlert?.show()
    })

  }
  
  
  func pushAlert(user: String, message: String, tapAction: (()->())? = nil) {
    guard let nc = UIApplication.topViewController()?.navigationController else { return }
    
    self._pushPanel.timeUntilDismiss = 8
    self._pushPanel.enableTabDismiss = true
    if let action = tapAction { self._pushPanel.addPanelDidTabAction(action) }
    
    let customView: UIView = {
      let insets: CGFloat = 4
      let imageWidth: CGFloat = 32
      
      
      let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: 40))
      view.backgroundColor = UIColor.whiteColor()
      
      let label = UILabel(frame: UIEdgeInsetsInsetRect(view.frame, UIEdgeInsets(top: insets, left: imageWidth + 2 * insets, bottom: insets, right: insets)))
      label.numberOfLines = 2
      label.lineBreakMode = NSLineBreakMode.ByTruncatingTail
      
      let text = NSMutableAttributedString()
      text.appendAttributedString( NSAttributedString(string: user, attributes: [
        NSForegroundColorAttributeName : UIColor.blackColor(),
        NSFontAttributeName : UIFont.boldSystemFontOfSize(12)
        ]) )
      text.appendAttributedString(NSAttributedString(string: "\n"))
      text.appendAttributedString( NSAttributedString(string: message, attributes: [
        NSForegroundColorAttributeName : UIColor.blackColor(),
        NSFontAttributeName : UIFont.systemFontOfSize(11)
        ]) )
      
      label.attributedText = text
      view.addSubview(label)
      return view
    }()
    
    self._pushPanel.showNotify(withView: customView, belowNavigation: nc)
    
  }
  
  
}



//  MARK: -
//  MARK: progress
extension MessageCenter {
  
  
  func showLoading(message: String, view: UIView) {
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
      let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
      hud.mode = .Indeterminate
      hud.labelText = message
    }
    
//    hud.show(true)
  }
  
  
  func hideLoading(view: UIView) {
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
      MBProgressHUD.hideHUDForView(view, animated: true)
    }
    
  }
  
  
}