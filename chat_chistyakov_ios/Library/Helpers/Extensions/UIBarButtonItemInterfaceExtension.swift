//
//  UIBarButtonItemInterfaceExtension.swift
//  neurofunc_ios
//
//  Created by Admin on 26.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


extension UIBarButtonItem {

  
  class func backButton(target: AnyObject, action: Selector, imageName: String = "back") -> UIBarButtonItem {
    let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    backButton.setImage(UIImage(named: imageName)?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
    backButton.addTarget(target, action: action, forControlEvents: .TouchUpInside)
    backButton.tintColor = UIColor.whiteColor()
    return UIBarButtonItem(customView: backButton)
  }
  
  
  class func signOutButton(target: AnyObject, action: Selector) -> UIBarButtonItem {
    let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
//    backButton.setImage(UIImage(named: "back")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
    backButton.setTitle("Sign out", forState: .Normal)
    backButton.addTarget(target, action: action, forControlEvents: .TouchUpInside)
    backButton.tintColor = UIColor.whiteColor()
    
//    backButton.titleEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: -2, right: 0)
    backButton.titleLabel?.numberOfLines = 2
    backButton.titleLabel?.font = UIFont.bebasNeueBoldFontOfSize(15)
    backButton.titleLabel?.textAlignment = .Center
    
    return UIBarButtonItem(customView: backButton)
  }
  
  
}