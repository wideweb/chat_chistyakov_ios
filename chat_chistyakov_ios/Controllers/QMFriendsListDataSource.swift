//
//  QMFriendsListDataSource.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

protocol QMFriendsListDataSourceDelegate {
  func didReloadDataSource()
  func didStartLoading()
}

class QMFriendsListDataSource: NSObject {
  
  
  //  MARK: -
  //  MARK: properties
  var delegate: QMFriendsListDataSourceDelegate?
  var contactList: [QBUUser] { return self.isSearching ? filteredFriendList : friendList }
  
  var isSearching = false
  private var isLoading = false
  private var friendList: [QBUUser] = []
  private var filteredFriendList: [QBUUser] = []
  
  
  
  //  MARK: -
  //  MARK: methods
  override init() {
    super.init()
    
    Singletone.sharedInstance.addDelegate(self)
    self.reloadDataSource()
  }
  
  
  deinit {
    Singletone.sharedInstance.removeDelegate(self)
  }
  
  
  func reloadDataSource() {
    self.friendList = Singletone.sharedInstance.contacts
    self.delegate?.didReloadDataSource()
    
  }
  
  
  func userAtIndexPath(indexPath: NSIndexPath) -> QBUUser {
    return self.friendList[indexPath.row]
  }
  
  
  func importContacts(completion: ([QBUUser], NSError?)->()) {
    QMAPI.instance().importFriendsFromAddressBook { (users, error) -> () in
      self.reloadDataSource()
      completion(users, error)
    }
    
  }
  
  
}



//  MARK: -
//  MARK: UISearchDisplayDelegate
extension QMFriendsListDataSource: UISearchBarDelegate {
  
  
  func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
    if let _ = self.delegate as? CCContactsListViewController { searchBar.superview?.alpha = 1 }
    return true
  }
  
  
  func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
    if let _ = self.delegate as? CCContactsListViewController { searchBar.superview?.alpha = 0.5 }
    return true
  }
  
  
  func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
    if searchText == "" {
      self.isSearching = false
      self.filteredFriendList = self.friendList }
    else {
      self.isSearching = true
      self.filteredFriendList = self.friendList.filter( {$0.fullName?.lowercaseString.containsString(searchText.lowercaseString) == true} ) }
    
    self.delegate?.didReloadDataSource()
  }
  
  
  func searchBarSearchButtonClicked(searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
  
  
}



//  MARK: -
//  MARK: SingletonDelegate
extension QMFriendsListDataSource: SingletonDelegate {
  
  
  func contactsDidUpdate() {
    self.reloadDataSource()
  }
  
  
  func dialogsDidUpdate() {
    self.reloadDataSource()
  }
  
  
}