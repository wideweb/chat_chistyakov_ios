//
//  Singletone.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 04.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


protocol SingletonDelegate: NSObjectProtocol {
  func contactsDidUpdate()
  func dialogsDidUpdate()
}


class Singletone: NSObject {
  
  
  static let sharedInstance = Singletone()
  
  var delegates: [SingletonDelegate] = []
  func addDelegate(newDelegate: SingletonDelegate) {
    if self.delegates.filter({ $0 === newDelegate }).count == 0 {
      self.delegates.append(newDelegate)
    }
  }
  func removeDelegate(oldDelegate: SingletonDelegate) {
    guard let index = self.delegates.indexOf({ $0 === oldDelegate }) else { return }
    self.delegates.removeAtIndex(index)
  }
  
  
  //  MARK: -
  //  MARK: User data
  func setupData() {
    QMAPI.instance().contactListService.addDelegate(self)
    QMAPI.instance().chatService.addDelegate(self)
    QMAPI.instance().usersService.addDelegate(self)
    QBChat.instance().addDelegate(self)
    
    
    self.loadCachedContacts()
    self.loadCachedDialogs()
    
    self.loadContacts()
    self.loadDialogs()
    
  }
  
  
  
  //  MARK: -
  //  MARK: Contacts
  var contacts: [QBUUser] = [] {
    didSet { self.delegates.map({ $0.contactsDidUpdate() }) }
  }
  
  
  func loadCachedContacts() {
    let contacts = QMAPI.instance().syncGetContacts().sort({ $0.0.login?.lowercaseString.compare($0.1.login?.lowercaseString ?? "") == NSComparisonResult.OrderedAscending })
    if contacts.count != 0 { self.contacts = contacts }
//    
//    QMAPI().asyncGetContacts { (users, error) in
//      if users.count != 0 { self.contacts = QMAPI.instance().syncGetContacts().sort({ $0.0.login?.lowercaseString.compare($0.1.login?.lowercaseString ?? "") == NSComparisonResult.OrderedAscending }) }
//    }
    
  }
  
  
  func loadContacts() {
    QMAPI().asyncGetContacts { (users, error) in
      if let _ = error {}
      else { self.loadCachedContacts() }
//      if users.count != 0 { self.contacts = users }
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: Dialogs
  var dialogs: [QBChatDialog] = [] { didSet { self.delegates.map({ $0.dialogsDidUpdate() }) } }
  
  
  func loadCachedDialogs() {
    self.dialogs = QMAPI.instance().dialogs()
  }
  
  
  func loadDialogs() {
    QMAPI.instance().safeDialogs { (chatDialogs, error) -> () in
      self.loadCachedDialogs()
    }
  }
  
  
}



//  MARK: -
//  MARK: QMContactListServiceDelegate
extension Singletone: QMContactListServiceDelegate, QMUsersServiceDelegate {
  
  
  func contactListServiceDidLoadCache() {
    NSTimer.scheduledTimerWithTimeInterval(15*60, target: QMAPI.instance(), selector: #selector(QMAPI.update(_:)), userInfo: nil, repeats: true)
  }
  
  
  func contactListService(contactListService: QMContactListService!, contactListDidChange contactList: QBContactList!) {
    self.loadCachedContacts()
  }
  
  
  func contactListService(contactListService: QMContactListService!, didReceiveContactItemActivity userID: UInt, isOnline: Bool, status: String!) {
    self.loadCachedContacts()
  }
  
  
  func usersService(usersService: QMUsersService!, didAddUsers user: [QBUUser]!) {
    self.loadCachedContacts()
  }
  
  
}



//  MARK: -
//  MARK: QMChatServiceDelegate
extension Singletone: QMChatServiceDelegate, QMChatConnectionDelegate {
  
  
  func chatService(chatService: QMChatService!, didReceiveNotificationMessage message: QBChatMessage!, createDialog dialog: QBChatDialog!) {
    self.loadCachedDialogs()
  }
  
  
  func chatService(chatService: QMChatService!, didAddMessageToMemoryStorage message: QBChatMessage!, forDialogID dialogID: String!) {
    self.loadCachedDialogs()
  }
  
  
}



// - MARK: QMChatDelegate
extension Singletone: QBChatDelegate {

  
  func chatDidSetActivePrivacyListWithName(name: String) {
    self.loadCachedContacts()
  }
  
  
}