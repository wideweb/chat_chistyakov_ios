//
//  CCContactTableViewCell.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 10.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit


class CCContactTableViewCell: UITableViewCell {

  
  
  private enum Mode {
    case Normal
    case Swipped
    case Flipped
  }
  
  
  
  //  MARK: -
  //  MARK: properties
  var delegate: CCContactCellDelegate?
  
  override var bounds: CGRect { didSet { self.updateHorizontalBar() } }
  
  
  private var user: QBUUser!
  private var mode: Mode = .Normal
  
  @IBOutlet private weak var nameLabel: UILabel!
  @IBOutlet private weak var statusLabel: UILabel!
  @IBOutlet private weak var avatarView: UIImageView!
  @IBOutlet private weak var mainBarView: UIView!
  @IBOutlet private weak var newMessageView: UIImageView!
  @IBOutlet private weak var unreadMessageCountLabel: UILabel!;
  
  @IBOutlet private weak var chatButton: UIButton!
  @IBOutlet private weak var callButton: UIButton!
  @IBOutlet private weak var videoChatButton: UIButton!
  
  @IBOutlet private weak var flipView: UIView!
  @IBOutlet private weak var editButton: UIButton!
  @IBOutlet private weak var blockButton: UIButton!
  @IBOutlet private weak var removeButton: UIButton!
  
  @IBOutlet private weak var horizontalButtonsBarWidthConstraint: NSLayoutConstraint!
  @IBOutlet private weak var horizontalButtonsBarLeadingConstraint: NSLayoutConstraint!
  
  @IBOutlet private weak var blockedUserView: UIView!
  
  
  
  //  MARK: -
  //  MARK: methods
  func setContact(contact: QBUUser) {
    self.resetCell()
    self.user = contact
    let userID = contact.ID
    
    nameLabel.text = contact.fullName ?? contact.login
    
    let avatarPlaceholder = UIImage(named: "contact_placeholder")
    avatarView.image = avatarPlaceholder
    avatarView.contentMode = .Bottom
    
    if let urlString = QMAPI.instance().publicImageUrlWithID(contact.blobID), let url = NSURL(string: urlString) {
      self.avatarView.sd_setImageWithURL(url, placeholderImage: avatarPlaceholder, completed: { (image: UIImage?, _, _, responseUrl: NSURL?) in
        if urlString == responseUrl?.absoluteString && image != nil {
          self.avatarView.contentMode = .ScaleAspectFit
          self.avatarView.image = image
        }
        else {
          self.avatarView.image = avatarPlaceholder
          self.avatarView.contentMode = .Bottom
        }
      })
    }
    
    if let chat = QMAPI.instance().privateDialogWithUser(userID)
      where chat.unreadMessagesCount > 0 {  //  Has unreaded messages
      self.newMessageView.hidden = false
      self.unreadMessageCountLabel.hidden = false
      self.unreadMessageCountLabel.text = "\(min(chat.unreadMessagesCount, 99))"
      
      if let lastMessage = chat.lastMessageText { //  Last unread message text
        statusLabel.text = lastMessage
      }
    }
      
    else {
      self.newMessageView.hidden = true
      self.unreadMessageCountLabel.hidden = true
      
      self.setUserStatus(QMAPI.instance().isOnlineUserWithID(userID) ? "Online" : "Offline")
    }

    let blocked = QMAPI.instance().isUserInBlockList(user.ID)
    self.blockedUserView.hidden = !blocked
    
  }
  
  
  func setUserStatus(status: String) {
    newMessageView.hidden = true
    self.statusLabel.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.45)
    
    statusLabel.text = status
    
  }
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    selectionStyle = .None
    
    self.configureInterface()
    self.setupGestures()
    self.updateButtons()
    
    (self.avatarView as? CCAvatarImageView)?.postUpdateBlock = { _ in
      self.blockedUserView.superview?.bringSubviewToFront(self.blockedUserView) }
    
  }
  

  func configureInterface() {
    //  TODO: configure cell
    self.newMessageView.hidden = true
    
    self.nameLabel.font = UIFont.helveticaNeueCyrLightFontOfSize(18)
    self.statusLabel.font = UIFont.helveticaNeueCyrRomanFontOfSize(10)
    self.unreadMessageCountLabel.font = UIFont.helveticaNeueCyrLightFontOfSize(15)
    
    self.nameLabel.textColor = UIColor.whiteColor()
    self.statusLabel.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.45)
    self.unreadMessageCountLabel.textColor = UIColor.whiteColor()
    
    let buttonColor = UIColor.whiteColor()
    self.chatButton.tintColor = buttonColor
    self.videoChatButton.tintColor = buttonColor
    self.callButton.tintColor = buttonColor
    
    self.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.06)
    
  }
  
  
  func updateHorizontalBar() {
    self.horizontalButtonsBarWidthConstraint.constant = self.mainBarView.bounds.size.width
  }
  
  
  func updateButtons() {
    self.blockButton.addTarget(self, action: #selector(CCContactTableViewCell.blockTapped(_:)), forControlEvents: .TouchUpInside)
    
    self.chatButton.enabled = true
    self.callButton.enabled = false
    self.videoChatButton.enabled = false
    
    self.editButton.enabled = false
    self.blockButton.enabled = true
    self.removeButton.enabled = true
  }
  
  
  
  //  MARK: -
  //  MARK: Actions
  @IBAction func chatTapped(sender: AnyObject) {
    self.delegate?.didTapChatWithUser(self.user)

  }
  
  
  @IBAction func removeTapped(sender: AnyObject) {
    self.delegate?.didTapRemoveWithUser(self.user)
    
  }
  
  
  @IBAction func blockTapped(sender: AnyObject) {
    self.delegate?.didTapBlockWithUser(self.user)
    
  }
  
  
  
  //  MARK: -
  //  MARK: Animations
  func resetCell(animation: Bool = false) {
    self.closeHorizontalButtons(false)
    self.closeFlipButtons(false)
  }
  
  
  func openHorizontalButtons() {
    if self.mode == .Swipped { return }
    self.delegate?.willShowEditOptionsForCell(self)
    
    self.updateHorizontalBar()
    
    self.layoutIfNeeded()
    self.horizontalButtonsBarLeadingConstraint.constant = -self.mainBarView.bounds.size.width
    
    let oldMode = self.mode
    self.mode = .Swipped
    UIView.animateWithDuration(0.4, animations: {
      switch oldMode {
      case .Flipped: self.flipView.alpha = 0
      case .Normal: self.mainBarView.alpha = 0
      case .Swipped: break
      }
      self.layoutIfNeeded()
    }) { (finished: Bool) in
      if !finished { return }
      switch oldMode {
      case .Flipped: self.resetFlipButtons(); self.mainBarView.alpha = 0
      case .Normal: break
      case .Swipped: break
      }
    }
    
  }
  
  
  func closeHorizontalButtons(animation: Bool) {
    if self.mode != .Swipped { return }
    self.mode = .Normal
    
    self.layoutIfNeeded()
    self.horizontalButtonsBarLeadingConstraint.constant = 0
    
    if animation {
    UIView.beginAnimations(nil, context: nil)
    UIView.setAnimationDuration(0.4)
    }
    
    self.mainBarView.alpha = 1
    self.layoutIfNeeded()
    
    if animation {
    UIView.commitAnimations()
    }
    
  }
  
  
  func toggleFlip() {
    switch self.mode {
    case .Normal: self.openFlipButtons(true)
    case .Flipped: self.closeFlipButtons(true)
    default: break
    }
    
  }
  
  
  func openFlipButtons(animation: Bool) {
    if self.mode != .Normal && animation { return }
    self.delegate?.willShowChatOptionsForCell(self)
    
    self.mode = .Flipped
    
    self.flipView.transform = CGAffineTransformMakeScale(1, -1)
    self.mainBarView.transform = CGAffineTransformIdentity
    self.layoutIfNeeded()
    
    if animation {
      UIView.beginAnimations(nil, context: nil)
      UIView.setAnimationDuration(0.4)
    }
    
    self.mainBarView.transform = CGAffineTransformMakeScale(1, -1)
    self.flipView.transform = CGAffineTransformIdentity
    self.mainBarView.alpha = 0
    self.flipView.alpha = 1
    self.layoutIfNeeded()
    
    if animation {
      UIView.commitAnimations()
    }
    
  }
  
  
  func closeFlipButtons(animation: Bool) {
    if self.mode != .Flipped && animation { return }
    self.mode = .Normal
    
    self.flipView.transform = CGAffineTransformIdentity
    self.mainBarView.transform = CGAffineTransformMakeScale(1, -1)
    self.layoutIfNeeded()
    
    if animation {
      UIView.beginAnimations(nil, context: nil)
      UIView.setAnimationDuration(0.4)
    }
    
    self.mainBarView.transform = CGAffineTransformIdentity
    self.flipView.transform = CGAffineTransformMakeScale(1, -1)
    self.mainBarView.alpha = 1
    self.flipView.alpha = 0
    self.layoutIfNeeded()
    
    if animation {
      UIView.commitAnimations()
    }
    
  }
  
  
  func resetFlipButtons() {
    self.mainBarView.transform = CGAffineTransformIdentity
    self.flipView.transform = CGAffineTransformMakeScale(1, -1)
    self.flipView.alpha = 0
    self.mainBarView.alpha = 1

  }
  
  
  
  //  MARK: -
  //  MARK: Gestures
  func setupGestures() {
    let leftSwipeGestureRecognize = UISwipeGestureRecognizer(target: self, action: #selector(handleLeftSwipe))
    leftSwipeGestureRecognize.direction = .Left
    self.addGestureRecognizer(leftSwipeGestureRecognize)
    
    let rightSwipeGestureRecognize = UISwipeGestureRecognizer(target: self, action: #selector(handleRightSwipe))
    rightSwipeGestureRecognize.direction = .Right
    self.addGestureRecognizer(rightSwipeGestureRecognize)
    
  }
  
  
  func handleLeftSwipe(recognizer: UISwipeGestureRecognizer) {
    self.openHorizontalButtons()
  }
  
  
  func handleRightSwipe(recognizer: UISwipeGestureRecognizer) {
    self.closeHorizontalButtons(true)
  }
  

}



extension CCContactTableViewCell: CCContactCellInterface {
  
  
  func setUser(user: QBUUser) {
    self.setContact(user)
  }
  
  
  func getUserID() -> UInt? {
    return self.user.externalUserID
  }
  
  
  func openEditOptions() {
    self.openFlipButtons(true)
  }
  
  
  func closeEditOptions() {
    self.closeFlipButtons(true)
  }
  
  
  func openChatOptions() {
    self.openHorizontalButtons()
  }
  
  
  func closeChatOptions() {
    self.closeHorizontalButtons(true)
  }
  
  
  func toggleChatOptions() {
    self.toggleFlip()
  }
  
  
}