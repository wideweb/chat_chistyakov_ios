//
//  CCStartViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 15.06.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCStartViewController: UIViewController {

  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if let phone = QMAPI.instance().savedLogin {
      //  autologin
      QMAPI.instance().autoLogIn { (error: NSError?) in self.signInCompletion(error) }
    }
    else {
      MessageCenter.sharedInstance.showLoading("Signing in ...", view: self.view)
      QMAPI.instance().logout { () -> () in
        QMAPI.instance().loginWithTwitterDigits("") { (error: NSError?) -> () in
          MessageCenter.sharedInstance.hideLoading(self.view)
          if let user = QMAPI.instance().currentUser() {
            if let login = user.login { QMAPI.instance().saveUser(login) }
            
            if let _ = user.fullName {
              self.signInCompletion(error)
            } else {
              self.performSegue(Segue.segueSignInToSignUp, sender: user)
            }
          } else {
            self.signInCompletion(error)
          }
        }
      }
    }
  
  }

  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    self.navigationController?.navigationBarHidden = true
  }
  
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    
    self.navigationController?.navigationBarHidden = false
  }
  
  
  func signInCompletion(error: NSError?) {
    let message = error?.localizedDescription ?? "SUCCESS"
    NSLog("LOGIN _\(message)")
    
    
    if let error = error {
      MessageCenter.sharedInstance.showError(error.localizedDescription)
    } else {
      self.performSegue(Segue.segueLoginToWSList)
    }
    
  }
  
  
  // - MARK: Navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue == Segue.segueSignInToSignUp {
      guard let user = sender as? QBUUser else { return }
      let controller = segue.destinationViewController as! CCRegisterViewController
      controller.user = user
    }
    
  }
  

}
