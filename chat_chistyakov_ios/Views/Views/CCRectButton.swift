//
//  CCRectButton.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 10.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

@IBDesignable
class CCRectButton: UIButton {

  
  @IBInspectable var fontSize: CGFloat = 36 { didSet { self.updateView() } }
  @IBInspectable var color: UIColor = UIColor.whiteColor() { didSet { self.updateView() } }
  @IBInspectable var borderColor: UIColor = UIColor.whiteColor() { didSet { self.updateView() } }
  @IBInspectable var bgColor: UIColor = UIColor.clearColor() 
  @IBInspectable var highlightedBgColor: UIColor = UIColor.clearColor()
  
  
  override var enabled: Bool { didSet { self.alpha = self.enabled ? 1 : 0.2 } }
  
  
  func updateView() {
    let borderColor = self.borderColor
    let bgColor = UIColor.clearColor()
    let titleColor = self.color
    
    
    self.layer.borderWidth = 1
    self.layer.borderColor = borderColor.CGColor
    
    self.layer.cornerRadius = 2
    
    self.backgroundColor = bgColor
    
    self.setTitleColor(titleColor, forState: .Normal)
    self.setTitleColor(titleColor, forState: .Selected)
    self.setTitleColor(titleColor, forState: .Highlighted)
    
    self.titleLabel?.font = UIFont.bebasNeueLightFontOfSize(self.fontSize)
    
    self.imageEdgeInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
    self.imageView?.contentMode = .ScaleAspectFit
    
  }
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.updateView()
  }

  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    self.updateView()
  }
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    self.updateView()
  }
  
  
  override func titleRectForContentRect(contentRect: CGRect) -> CGRect {
    let rect = super.titleRectForContentRect(contentRect)
    return UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0))
  }
  
  
  
  
  override var highlighted: Bool {
    didSet {
      self.backgroundColor = self.highlighted ? self.highlightedBgColor : self.bgColor
    }
  }
  
  
//  override func prepareForInterfaceBuilder() {
//    super.prepareForInterfaceBuilder()
//    
//    updateView()
//  }
  

}
