//
//  CCAvatarImageView.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 10.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCAvatarImageView: UIImageView {

  
  var postUpdateBlock: (()->())?
  
  
  private var shadowView: UIView = {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
    view.backgroundColor = UIColor.clearColor()
    
    view.layer.cornerRadius = view.bounds.size.width / 2
    
    view.layer.shadowOffset = CGSize(width: 0, height: 1.2)
    view.layer.shadowRadius = 1
    view.layer.shadowOpacity = 0.3
    view.layer.shadowColor = UIColor.blackColor().CGColor
    
    return view
  }()
  
  func updateView() {
    self.layer.borderWidth = 1.5
    self.layer.borderColor = UIColor.whiteColor().CGColor
    
    self.layer.cornerRadius = self.bounds.size.width / 2
    
//    self.layer.shadowOffset = CGSize(width: 0, height: 1.2)
//    self.layer.shadowRadius = 1
//    self.layer.shadowOpacity = 0.3
//    self.layer.shadowColor = UIColor.blackColor().CGColor
    
    self.contentMode = .ScaleAspectFit
    self.layer.masksToBounds = true
    
    self.shadowView.frame = self.frame
    self.shadowView.layer.cornerRadius = self.bounds.size.height / 2
    
    self.superview?.addSubview(self.shadowView)
    self.superview?.bringSubviewToFront(self)
    
    self.postUpdateBlock?()
    
  }
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.updateView()
  }

  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    self.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.3)
    
    self.updateView()
  }
  
  
  override func layoutIfNeeded() {
    super.layoutIfNeeded()
    
    self.updateView()
  }
  
  
  override var bounds: CGRect {
    didSet { self.updateView() }
  }
  

}
