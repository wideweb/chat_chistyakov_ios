//
//  QuickBloxExtensions.m
//  chat_chistyakov_ios
//
//  Created by Admin on 02.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

#import "QuickBloxExtensions.h"

@implementation QMUsersService (CC)


- (BFTask *)getUsersWithPhones:(NSArray *)phones
{
  QBGeneralResponsePage *pageResponse =
  [QBGeneralResponsePage responsePageWithCurrentPage:1 perPage:phones.count < 100 ? phones.count : 100];
  
  return [self getUsersWithPhones:phones page:pageResponse];
}


- (BFTask *)getUsersWithPhones:(NSArray *)phones page:(QBGeneralResponsePage *)page
{
  NSParameterAssert(phones);
  NSParameterAssert(page);
  
  __weak __typeof(self)weakSelf = self;
  return [[self loadFromCache] continueWithBlock:^id(BFTask *task) {
    __typeof(weakSelf)strongSelf = weakSelf;
    
    NSDictionary* searchInfo = [strongSelf.usersMemoryStorage usersByExcludingLogins:phones];
    NSArray* foundUsers = searchInfo[QMUsersSearchKey.foundObjects];
    if ([searchInfo[QMUsersSearchKey.notFoundSearchValues] count] == 0) {
      return [BFTask taskWithResult:foundUsers];
    }
    
    BFTaskCompletionSource* source = [BFTaskCompletionSource taskCompletionSource];
    
    [QBRequest usersWithPhoneNumbers:searchInfo[QMUsersSearchKey.notFoundSearchValues]
                                page:page
                        successBlock:^(QBResponse *response, QBGeneralResponsePage *page, NSArray *users) {
                          [strongSelf.usersMemoryStorage addUsers:users];
                          
                          [source setResult:[foundUsers arrayByAddingObjectsFromArray:users]];
                        } errorBlock:^(QBResponse *response) {
                          [source setError:response.error.error];
                        }];
    
    return source.task;
  }];
}


@end
