//
//  QuickBloxExtensions.h
//  chat_chistyakov_ios
//
//  Created by Admin on 02.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QMServices.h>

@interface QMUsersService (CC)

- (BFTask *)getUsersWithPhones:(NSArray *)phones;

@end