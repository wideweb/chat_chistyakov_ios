//
//  CCIncomingChatCell.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 18.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCIncomingChatCell: CCChatCell {

  
  override func customizeView() {
    super.customizeView()
    
    let bgColor = UIColor.whiteColor().colorWithAlphaComponent(0.8)
    self.containerView?.bgColor = bgColor
    self.containerView.highlightColor = bgColor.colorWithAlphaComponent(0.6)
  }
  

}
