//
//  CCCircleButton.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 16.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCCircleButton: UIButton {

  
  func updateView() {
    self.titleLabel?.numberOfLines = 0
    self.titleLabel?.textAlignment = .Center
    
    
    
    self.tintColor = UIColor.whiteColor()
    
    self.layer.borderWidth = 1
    self.layer.borderColor = UIColor.whiteColor().CGColor
    self.layer.cornerRadius = self.frame.size.height / 2
  }
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    self.updateView()
  }
  
  
  override var bounds: CGRect { didSet { self.updateView() } }
  
  
  override func setImage(image: UIImage?, forState state: UIControlState) {
    super.setImage(image, forState: state)
    self.layoutIfNeeded()
    
    if (self.attributedTitleForState(state) != nil && self.attributedTitleForState(state)?.string != "") || (self.titleForState(state) != nil && self.titleForState(state) != "") {
      guard let imageSize = image?.size else { return }
      
      self.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageSize.width, bottom: 0, right: 0)
      self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -(self.titleLabel?.frame.size.width ?? 0))
      
      self.imageView?.alpha = 0.5
    } else {
      self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
      self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
      
      self.imageView?.alpha = 1
    }
    
  }
  

}
