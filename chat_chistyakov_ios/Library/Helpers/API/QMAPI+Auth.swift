//
//  QBAPI+Auth.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation
import Crashlytics


typealias QMAPIAuthEmptyCompletion = ()->()
typealias QMAPIAuthSimpleCompletion = (NSError?)->()


extension QMAPI {
  
  
  func signUp(user: QBUUser, completion: QMAPIAuthSimpleCompletion?) {
    let error = NSError(domain: "auth", code: -107, userInfo: [
      NSLocalizedDescriptionKey : "Not available now. Use authentification screen"
      ])
    completion?(error)
    return
    
    CCLog.log("\(#file) \(#function)  (user: \(user))")
    
    var request: QBRequest?
    request = self.authService.signUpAndLoginWithUser(user) { (response: QBResponse!, userProfile: QBUUser!) -> Void in
      CCLog.log("\(#file) \(#function)  ...authService.signUpAndLoginWithUser... response: \(response.logProperties())")
      
      if response.success {
        //  Additional actions here...
        self.connectChat({ (error) -> () in
          CCLog.log("\(#file) \(#function)  ...connectChat... error: \(error)")
          
          if let error = error {
            self.handleErrorRequest(request, response: response, localError: error)
            
            CCLog.log("\(#file) \(#function)  ...authService.signUpAndLoginWithUser... completion: \(error)")
            completion?(error) }
          else {
            
            CCLog.log("\(#file) \(#function)  ...authService.signUpAndLoginWithUser... success")
            self.successLogin(completion) }
          
        })
      }
      else {
        self.handleErrorRequest(request, response: response)
        
        let error = self.customHandleErrorResponse(response)
        CCLog.log("\(#file) \(#function)  ...authService.signUpAndLoginWithUser... completion: \(error)")
        completion?(error)
      }
    }
    CCLog.log("\(#file) \(#function)  ...authService.signUpAndLoginWithUser... request: \(request?.logProperties() ?? "")")
    
  }
  
  
  func login(login: String, password: String, remember: Bool, completion: QMAPIAuthSimpleCompletion?) {
    let loginUser = QBUUser()
    
    loginUser.login = login
    loginUser.password = password
    
    CCLog.log("\(#file) \(#function)  (user: \(loginUser))")
    
    var request: QBRequest?
    request = self.authService.logInWithUser(loginUser) { (response: QBResponse!, userProfile: QBUUser!) -> Void in
      CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... response: \(response.logProperties())")
      
      if response.success {
        //  Additional actions here...
        self.connectChat({ (error) -> () in
          CCLog.log("\(#file) \(#function)  ...connectChat... error: \(error)")
          
          if let error = error {
            self.handleErrorRequest(request, response: response, localError: error)
            
            CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... completion: \(error)")
            completion?(error) }
          else {
            CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... success")
//            if remember { self.saveUser(login, password: password) }
            
            
            self.successLogin(completion)
            
          }
          
        })
//        self.successLogin(completion)
      }
      else {
        self.handleErrorRequest(request, response: response)
        
        let error = self.customHandleErrorResponse(response)
        CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... completion: \(error)")
        completion?(error)
      }
    }
    CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... request: \(request?.logProperties() ?? "")")
    
  }
  
  
  func logout(completion: QMAPIAuthEmptyCompletion?) {
    CCLog.log("\(#file) \(#function)")
    
    self.logoutWithCompletion { () -> Void in
      //  Additional actions here...
      self.clearUser()
      Digits.sharedInstance().logOut()
      completion?()
    }
    
  }
  
  
  internal func successLogin(completion: QMAPIAuthSimpleCompletion?) {
//    self.updatePrivacyList()
    self.setupPrivacyList()
//    self.registerForRemoteNotifications()
    self.importContacts(completion)

//    let kImportOnceKey = "kImportOnceKey"
//    if NSUserDefaults.standardUserDefaults().objectForKey(kImportOnceKey) == nil {
//      NSUserDefaults.standardUserDefaults().setBool(true, forKey: kImportOnceKey)
//      NSUserDefaults.standardUserDefaults().synchronize()
//      self.importContacts(completion)
//    } else {
//      completion?(nil)
//    }
    
  }
  
  
  func customHandleErrorResponse(response: QBResponse) -> NSError? {
    var errorString = response.error?.error?.localizedDescription ?? ""
    
    var reasonsString = ""
    
    switch response.status {
    case .UnAuthorized: reasonsString += "- Invalid user login or password."
    default: if let reasons = response.error?.reasonsText() where reasons != "" { reasonsString += reasons }
    }
    
    if reasonsString != "" { errorString += "\nReasons:\n" + reasonsString }
    
    return NSError(domain: "qb", code: -57, userInfo: [
      NSLocalizedDescriptionKey : errorString
      ])
  }
  
  
  func test() {
    let user = QBUUser()
    user.login = "test"
    user.password = "testtest"
    self.authService.logInWithUser(user) { (response: QBResponse!, usr: QBUUser!) in
      if (!self.chatService.serviceManager.isAuthorized()) {
        let error = NSError(domain: "com.q-municate.chatservice", code: -1000, userInfo: [NSLocalizedRecoverySuggestionErrorKey:"You are not authorized in REST."])
        NSLog("\(error)")
        return;
      }
      
      if (QBChat.instance().isConnected()) {
        
      }
      else {
        QBSettings.setAutoReconnectEnabled(true)
        
        let user = QBUUser()
        user.login = "test1000"
        user.password = "testtest"
        QBChat.instance().connectWithUser(user) { (error: NSError?) in
          NSLog("\(error)")
        }
      }
    }
    
  }
  
  
  func handleErrorRequest(request: QBRequest?, response: QBResponse?, localError: NSError? = nil) {
    var lightUserInfo: [String : String] = [:]
    lightUserInfo += [
      "ios_version" : UIDevice.currentDevice().systemVersion
    ]
    lightUserInfo += [
      "params" : request?.paramsDescription() ?? "",
      "error" : response?.error?.error?.localizedDescription ?? "",
      "reasons" : response?.error?.reasonsText() ?? "",
    ]
    if let localError = localError {
      lightUserInfo += [
        "_error_description" : localError.localizedDescription,
        "_error_fail_reason" : localError.localizedFailureReason ?? "",
        "_error_recovery" : localError.localizedRecoverySuggestion ?? "",
        "_error_options" : localError.localizedRecoveryOptions?.description ?? ""
      ]
      
    }
    
    Flurry.logEvent("Error", withParameters: lightUserInfo)
    
  }
  
  
  //  MARK: -
  //  MARK: autologin
  private var kRememberMeKey: String { return "rememberMeKey" }
  private var kLoginKey: String { return "loginKey" }
  private var kPasswordKey: String { return "passwordKey" }
  
  
  var savedLogin: String? {
    return NSUserDefaults.standardUserDefaults().objectForKey(kLoginKey) as? String
  }
  
  
  func saveUser(login: String) {
    CCLog.log("\(#file) \(#function)")
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    defaults.setBool(true, forKey: kRememberMeKey)
    defaults.setObject(login, forKey: kLoginKey)
//    defaults.setObject(password, forKey: kPasswordKey)
    
    defaults.synchronize()
  }
  
  
  func clearUser() {
    CCLog.log("\(#file) \(#function)")
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    defaults.setBool(false, forKey: kRememberMeKey)
    defaults.removeObjectForKey(kLoginKey)
//    defaults.removeObjectForKey(kPasswordKey)
    
    defaults.synchronize()
  }
  
  
  func autoLogIn(completion: QMAPIAuthSimpleCompletion?) {
    CCLog.log("\(#file) \(#function)")
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    let rememberMe = defaults.boolForKey(kRememberMeKey)
    
    if rememberMe {
      guard let _ = self.savedLogin else { return }
      
      completion?(nil)
      self.loginIfReachable()
      
    }
  
  }
  
  
  func loginIfReachable() {
    if reachability.isReachable() {
//      let defaults = NSUserDefaults.standardUserDefaults()
      guard let phone = self.savedLogin else { return }
//      guard let password = defaults.objectForKey(kPasswordKey) as? String else { return }
      
      self.loginWithTwitterDigits(phone, completion: { (error: NSError?) in
        if let _ = error {}
        else { NSNotificationCenter.defaultCenter().removeObserver(self, name: kReachabilityChangedNotification, object: nil) }
      })
    } else {
      NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(QMAPI.loginIfReachable), name: kReachabilityChangedNotification, object: nil)
    }
    
  }
  
  
}



extension NSURLRequest {
  
  
  var customDescription: String {
    return ""
      + "\nHTTPMethod: \(self.HTTPMethod)\n"
      + "\nHTTPBody: \(self.HTTPBody)\n"
      + "\nallHTTPHeaderFields: \(self.allHTTPHeaderFields)\n"
      + "\ndescription: \(self.description)"
  }
  
}


func += <KeyType, ValueType> (inout left: Dictionary<KeyType, ValueType>, right: Dictionary<KeyType, ValueType>) {
  for (k, v) in right {
    left.updateValue(v, forKey: k)
  }
}