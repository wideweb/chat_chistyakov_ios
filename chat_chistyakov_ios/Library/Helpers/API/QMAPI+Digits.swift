//
//  QMAPI+Digits.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 23.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


extension QMAPI {
  
  
  func loginWithTwitterDigits(phone: String, completion: QMAPIAuthSimpleCompletion?) {
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    
    let configuration = DGTAuthenticationConfiguration(accountFields: .DefaultOptionMask)
    configuration.phoneNumber = phone
    configuration.title = "Wavix"
    
    Digits.sharedInstance().authenticateWithViewController(nil, configuration: configuration) { (session: DGTSession?, error: NSError?) in
      let authConfig = Digits.sharedInstance().authConfig
      
      if let error = error { completion?(error); return }
      guard let session = session else { completion?(nil); return }
      
      let oauthSigning = DGTOAuthSigning(authConfig: authConfig, authSession: session)
      let authHeaders = oauthSigning.OAuthEchoHeadersToVerifyCredentials()
      
      var request: QBRequest?
      request = self.authService.loginWithTwitterDigitsAuthHeaders(authHeaders, completion: { (response: QBResponse!, user: QBUUser!) in
        if response.success {
          //  Additional actions here...
          self.connectChat({ (error) -> () in
            CCLog.log("\(#file) \(#function)  ...connectChat... error: \(error)")
            
            if let error = error {
              self.handleErrorRequest(request, response: response, localError: error)
              
              CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... completion: \(error)")
              completion?(error)
              UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
            else {
              CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... success")
              
              if self.currentUser().colorCode == nil {
                
                self.updateCurrentUser([.Color : userColors[Int(rand()) % userColors.count]], completion: { (error: NSError?) in
                  if let error = error {
                    self.handleErrorRequest(request, response: response, localError: error)
                    
                    completion?(error)
                  }
                  else { self.successLogin(completion) }
                  UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                })
              } else {
                self.successLogin(completion)
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
              }
              
              /*self.successLogin(completion)*/ }
            
          })
        }
        else {
          self.handleErrorRequest(request, response: response)
          
          let error = self.customHandleErrorResponse(response)
          CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... completion: \(error)")
          completion?(error)
          UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
      })
      
    }
//    Digits.sharedInstance().authenticateWithCompletion { (session: DGTSession?, error: NSError?) in
//      let authConfig = Digits.sharedInstance().authConfig
//      
//      if let error = error { completion?(error); return }
//      guard let session = session else { completion?(nil); return }
//      
//      let oauthSigning = DGTOAuthSigning(authConfig: authConfig, authSession: session)
//      let authHeaders = oauthSigning.OAuthEchoHeadersToVerifyCredentials()
//      
//      var request: QBRequest?
//      request = self.authService.loginWithTwitterDigitsAuthHeaders(authHeaders, completion: { (response: QBResponse!, user: QBUUser!) in
//        if response.success {
//          //  Additional actions here...
//          self.connectChat({ (error) -> () in
//            CCLog.log("\(#file) \(#function)  ...connectChat... error: \(error)")
//            
//            if let error = error {
//              self.handleErrorRequest(request, response: response, localError: error)
//              
//              CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... completion: \(error)")
//              completion?(error) }
//            else {
//              CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... success")
//              self.successLogin(completion) }
//            
//          })
//        }
//        else {
//          self.handleErrorRequest(request, response: response)
//          
//          let error = self.customHandleErrorResponse(response)
//          CCLog.log("\(#file) \(#function)  ...authService.logInWithUser... completion: \(error)")
//          completion?(error)
//        }
//      })
//      
//    }
  }
  
  
}