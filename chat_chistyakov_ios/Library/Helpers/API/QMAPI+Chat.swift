//
//  QMAPI+Chat.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


typealias QMAPIChatSimpleCompletion = (error: NSError?)->()
typealias QMAPIChatDialogCompletion = (chatDialog: QBChatDialog?, error: NSError?)->()
typealias QMAPIChatDialogsCompletion = (chatDialogs: [QBChatDialog], error: NSError?)->()


extension QMAPI {

  
  func connectChat(completion: QMAPIChatSimpleCompletion?) {
    self.chatService.connectWithCompletionBlock { (error: NSError?) -> Void in
      NSLog("Connected with: \(self.currentUser()?.login)")
      completion?(error: error)
    }
    
  }
  
  
  func chatWithUser(user: QBUUser, completion: QMAPIChatDialogCompletion?) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
      QMAPI.instance().createPrivateChatDialogIfNeeded(user) { (chatDialog, error) -> () in
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          completion?(chatDialog: chatDialog, error: error)
        })
      }
    }
    
  }
  
  
  func getDialog(dialogID: String, completion: QMAPIChatDialogCompletion?) {
    QMAPI.instance().chatService.fetchDialogWithID(dialogID) { (dialog: QBChatDialog?) -> Void in
      if let dialog = dialog { completion?(chatDialog: dialog, error: nil) }
      else {
        QMAPI.instance().chatService.loadDialogWithID(dialogID, completion: { (dialog: QBChatDialog?) -> Void in
          completion?(chatDialog: dialog, error: nil)
        })
      }
    }
    
  }
  
  
  func createPrivateChatDialogIfNeeded(opponent: QBUUser, completion: QMAPIChatDialogCompletion?) {
    self.chatService.createPrivateChatDialogWithOpponent(opponent) { (response: QBResponse?, chatDialog: QBChatDialog?) -> Void in
      
      completion?(chatDialog: chatDialog, error: response?.error?.error)
    }
    
  }
  
  
  func disconnectFromChat() {
    self.chatService.disconnectWithCompletionBlock { (error: NSError?) -> Void in
      
    }
    
  }
  
  
  func disconectFromChatIfNeeded() {
    if UIApplication.sharedApplication().applicationState == .Background && QBChat.instance().isConnected() {
      self.disconnectFromChat()
    }
    
  }
  
  
  //  MARK: -
  //  MARK: dialogs
  func dialogs() -> [QBChatDialog] {
    return self.chatService.dialogsMemoryStorage.dialogsSortByUpdatedAtWithAscending(false) as? [QBChatDialog] ?? []
  }
  
  
  func dialogHistory() -> [QBChatDialog] {
    return self.chatService.dialogsMemoryStorage.unsortedDialogs() as? [QBChatDialog] ?? []
  }
  
  
  func privateDialogWithUser(userID: UInt) -> QBChatDialog? {
    return self.chatService.dialogsMemoryStorage.privateChatDialogWithOpponentID(userID)
  }
  
  
  func safeDialogs(completion: QMAPIChatDialogsCompletion?) {
    self.chatService.allDialogsWithPageLimit(1000, extendedRequest: [:], iterationBlock: { (_, _, _, _) -> Void in }) {
      (response: QBResponse?) -> Void in
      
      completion?(chatDialogs: self.dialogs(), error: response?.error?.error)
    }
  }
  
  
  
  //  MARK: -
  //  MARK: Edit dialog
  func deleteChatDialog(dialog: QBChatDialog, completion: QMAPIChatSimpleCompletion?) {
    self.chatService.deleteDialogWithID(dialog.ID) { (response: QBResponse!) -> Void in
      completion?(error: response.error?.error)
    }
  }
  
  
  //  MARK: -
  //  MARK: Messages
  func messagesFromDialogID(dialogID: String) -> [QBChatMessage] {
    return self.chatService.messagesMemoryStorage.messagesWithDialogID(dialogID) as? [QBChatMessage] ?? []
  }
  
  
  func lastMessageFromDialogID(dialogID: String) -> QBChatMessage? {
    return self.chatService.messagesMemoryStorage.lastMessageFromDialogID(dialogID)
  }
  
  
  func messages(dialogID: String, completion: ([QBChatMessage])->()) {
    self.chatService.messagesWithChatDialogID(dialogID) { (response: QBResponse!, items: [AnyObject]?) -> Void in
      if let messages = items as? [QBChatMessage] { completion(messages) }
      else { completion([]) }
    }
  }
  
  
  func lastMessage(dialogID: String, completion: (QBChatMessage?)->()) {
    QBRequest.messagesWithDialogID(dialogID,
      extendedRequest: ["sort_desc" : "date_sent", "limit" : "1", "mark_as_read":"0"],
      forPage: nil,
      successBlock: { (response, messages, page) -> Void in
        completion(messages?.last)
      },
      errorBlock: { (response) -> Void in
        completion(nil)
      }
    )
  }
  
  
  func notReadedMessages(dialogID: String, completion: ([QBChatMessage])->()) {
    self.messages(dialogID) { (messages: [QBChatMessage]) -> () in
      let myID = QMAPI.instance().currentUser().ID
      completion(messages.filter({ $0.readIDs?.filter({ $0 == myID }).count == 0 }))
    }
  }
  
  
  func lastNotReadMessage(dialogID: String, completion: (QBChatMessage?)->()) {
    self.lastMessage(dialogID) { (message: QBChatMessage?) -> () in
      let myID = self.currentUser().ID
      if message?.readIDs?.filter({ $0 == myID }).count == 0 { completion(message) }
      else { completion(nil) }
    }
  }

  
}