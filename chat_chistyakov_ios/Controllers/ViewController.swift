//
//  ViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 01.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


  private var items = [
    ("Register", Segue.segueMenuToRegister),
    ("Signin", Segue.segueMenuToSignin)
  ]
  
  
  @IBOutlet private weak var tableView: UITableView!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }


}



extension ViewController : UITableViewDataSource, UITableViewDelegate {

  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    let item = items[indexPath.row]
    cell.textLabel?.text = item.0
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let item = items[indexPath.row]
    let segue = item.1
    performSegue(segue)
  }
  
  
}
