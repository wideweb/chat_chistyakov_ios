//
//  CCBaseViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 10.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit
import MessageUI


class CCBaseViewController: UIViewController, YLLongTapShareDelegate, MFMailComposeViewControllerDelegate {

  
  override func viewDidLoad() {
    super.viewDidLoad()
//    self.setupLongTapMenu()
    
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
     self.updateViewController()
  }
  
  
  func updateViewController() {
    self.configureNavigationBar()
    self.configureTabBar()
  }
  
  
  
  //  MARK: -
  //  MARK: Navigation bar
  func configureNavigationBar() {
    self.setTrasparentNavigationBar()
    
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.systemFontOfSize(33)
    ]
    
    self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
 
    if self.navigationController?.viewControllers.count > 1 {
      let spacer = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
      spacer.width = -10
      self.navigationItem.leftBarButtonItems = [spacer, self.backButton()]
    }
    
  }
  
  
  func setTrasparentNavigationBar() {
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
    self.navigationController?.navigationBar.shadowImage = UIImage()
    
    self.navigationController?.navigationBar.translucent = true
  }
  
  
  func backButton() -> UIBarButtonItem {
    if let nc = self.navigationController { return UIBarButtonItem.backButton(nc, action: Selector("popViewControllerAnimated:")) }
    else { return UIBarButtonItem() }
    
  }
  
  
  //  MARK: -
  //  MARK: Tab bar
  func configureTabBar() {
    if let tabBar = self.tabBarController?.tabBar {
      tabBar.backgroundImage = UIImage()
      tabBar.translucent = true

      tabBar.tintColor = UIColor.whiteColor()
    }
  
  }
  
  
  
  //  MARK: -
  //  MARK: Navigation
  func ccShowViewController(vc: UIViewController, preparation: ((controller: UIViewController)->())?) {
    switch vc {
    case let nc as UINavigationController:
      let controller = nc.topViewController!
      preparation?(controller: controller)
      self.presentViewController(nc, animated: true, completion: nil)
    default:
      preparation?(controller: vc)
      self.navigationController?.pushViewController(vc, animated: true)
    }
    
  }
  
  
  func ccShowChat(preparation: ((controller: UIViewController)->())? = nil) {
//    QMAPI.instance().chatWithUser(user) { (chatDialog, error) -> () in
//      if let error = error { MessageCenter.sharedInstance.showError(error.localizedDescription) }
//      else { self.performSegue(Segue.segueContactsToChat, sender: chatDialog) }
//    }
//    
//    let controller = Storyboards.Main.instantiateChat()
//    self.ccShowViewController(controller, preparation: preparation)
  }
  
  
  
  // - MARK: YLLongTapShareDelegate
  func setupLongTapMenu(mainView: UIView) {
    let longTapMenuView = YLLongTapShareView(frame: UIScreen.mainScreen().bounds)
    mainView.addSubview(longTapMenuView)
    mainView.sendSubviewToBack(longTapMenuView)
    for v in self.view.subviews {
      if let bg = v as? UIImageView { self.view.sendSubviewToBack(bg); break }
    }
    
    UIGraphicsBeginImageContext(CGSize(width: 70, height: 70))
    let context = UIGraphicsGetCurrentContext()
    UIGraphicsPushContext(context!)
    UIImage(named: "logout")!.drawAtPoint(CGPoint(x: 10, y: 10))
    UIGraphicsPopContext()
    let bugImageWithInset = UIGraphicsGetImageFromCurrentImageContext()
    
    longTapMenuView.addShareItem(YLShareItem(icon: bugImageWithInset, andTitle: "LogOut"))
    longTapMenuView.delegate = self
    
  }
  
  func longTapShareView(view: UIView!, didSelectShareTo item: YLShareItem!, withIndex index: UInt) {
//    self.openEmail("sema@wide-web.spb.ru", parentViewController: self)
  }

  
  func openEmail(url: String, parentViewController: UIViewController) {
    guard MFMailComposeViewController.canSendMail() else {
      MessageCenter.sharedInstance.showError("Unable to send an e-mail")
      return
    }
    guard let logData = CCLog.loggedData(), let logString = String(data: logData, encoding: NSUTF8StringEncoding) else {
      MessageCenter.sharedInstance.showError("No logs to send")
      return
    }
    
    NSLog("LOG TO SEND: \(logString)")
    
    let mc = MFMailComposeViewController()
    mc.setToRecipients([url])
    mc.setSubject("Bug report")
    mc.mailComposeDelegate = self
    mc.setMessageBody(logString, isHTML: false)
//    mc.addAttachmentData(logData, mimeType: "text/plain", fileName: "log.txt")
    
    parentViewController.presentViewController(mc, animated: true, completion: nil)
    
  }
  
  
  
  func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
    controller.dismissViewControllerAnimated(true, completion: nil)
  }
  
  
  
  // - MARK: PopMenu
  func showMenu() {
    let styleItem = MenuItem(title: "Change Style", iconName: "style")
    let profileItem = MenuItem(title: "Change Avatar", iconName: "contact_placeholder")
    
    guard let view = UIApplication.sharedApplication().keyWindow else { return }
    let popMenu = PopMenu(frame: view.bounds, items: [styleItem, profileItem])
    popMenu.menuAnimationType = .Sina
    popMenu.perRowItemCount = 2
    popMenu.showMenuAtView(view)
    
    popMenu.didSelectedItemCompletion = { menuItem in
      switch menuItem {
      case styleItem: self.changeStyle()
      case profileItem: self.changeAvatar()
      default: break
      }
    }
    
//    self.setupLongTapMenu(popMenu)
    
  }
  
  
  func changeStyle() {
    
  }
  
  
  func changeAvatar() {
    UIImagePickerControllerHelper.sharedInstance.showImagePickerInViewController(self) { (selectedIamge: UIImage?) in
      dispatch_async(dispatch_get_main_queue(), { 
        if let image = selectedIamge {
          let cropper = RSKImageCropViewController(image: image)
          cropper.delegate = self
          cropper.avoidEmptySpaceAroundImage = true
          
          dispatch_async(dispatch_get_main_queue(), {
            self.presentViewController(cropper, animated: true, completion: nil)
          })
        }
      })
    }
    
  }
  
  
}


extension CCBaseViewController: RSKImageCropViewControllerDelegate {

  
  func imageCropViewControllerDidCancelCrop(controller: RSKImageCropViewController) {
    self.navigationController?.popViewControllerAnimated(true)
  }

  
  func imageCropViewController(controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
    dispatch_async(dispatch_get_main_queue()) { 
      let hud = MBProgressHUD.showHUDAddedTo(controller.view, animated: true)
      hud.mode = .DeterminateHorizontalBar
      hud.labelText = "Uploading image..."
      hud.show(true)
      
      let resizedImage = croppedImage.resizeTo(CGSize(width: 512, height: 512))
      QMAPI.instance().updateCurrentUserAvatar(resizedImage ?? croppedImage, statusBlock: { percent in hud.progress = percent }) { (error: NSError?) in
        hud.hide(true)
        if let error = error {
          MessageCenter.sharedInstance.showError(error.localizedDescription)
        } else {
          controller.dismissViewControllerAnimated(true, completion: nil)
        }
      }
    }
    
  }
  
  
}