//
//  UIFontExtensions.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 10.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


private let kBebasNeueLightFontName = "BebasNeue-Light"
private let kBebasNeueBookFontName = "BebasNeueBook"
private let kBebasNeueBoldFontName = "BebasNeueBold"
private let kHelveticaNeueCyrLightFontName = "HelveticaNeueCyr-Light"
private let kHelveticaNeueCyrMediumFontName = "HelveticaNeueCyr-Medium"
private let kHelveticaNeueCyrUltraLightFontName = "HelveticaNeueCyr-UltraLight"
private let kHelveticaNeueCyrRomanFontName = "HelveticaNeueCyr-Roman"
private let kHelveticaNeueCyrItalicFontName = "HelveticaNeueCyr-Italic"
private let kLarsseitFontName = "Larsseit"
private let kShadowFontName = "Shadow-Regular"

private let kHarlowSolidFontName = "HarlowSolid"
private let kPragmaticaBoldFontName = "PragmaticaBold"

extension UIFont {

  
  class func bebasNeueLightFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kBebasNeueLightFontName, size: size)
  }
  
  
  class func bebasNeueBookFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kBebasNeueBookFontName, size: size)
  }
  
  
  class func bebasNeueBoldFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kBebasNeueBoldFontName, size: size)
  }
  
  
  class func helveticaNeueCyrLightFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kHelveticaNeueCyrLightFontName, size: size)
  }
  
  
  class func helveticaNeueCyrMediumFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kHelveticaNeueCyrMediumFontName, size: size)
  }
  
  
  class func helveticaNeueCyrUltraLightFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kHelveticaNeueCyrUltraLightFontName, size: size)
  }
  
  
  class func helveticaNeueCyrRomanFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kHelveticaNeueCyrRomanFontName, size: size)
  }
  
  
  class func helveticaNeueCyrItalicFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kHelveticaNeueCyrItalicFontName, size: size)
  }
  
  
  class func larsseitFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kLarsseitFontName, size: size)
  }
  
  
  class func shadowFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kShadowFontName, size: size)
  }
  
  
  class func harlowSolidFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kHarlowSolidFontName, size: size)
  }
  
  
  class func pragmaticaBoldFontOfSize(size: CGFloat) -> UIFont {
    return fontWithName(kPragmaticaBoldFontName, size: size)
  }
  
  
  private class func fontWithName(name: String, size: CGFloat) -> UIFont {
    guard let font = UIFont(name: name, size: size) else {
      assertionFailure("Font with name \" \(name) \" not exist")
      return UIFont.systemFontOfSize(size) }
    
    return font
  }
  
  
  class func enumerateAvailableFonts() {
    for family: String in UIFont.familyNames()
    {
      print("\(family)")
      for names: String in UIFont.fontNamesForFamilyName(family)
      {
        print("== \(names)")
      }
    }
    
  }
  
  
}