//
//  CCChatsViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCChatsViewController: CCBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  private var dataSource: QMDialogsDataSource!
  
  
  @IBOutlet private weak var tableView: UITableView!
  
  
  
  //  MARK: -
  //  MARK: VLC
  override func viewDidLoad() {
    super.viewDidLoad()
    
    dataSource = QMDialogsDataSource()
    dataSource.delegate = self
    
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    self.setupNavBar()
  }
  
  
  func setupNavBar() {
    if let navigationItem = self.tabBarController?.navigationItem {
      navigationItem.title = "CHATS"
      
      navigationItem.rightBarButtonItems = []
    }
    
//    let addItem = UIBarButtonItem(image: UIImage(named: "add_contact"), style: .Plain, target: self, action: Selector("addTapped:"))
//    let searchItem = UIBarButtonItem(image: UIImage(named: "search"), style: .Plain, target: self, action: Selector("searchTapped:"))
//    
//    addItem.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5)
//    searchItem.imageInsets = UIEdgeInsetsMake(0, 15, 0, -15)
//    
//    navigationItem.rightBarButtonItems = [addItem, searchItem]
  }
  

}



//  MARK: -
//  MARK: QMDialogsDataSourceDelegate
extension CCChatsViewController: QMDialogsDataSourceDelegate {
  
  
  func didReloadDataSource() {
    self.tableView.reloadData()
  }
  

}


//  MARK: -
//  MARK: UITableViewDataSource, UITableViewDelegate
//extension CCChatsViewController: UITableViewDataSource, UITableViewDelegate {
//  
//  
//  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//    return 1
//  }
//  
//  
//  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//    return dataSource.dialogs.count
//  }
//  
//  
//  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//    let cell = tableView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)
//    return cell!
//  }
//  
//  
//  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
//    let dialog = dataSource.dialogs[indexPath.row]
//    
//    if let cell = cell as? CCChatTableViewCell {
//      cell.setData(dialog)
//    }
//    
//  }
//  
//  
//  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//    let dialog = dataSource.dialogs[indexPath.row]
//    self.performSegue(Segue.segueListToChat, sender: dialog)
//  }
//  
//  
//}



//  MARK: -
//  MARK: Navigation
//extension CCChatsViewController {
//  
//  
//  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//    if segue == Segue.segueListToChat {
//      let controller = segue.destinationViewController as! CCChatViewController
//      controller.dialog = sender as! QBChatDialog
//    }
//    
//  }
//  
//  
//}