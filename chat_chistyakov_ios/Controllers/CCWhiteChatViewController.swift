//
//  CCWhiteChatViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 24.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit


class CCWhiteChatViewController: QMChatViewController {
  
  
  
  //  MARK: -
  //  MARK: Properties
  var dialog: QBChatDialog?
  var recipient: QBUUser?
  
  
  
  private var sectionHeaderTitles: [String : Int] = [:]
  private var unreadMessages: [QBChatMessage] = []
  private var storedMessages: [QBChatMessage] {
    return QMAPI.instance().chatService.messagesMemoryStorage.messagesWithDialogID(self.dialog?.ID) as? [QBChatMessage] ?? []
  }
  
  private var statusTimer: NSTimer?
  
  
  @IBOutlet private var statusLabel: UILabel?
  @IBOutlet private var bgView: UIImageView?
  
  @IBOutlet private var sendButton: UIButton!
  
  
  //  MARK: -
  //  MARK: Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupNotifications()
    self.configureInterface()
    
    let user = QMAPI.instance().currentUser()
    self.senderID = user.ID
    self.senderDisplayName = user.fullName ?? user.login
    
    self.timeIntervalBetweenSections = 300
    self.heightForSectionHeader = 40
    
    let nibOut = UINib(nibName: "CCWSOutcomingChatCell", bundle: nil)
    let reuseOut = CCWSOutcomingChatCell.cellReuseIdentifier()
    self.collectionView?.registerNib(nibOut, forCellWithReuseIdentifier: reuseOut)
    
    let nibIn = UINib(nibName: "CCWSIncomingChatCell", bundle: nil)
    let reuseIn = CCWSIncomingChatCell.cellReuseIdentifier()
    self.collectionView?.registerNib(nibIn, forCellWithReuseIdentifier: reuseIn)
    
    QBChat.instance().addDelegate(self)
    
    self.loadDialog()
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.setupNavBar()
    
    self.retrieveMessages()
    //    self.startUpdatingStatus()
  }
  
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    self.loadCachedMessages()
    
    QMAPI.instance().chatService.addDelegate(self)
    QMAPI.instance().chatService.chatAttachmentService.delegate = self
    
  }
  
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    
//    self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
    
    self.stopUpdatingStatus()
  }
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    self.bgView?.frame = self.view.bounds
    if let tapView = self.view.viewWithTag(123) {
      self.view.bringSubviewToFront(tapView)
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: Interface
  func setupNavBar() {
    self.navigationItem.titleView = self.customNavigationTitleView()
    
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
    self.navigationController?.navigationBar.shadowImage = UIImage()
    self.navigationController?.navigationBar.translucent = true
    self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
    
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.shadowFontOfSize(30)
    ]
    
    if self.navigationController?.viewControllers.count > 1 {
      let spacer = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
      spacer.width = -10
      
      self.navigationItem.leftBarButtonItems = [spacer, self.backButton()]
    }
    
  }
  
  
  func customNavigationTitleView() -> UIView {
    let user = QMAPI.instance().user(UInt(dialog?.recipientID ?? 0))
    let titleText = user?.fullName ?? user?.login ?? "CHAT"
    var statusText = ""
    
    if let date = user?.lastRequestAt {
      if NSDate().timeIntervalSinceDate(date) < 60 {
        statusText = "online" }
      else {
        statusText = "last online " + QMDateUtils.formattedStringFromDate(date) }
    } else {
      statusText = "offline"
    }
    
    let titleAttributedText = NSAttributedString(string: titleText, attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0x000000),
      NSFontAttributeName : UIFont.helveticaNeueCyrMediumFontOfSize(30/2),
      NSKernAttributeName : 0.3/2,
      NSParagraphStyleAttributeName : {
        let style = NSMutableParagraphStyle()
        style.alignment = .Center
        style.lineBreakMode = .ByTruncatingTail
        return style
        }()
    ])
    let statusAttributedText = NSAttributedString(string: statusText, attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0x474747).colorWithAlphaComponent(0.6),
      NSFontAttributeName : UIFont.helveticaNeueCyrMediumFontOfSize(22/2),
      NSKernAttributeName : 0.22/2
    ])
    
    let textRect = titleAttributedText.boundingRectWithSize(CGSize(width: 10000, height: 30), options: NSStringDrawingOptions(rawValue: 0), context: nil).width
    let statusRect = statusAttributedText.boundingRectWithSize(CGSize(width: 10000, height: 30), options: NSStringDrawingOptions(rawValue: 0), context: nil).width
    
    let avatarWidth: CGFloat = 0
    let avatarOffset: CGFloat = 0
    let titleHeight: CGFloat = 17
    let statusHeight: CGFloat = 17
    let width = UIScreen.mainScreen().bounds.width - 90 * 2 //min( UIScreen.mainScreen().bounds.width - 100, max(textRect, statusRect) + (avatarWidth + avatarOffset))
    let titleWidth: CGFloat = width//max(textRect, statusRect) //width - (avatarWidth + avatarOffset)
    let height: CGFloat = 60
    
    let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
    view.backgroundColor = UIColor.clearColor()
    
//    let avatarView = CCAvatarImageView(frame: CGRect(x: 0, y: 8, width: avatarWidth, height: avatarWidth))
//    avatarView.layer.shadowOpacity = 0
//    avatarView.layer.borderWidth = 0
//    view.addSubview(avatarView)
//    
//    var userColor = UIColor.blueColor()
//    if let colorCodeString = user?.colorCode {
//      userColor = UIColor(hex: colorCodeString)
//    }
//    avatarView.backgroundColor = userColor
//    
//    
//    if let urlString = QMAPI.instance().publicImageUrlWithID(user?.blobID ?? 0), let url = NSURL(string: urlString) {
//      avatarView.sd_setImageWithURL(url, placeholderImage: nil, completed: { (image: UIImage?, _, _, responseUrl: NSURL?) in
//        if urlString == responseUrl?.absoluteString && image != nil {
//          avatarView.contentMode = .ScaleAspectFit
//          avatarView.image = image
//          avatarView.layer.borderWidth = 0
//        }
//      })
//    }
    
    
    let titleLabel = UILabel(frame: CGRect(x: avatarWidth + avatarOffset, y: 15, width: titleWidth, height: titleHeight))
    titleLabel.attributedText = titleAttributedText
    view.addSubview(titleLabel)
    
    self.statusLabel = UILabel(frame: CGRect(x: avatarWidth + avatarOffset, y: titleLabel.frame.origin.y + titleLabel.frame.height, width: titleWidth, height: statusHeight))
    if let statusLabel = self.statusLabel {
      statusLabel.attributedText = statusAttributedText
      statusLabel.textAlignment = .Center
      view.addSubview(statusLabel)
    }
    
//    titleLabel.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.3)
//    view.backgroundColor = UIColor.greenColor().colorWithAlphaComponent(0.3)
//    statusLabel?.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.3)
    
    return view
  }
  
  
  func backButton() -> UIBarButtonItem {
    let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 40))
    backButton.setImage(UIImage(named: "left_arrow_ws")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
    backButton.contentEdgeInsets = UIEdgeInsets(top: -1/2, left: -4/2, bottom: 1/2, right: 4/2)
    backButton.addTarget(self.navigationController, action: #selector(UINavigationController.popViewControllerAnimated(_:)), forControlEvents: .TouchUpInside)
    
    return UIBarButtonItem(customView: backButton)
    
  }
  
  
  func configureInterface() {
    self.collectionView?.backgroundColor = UIColor.clearColor()
    
//    let topSeparatorView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: 1))
//    topSeparatorView.backgroundColor = UIColor(netHex: 0x95dcf9)
//    self.view.addSubview(topSeparatorView)
    
    let settingsItem = UIBarButtonItem(image: UIImage(named: "settings"), style: .Plain, target: nil, action: nil)
    self.navigationItem.rightBarButtonItem = settingsItem
    
    //  background
    bgView = UIImageView(frame: self.view.bounds)
    if let bgView = self.bgView {
      bgView.contentMode = .ScaleAspectFill
      
      if var bgImage = UIImage(named: "bg_chat_ws") {
        let width = bgImage.size.width * bgImage.scale
        let k = width / UIScreen.mainScreen().bounds.width
        let navHeight = 64 * k
        let height = bgImage.size.height * bgImage.scale - navHeight
        
        bgImage = bgImage
          .simpleCrop(CGPoint(x: 0, y: navHeight), cropSize: CGSize(width: width, height: height))!
          .resizeTo(CGSize(width: UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height - 64))!
        bgView.image = bgImage
      }
      self.view.addSubview(bgView)
      self.view.sendSubviewToBack(bgView)
    }
    
    //  touch view
    let tapView = TapCatchView(frame: self.view.bounds)
    self.view.addSubview(tapView)
    tapView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero)
    tapView.tag = 123
    
    //  toolbar
    let bgColor = UIColor(netHex: 0xE4F4FA)
    self.inputToolbar?.setBackgroundImage(UIImage.imageWithColor(bgColor, size: CGSizeMake(1000, 49)), forToolbarPosition: .Any, barMetrics: .Default)
    
    let leftButtonWidth: CGFloat = 56/2
    let leftButtonInset: CGFloat = 11/2
    let leftViewInsets = UIEdgeInsets(top: 16/2, left: 13/2, bottom: 16/2, right: 24/2)
    let rightButtonWidth: CGFloat = 80/2
    let rightButtonInset: CGFloat = 15/2
    
    //  RIGHT
    sendButton = UIButton(type: UIButtonType.Custom)
    sendButton.addTarget(self, action: #selector(CCWhiteChatViewController.sendTapped(_:)), forControlEvents: .TouchUpInside)
    sendButton.setImage(UIImage(named: "micro_ws"), forState: .Normal)

    sendButton.frame = CGRect(x: rightButtonInset, y: rightButtonInset, width: rightButtonWidth, height: rightButtonWidth)
    
    let rightView = UIView(frame: CGRect(x: 0, y: 0, width: rightButtonInset * 2 + rightButtonWidth, height: rightButtonInset * 2 + rightButtonWidth))
    rightView.addSubview(sendButton)
    rightView.backgroundColor = bgColor
    
    rightView.frame.origin = CGPoint(x: UIScreen.mainScreen().bounds.width - rightView.bounds.width - 1, y: -11/2)
    rightView.layer.cornerRadius = rightView.frame.size.height / 2
    
    //  LEFT
    let addButton = UIButton(frame: CGRect(x: 0, y: 0, width: leftButtonWidth, height: leftButtonWidth))
    let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: leftButtonWidth, height: leftButtonWidth))
    
    addButton.setImage(UIImage(named: "add_ws"), forState: .Normal)
    menuButton.setImage(UIImage(named: "menu_ws"), forState: .Normal)
    
//    addButton.enabled = false
//    menuButton.enabled = false
    
    let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 2 * leftButtonWidth + leftButtonInset, height: leftButtonWidth))
    addButton.frame.origin = CGPoint(x: 0, y: 0)
    menuButton.frame.origin = CGPoint(x: leftButtonWidth + leftButtonInset, y: 0)
    leftView.addSubview(addButton)
    leftView.addSubview(menuButton)
    
    leftView.frame.origin = CGPoint(x: leftViewInsets.left, y: leftViewInsets.top)
    
    //  TOOLBAR
    self.inputToolbar.preferredDefaultHeight = 44
    self.inputToolbar?.contentView?.rightBarButtonItem = nil
    self.inputToolbar?.contentView?.rightBarButtonItemWidth = 50
    self.inputToolbar?.contentView?.leftBarButtonItem = nil
    self.inputToolbar?.contentView?.addSubview(leftView)
    self.inputToolbar.contentView.addSubview(rightView)
    self.inputToolbar?.contentView?.leftBarButtonItemWidth = leftViewInsets.left + leftView.frame.width + leftViewInsets.right - 8
    
    self.inputToolbar.contentView.textView.layer.cornerRadius = 10
    self.inputToolbar.contentView.textView.backgroundColor = bgColor
    
    self.inputToolbar?.setShadowImage(UIImage.imageWithColor(UIColor(netHex: 0xedf6fb), size: CGSize(width: 1000, height: 1)), forToolbarPosition: .Any)
    
    self.inputToolbar.opaque = true
    self.inputToolbar.translucent = false
    self.inputToolbar.barTintColor = UIColor.whiteColor()
    
    self.configureToolbar(false)
    
//    if let constraint = super.valueForKey("toolbarHeightConstraint") as? NSLayoutConstraint { constraint.constant = 49 }
    
    self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 10, left: 21/2, bottom: 15, right: 21/2)
    self.view.constraints.filter({ $0.firstItem as? QMChatCollectionView != nil && $0.firstAttribute == .Top }).first?.constant = 64
    
  }
  
  
  func configureToolbar(active: Bool) {
    self.inputToolbar.contentView.textView.backgroundColor = active ? UIColor.whiteColor() : UIColor.whiteColor().colorWithAlphaComponent(0.3)
    self.sendButton.setImage(active ? UIImage(named: "send_ws") : UIImage(named: "micro_ws"), forState: .Normal)
    
  }
  
  
  
  // - MARK: Actions
  @IBAction func sendTapped(sender: UIButton) {
    let messageText = self.currentlyComposedMessageText()
    if messageText.characters.count > 0 {
    self.didPressSendButton(sender,
                            withMessageText: messageText,
                            senderId: self.senderID,
                            senderDisplayName: self.senderDisplayName,
                            date: NSDate())
    }
    
  }
  
  
  
  // - MARK: Notifications
  func setupNotifications() {
    let notificationCenter = NSNotificationCenter.defaultCenter()
    
    notificationCenter.addObserver(self, selector: #selector(CCWhiteChatViewController.handleOutsideTouch(_:)), name: kUserTouchNotificationName, object: nil)
    
  }
  
  
  func handleOutsideTouch(notification: NSNotification) {
    if let wPoint = (notification.object as? NSValue)?.CGPointValue() {
      let view = self.inputToolbar.contentView
      let cPoint = view.convertPoint(wPoint, fromView: UIApplication.sharedApplication().keyWindow)
      if !view.bounds.contains(cPoint) {
        view.textView.resignFirstResponder()
      }
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: QB helpers
  func startUpdatingStatus() {
    self.stopUpdatingStatus()
    
    self.updateRecipientStatus()
    self.statusTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: Selector("updateRecipientStatus"), userInfo: nil, repeats: true)
  }
  
  
  func stopUpdatingStatus() {
    self.statusTimer?.invalidate()
    self.statusTimer = nil
  }
  
  
  func updateRecipientStatus() {
    let status = QMAPI.instance().isOnlineUserWithID(UInt(dialog?.recipientID ?? 0))
    if status { self.statusLabel?.text = "online" }
    else if let lastActivityTime = QMAPI.instance().user(UInt(dialog?.recipientID ?? 0))?.lastRequestAt {
      let formatter = NSDateFormatter()
      formatter.dateFormat = "dd.MM.yy HH:mm"
      self.statusLabel?.text = formatter.stringFromDate(lastActivityTime)
    }
    else { self.statusLabel?.text = "offline" }
  }
  
  
  func loadDialog() {
    if let _ = self.dialog { return }
    
    if let recipient = self.recipient {
      QMAPI.instance().createPrivateChatDialogIfNeeded(recipient) { (chatDialog, error) -> () in
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          if let error = error {
            NSLog("\(error)")
            self.navigationController?.popViewControllerAnimated(true)
            
          } else if let dialog = chatDialog {
            self.dialog = dialog
            self.setupNavBar()
            self.retrieveMessages()
            self.loadCachedMessages()
            
          } else {
            NSLog("unknown error")
            self.navigationController?.popViewControllerAnimated(true)
            
          }
          
        })
      }
    }
    
  }
  
  
  func retrieveMessages() {
    if self.storedMessages.count > 0 && self.totalMessagesCount == 0 {
      self.updateDataSourceWithMessages(self.storedMessages)
      self.refreshMessagesShowingProgress(false)
    } else {
      if self.totalMessagesCount == 0 { /*MessageCenter.sharedInstance.showLoading("Refreshing...")*/ }
      QMAPI.instance().cachedMessagesWithDialogID(self.dialog?.ID, block: { (collection: [AnyObject]!) -> Void in
        if collection.count > 0 {
          self.insertMessagesToTheBottomAnimated(collection as? [QBChatMessage] ?? [])
        }
        
        self.refreshMessagesShowingProgress(false)
      })
    }
    
  }
  
  
  func loadCachedMessages() {
    if self.storedMessages.count > 0 && Int(self.totalMessagesCount) != self.storedMessages.count {
      self.insertMessagesToTheBottomAnimated(self.storedMessages)
    }
  }
  
  
  func refreshMessagesShowingProgress(showingProgress: Bool) {
    if showingProgress /*&& self.isSendingAttachment*/ {
      /*MessageCenter.sharedInstance.showLoading("Refreshing...")*/
    }
    
    QMAPI.instance().chatService.messagesWithChatDialogID(self.dialog?.ID) { (response: QBResponse!, messages: [AnyObject]!) -> Void in
      if response.success {
        if messages.count > 0 { self.insertMessagesToTheBottomAnimated(messages as! [QBChatMessage]) }
        /*if self.isSendingAttachment { MessageCenter.sharedInstance.hideLoading() }*/
      } else {
        MessageCenter.sharedInstance.showError(response.error?.error?.localizedDescription ?? "Can not refresh messages")
      }
    }
    
  }
  
  
  func loadEarlierMessages() {
    QMAPI.instance().chatService.loadEarlierMessagesWithChatDialogID(self.dialog?.ID).continueWithBlock { (task:BFTask) -> AnyObject? in
      if task.result?.count > 0 { self.insertMessagesToTheTopAnimated(task.result as? [QBChatMessage] ?? []) }
      return nil
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: Utilities
  func sendReadStatusForMessage(message: QBChatMessage) {
    guard let readIDs = message.readIDs else { return }
    
    if message.senderID != self.senderID && !readIDs.contains(self.senderID) {
      QMAPI.instance().chatService.readMessage(message, completion: { (error: NSError?) -> Void in
        if let error = error { MessageCenter.sharedInstance.showError(error.localizedDescription) }
        
      })
    }
    
  }
  
  
  func readMessages(messages: [QBChatMessage]) {
    if QBChat.instance().isConnected() {
      QMAPI.instance().chatService.readMessages(messages, forDialogID: self.dialog?.ID, completion: { (error: NSError?) -> Void in
        if let error = error { MessageCenter.sharedInstance.showError(error.localizedDescription) }
      })
      
    } else {
      self.unreadMessages = messages
    }
    
  }
  
  
  func currentlyComposedMessageText() -> String {
    //  auto-accept any auto-correct suggestions
    self.inputToolbar.contentView.textView.inputDelegate?.selectionWillChange(self.inputToolbar.contentView.textView)
    self.inputToolbar.contentView.textView.inputDelegate?.selectionDidChange(self.inputToolbar.contentView.textView)
    
    return NSString(string: self.inputToolbar.contentView.textView.text).stringByTrimingWhitespace()
  }
  
  
  //  MARK: -
  //  MARK: Cell classes
  override func viewClassForItem(item: QBChatMessage?) -> AnyClass? {
    guard let item = item else { return nil }
    
    if item.senderID != self.senderID {
      if item.attachments?.count > 0 || item.attachmentStatus != .NotLoaded { return QMChatAttachmentIncomingCell.self }
//      else { return CCWSIncomingChatCell.self }
      else { return CCWSOutcomingChatCell.self }
    }
    else {
      if item.attachments?.count > 0 || item.attachmentStatus != .NotLoaded { return QMChatAttachmentOutgoingCell.self }
//      else { return CCWSOutcomingChatCell.self }
      else { return CCWSIncomingChatCell.self }
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: String builder
  override func attributedStringForItem(messageItem: QBChatMessage?) -> NSAttributedString? {
    guard let messageItem = messageItem else { return nil }
    guard let _ = messageItem.text else { return nil }
    
    let color = UIColor.blackColor()
    let font = UIFont.helveticaNeueCyrRomanFontOfSize(33/2)
    let attributes: [String : AnyObject] = [
      NSForegroundColorAttributeName : color,
      NSFontAttributeName : font,
      NSKernAttributeName : 0.33/2
    ]
    let attrStr = NSAttributedString(string: messageItem.text!, attributes: attributes)
    
    return attrStr
  }
  
  
  override func topLabelAttributedStringForItem(messageItem: QBChatMessage?) -> NSAttributedString? {
    guard let messageItem = messageItem else { return nil }
    
    if messageItem.senderID == self.senderID { return nil }
    
    let color = UIColor.redColor()
    let font = UIFont(name: "Helvetica", size: 14)!
    let attributes: [String : AnyObject] = [
      NSForegroundColorAttributeName : color,
      NSFontAttributeName : font
    ]
    let attrStr = NSAttributedString(string: messageItem.senderNick ?? "", attributes: attributes)
    
    return attrStr
  }
  
  
  override func bottomLabelAttributedStringForItem(messageItem: QBChatMessage?) -> NSAttributedString? {
    guard let messageItem = messageItem else { return nil }
    
    let color = UIColor(netHex: 0x4c4b4b)
    let font = UIFont.helveticaNeueCyrRomanFontOfSize(18 / 2)
    let attributes: [String : AnyObject] = [
      NSForegroundColorAttributeName : color,
      NSFontAttributeName : font,
      NSKernAttributeName : 0.18 / 2
    ]
    
    var string = ""
    if let dateSent = messageItem.dateSent {
      let formatter = NSDateFormatter(); formatter.dateFormat = "HH:mm"
      string = formatter.stringFromDate(dateSent) ?? "" }
    
    let attrStr = NSAttributedString(string: string, attributes: attributes)
    
    return attrStr
  }
  
  
  
  //  MARK: -
  //  MARK: Section headers
  override func collectionView(collectionView: QMChatCollectionView!, sectionHeaderAtIndexPath indexPath: NSIndexPath!) -> UICollectionReusableView! {
    let headerView = super.collectionView(collectionView, sectionHeaderAtIndexPath: indexPath)
    
    if let headerView = headerView as? QMHeaderCollectionReusableView {
      guard let title = self.sectionHeaderTitleAtIndexPath(indexPath.section, title: headerView.headerLabel?.text) else {
        headerView.hidden = true
        return headerView }
      let attributedTitle = NSAttributedString(string: title, attributes: [
        NSForegroundColorAttributeName : UIColor.whiteColor(),
        NSFontAttributeName : UIFont.larsseitFontOfSize(23 / 2),
        NSKernAttributeName : -0.97 / 3
        ])
      
      headerView.hidden = false
      headerView.headerLabel?.attributedText = attributedTitle
      headerView.headerLabel?.backgroundColor = UIColor.clearColor()
      
      headerView.layoutIfNeeded()
      let textRect = attributedTitle.boundingRectWithSize(CGSize(width: 10000, height: headerView.headerLabel?.frame.height ?? 15), options: NSStringDrawingOptions(rawValue: 0), context: nil)
      let frame = CGRectInset(textRect, -10, -5)
      let kBGViewTag = 1234
      let isNewBG = headerView.viewWithTag(kBGViewTag) == nil
      let bgView = headerView.viewWithTag(kBGViewTag) ?? UIView(frame: frame)
      bgView.frame = frame
      bgView.tag = kBGViewTag
      bgView.backgroundColor = UIColor(netHex:
        0x58627a
        //      0x505e79
        //      0x040736
      )
      //      .colorWithAlphaComponent(0.62)
      bgView.center = headerView.headerLabel?.center ?? CGPointZero
      bgView.layer.shadowColor = UIColor.blackColor().CGColor
      bgView.layer.shadowOffset = CGSize(width: 1, height: 6 / 2)
      bgView.layer.shadowOpacity = 0.3
      bgView.layer.shadowRadius = 6 / 2
      bgView.layer.masksToBounds = false
      bgView.clipsToBounds = false
      //      bgView.layer.borderWidth = 1
      //      bgView.layer.borderColor = UIColor(netHex: 0x58627a).CGColor
      
      if isNewBG {
        bgView.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(bgView)
        let CX = NSLayoutConstraint(item: bgView, attribute: NSLayoutAttribute.CenterX, relatedBy: .Equal, toItem: headerView, attribute: .CenterX, multiplier: 1, constant: 0)
        let CY = NSLayoutConstraint(item: bgView, attribute: NSLayoutAttribute.CenterY, relatedBy: .Equal, toItem: headerView, attribute: .CenterY, multiplier: 1, constant: 0)
        let W = NSLayoutConstraint(item: bgView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: frame.width)
        let H = NSLayoutConstraint(item: bgView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: frame.height)
        headerView.addConstraints([CX, CY, W, H])
      } else {
        if let W = headerView.constraints.filter({ $0.firstAttribute == NSLayoutAttribute.Width }).first { W.constant = frame.width }
        if let H = headerView.constraints.filter({ $0.firstAttribute == NSLayoutAttribute.Height }).first { H.constant = frame.height }
      }
      headerView.sendSubviewToBack(bgView)
    }
    
    return headerView
  }
  
  
  override func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
    let empty = CGSize(width: 0, height: 0)
    return self.sectionHeaderTitleAtIndexPath(section) == nil ? empty : super.collectionView(collectionView, layout: collectionViewLayout, referenceSizeForFooterInSection: section)
  }
  
  
  private func sectionHeaderTitleAtIndexPath(section: Int, title: String? = nil) -> String? {
    //    return title?.lowercaseString.capitalizedString
    //    if let title = title?.characters.split(isSeparator: { $0 == " " }).map(String.init).first {
    //      if let titleSection = self.sectionHeaderTitles[title] {
    //        if section > titleSection {
    //          self.sectionHeaderTitles[title] = section
    //          self.collectionView?.performBatchUpdates({ () -> Void in }, completion: nil) }
    //      }
    //      else {
    //        self.sectionHeaderTitles[title] = section }
    //
    //    }
    return ((self.sectionHeaderTitles as NSDictionary).allKeysForObject(section) as? [String])?.first?.lowercaseString.capitalizedString
    
  }
  
  
  override func updateDataSourceWithMessages(messages: [QBChatMessage]!) -> [NSObject : AnyObject]! {
    let result = super.updateDataSourceWithMessages(messages)
    if let sections = super.valueForKey("chatSections") as? [QMChatSection] {
      let titles = sections.map({ QMDateUtils.formattedStringFromDate($0.lastMessageDate) })
        .map({ $0.stringByReplacingOccurrencesOfString(" " + ($0.componentsSeparatedByString(" ").last ?? ""), withString: "")  })
      if titles.count > 0 {
        for i in 0..<titles.count {
          self.sectionHeaderTitles[titles[i]] = i
        }
      }
    }
    
    return result
  }
  
  
  func updateHeaders() {
    guard let collectionView = self.collectionView else { return }
    let count = super.numberOfSectionsInCollectionView(collectionView)
    //    let allHeaders = super.collec
  }
  
  
  
  //  MARK: -
  //  MARK: Collection view DataSource
  override func collectionView(collectionView: QMChatCollectionView!, dynamicSizeAtIndexPath indexPath: NSIndexPath!, var maxWidth: CGFloat) -> CGSize {
    let item = self.messageForIndexPath(indexPath)
    let viewClass = self.viewClassForItem(item)
    var size = CGSizeZero
    
    
    if viewClass == QMChatAttachmentIncomingCell.self {
      size = CGSizeMake(min(200, maxWidth), 200) }
      
    else if viewClass == QMChatAttachmentOutgoingCell.self {
      let attributedString = self.bottomLabelAttributedStringForItem(item)
      let bottomLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSizeMake(min(200, maxWidth), CGFloat.max), limitedToNumberOfLines: 0)
      size = CGSizeMake(min(200, maxWidth), 200 + bottomLabelSize.height)
      
    }
      
    else {
      let attributedString = self.attributedStringForItem(item)
      
      maxWidth = min(maxWidth, self.view.frame.width - 58)
      if viewClass == CCWSOutcomingChatCell.self { maxWidth -= 20 + 37 }
      else if viewClass == CCWSIncomingChatCell.self { maxWidth -= 20 + 45 }
      
      size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSizeMake(maxWidth, CGFloat.max), limitedToNumberOfLines: 0)
      
      size.height += 8 + 4
      if viewClass == CCWSOutcomingChatCell.self { size.width += 20 + 37;  }
      else if viewClass == CCWSIncomingChatCell.self { size.width += 20 + 45; /*size.height += (item.readIDs?.count > 1 ? 0 : 11)*/ }
     
    }
    
    return size
  }
  
  
  override func collectionView(collectionView: QMChatCollectionView!, minWidthAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
    guard let item = self.messageForIndexPath(indexPath) else { return 0 }
    
    let attributedString = item.senderID == self.senderID ? self.bottomLabelAttributedStringForItem(item) : self.topLabelAttributedStringForItem(item)
    let size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSizeMake(1000, 10000), limitedToNumberOfLines: 1)
    
    return max(size.width, 31)
  }
  
  
  
  //  MARK: -
  //  MARK: Tool bar actions
  override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: UInt, senderDisplayName: String!, date: NSDate!) {
    let message = QBChatMessage.markableMessage()
    
    message.text = text
    message.senderID = senderID
    message.deliveredIDs = [NSNumber(unsignedInteger: self.senderID)]
    message.readIDs = [NSNumber(unsignedInteger: self.senderID)]
    message.dialogID = self.dialog?.ID
    message.dateSent = date
    
    QMAPI.instance().chatService.sendMessage(message, toDialogID: self.dialog?.ID, saveToHistory: true, saveToStorage: true) { (error: NSError?) -> Void in
      if let error = error {
        MessageCenter.sharedInstance.showError(error.localizedRecoverySuggestion ?? error.localizedDescription)
      }
      else {
        //        self.insertMessageToTheBottomAnimated(message)
      }
      
    }
    
    self.finishSendingMessageAnimated(true)
    
  }
  
  
  
  //  MARK: -
  //  MARK: ChatCollectionViewDelegate
  override func collectionView(collectionView: QMChatCollectionView!, layoutModelAtIndexPath indexPath: NSIndexPath!) -> QMChatCellLayoutModel {
    let layoutModel = super.collectionView(collectionView, layoutModelAtIndexPath: indexPath)
    return layoutModel
  }
  
  
  override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
    
    let lastSection = collectionView.numberOfSections() - 1
    if indexPath.section == lastSection && indexPath.item == collectionView.numberOfItemsInSection(lastSection) - 1 {
      self.loadEarlierMessages()
    }
    
    let itemMessage = self.messageForIndexPath(indexPath)
    self.sendReadStatusForMessage(itemMessage)

    if let cell = cell as? CCWSIncomingChatCell {
      cell.read = itemMessage.readIDs?.count > 1
    }
    
    NSLog("\(itemMessage.text) - (\(itemMessage.read))")
    
    
  }
  
  
  override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    switch action {
    case #selector(NSObject.copy(_:)) :
      let message = self.messageForIndexPath(indexPath)
      UIPasteboard.generalPasteboard().string = message.text ?? ""
    default: break
    }
    
  }
  
  
  override func scrollViewWillBeginDragging(scrollView: UIScrollView) {
    if scrollView.panGestureRecognizer.translationInView(scrollView.superview).y < 0 {}
    else { self.inputToolbar?.contentView?.textView?.resignFirstResponder() }
  }
  
  
}



//  MARK: -
//  MARK: QMChatServiceDelegate
extension CCWhiteChatViewController: QMChatServiceDelegate {
  
  
  func chatService(chatService: QMChatService!, didAddMessageToMemoryStorage message: QBChatMessage!, forDialogID dialogID: String!) {
    if self.dialog?.ID == dialogID {
      self.insertMessageToTheBottomAnimated(message)
    }
    
  }
  
  
  func chatService(chatService: QMChatService!, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog!) {
    if self.dialog?.ID == chatDialog.ID {
      self.dialog = chatDialog
      
      if self.dialog?.type == QBChatDialogType.Private {
        self.title = self.dialog?.name
      }
    }
    
  }
  
  
  func chatService(chatService: QMChatService!, didUpdateMessage message: QBChatMessage!, forDialogID dialogID: String!) {
    if self.dialog?.ID == dialogID && message.senderID == self.senderID {
      self.updateMessage(message)
    }
    
  }
  
  
}



//  MARK: -
//  MARK: QMChatConnectionDelegate
extension CCWhiteChatViewController: QMChatConnectionDelegate {
  
  
  func refreshAndReadMessages() {
    self.refreshMessagesShowingProgress(true)
    
    self.readMessages(self.unreadMessages)
    self.unreadMessages = []
  }
  
  
  func chatServiceChatDidConnect(chatService: QMChatService!) {
    self.refreshAndReadMessages()
  }
  
  
  func chatServiceChatDidReconnect(chatService: QMChatService!) {
    self.refreshAndReadMessages()
  }
  
  
}



//  MARK: -
//  MARK: QMChatAttachmentServiceDelegate
extension CCWhiteChatViewController: QMChatAttachmentServiceDelegate {
  
  
  func chatAttachmentService(chatAttachmentService: QMChatAttachmentService!, didChangeAttachmentStatus status: QMMessageAttachmentStatus, forMessage message: QBChatMessage!) {
    
    if message.dialogID == self.dialog?.ID {
      self.updateMessage(message)
    }
    
  }
  
  
  func chatAttachmentService(chatAttachmentService: QMChatAttachmentService!, didChangeLoadingProgress progress: CGFloat, forChatAttachment attachment: QBChatAttachment!) {
    
  }
  
  
  func chatAttachmentService(chatAttachmentService: QMChatAttachmentService!, didChangeUploadingProgress progress: CGFloat, forMessage message: QBChatMessage!) {
    
  }
  
  
}



//  MARK: -
//  MARK: UITextViewDelegate
extension CCWhiteChatViewController {
  
  
  override func textViewDidBeginEditing(textView: UITextView) {
    super.textViewDidBeginEditing(textView)
    self.configureToolbar(true)
  }
  
  
  override func textViewDidEndEditing(textView: UITextView) {
    super.textViewDidEndEditing(textView)
    self.configureToolbar(false)
  }
  
  
}



// - MARK: QMContactListServiceDelegate
extension CCWhiteChatViewController: QBChatDelegate {

  
  func chatDidReceiveContactItemActivity(userID: UInt, isOnline: Bool, status: String?) {
    if dialog?.recipientID == Int(userID) {
      self.navigationItem.titleView = self.customNavigationTitleView()
    }
    
  }

  
  

  
}