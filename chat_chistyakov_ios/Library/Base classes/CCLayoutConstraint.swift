//
//  CCLayoutConstraint.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 16.03.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCLayoutConstraint: NSLayoutConstraint {

  
  var baseValue: CGFloat { return 1 }
  var realValue: CGFloat { return 1 }
  
  
  private var scale: CGFloat { return self.realValue / self.baseValue }
  
  
  override var constant: CGFloat {
    get { return super.constant * self.scale }
    set { super.constant = newValue }
  }
  
  
}


class CCLayoutConstraint_Horizontal_6: CCLayoutConstraint {
  override var baseValue: CGFloat { return 750 / 2 }
  override var realValue: CGFloat { return UIScreen.mainScreen().bounds.size.width }
}


class CCLayoutConstraint_Vertical_6: CCLayoutConstraint {
  override var baseValue: CGFloat { return 1334 / 2 }
  override var realValue: CGFloat { return UIScreen.mainScreen().bounds.size.height }
}


class CCLayoutConstraint_Horizontal_6_plus: CCLayoutConstraint {
  override var baseValue: CGFloat { return 1242 / 3 }
  override var realValue: CGFloat { return UIScreen.mainScreen().bounds.size.width }
}


class CCLayoutConstraint_Vertical_6_plus: CCLayoutConstraint {
  override var baseValue: CGFloat { return 2208 / 3 }
  override var realValue: CGFloat { return UIScreen.mainScreen().bounds.size.height }
}