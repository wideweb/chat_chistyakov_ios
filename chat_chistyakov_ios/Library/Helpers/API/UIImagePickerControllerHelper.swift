//
//  UIImagePickerControllerHelper.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 25.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


class UIImagePickerControllerHelper: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  
  typealias CompletionType = ((UIImage?)->())
  
  
  static let sharedInstance = UIImagePickerControllerHelper()
  
  
  private weak var viewController: UIViewController?
  private var completion: CompletionType? = nil
  
  
  func showImagePickerInViewController(viewController: UIViewController, completion: CompletionType?) {
    self.viewController = viewController
    self.completion = completion
    
    let imagePicker = UIImagePickerController()
    
    imagePicker.delegate = self
    
    imagePicker.allowsEditing = false
    imagePicker.sourceType = .PhotoLibrary
    
    viewController.presentViewController(imagePicker, animated: true, completion: nil)
    
  }
  
  
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
    picker.dismissViewControllerAnimated(true, completion: nil)
    if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
      self.completion?(pickedImage)
    } else {
      self.completion?(nil)
    }

  }
  
  
  func imagePickerControllerDidCancel(picker: UIImagePickerController) {
    picker.dismissViewControllerAnimated(true, completion: nil)
    self.completion?(nil)
  }
  
  
}