//
//  NSAttributedStringExtensions.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 14.03.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation



extension NSAttributedString {
}


func +(op1: NSAttributedString, op2: NSAttributedString) -> NSAttributedString {
  let result = NSMutableAttributedString()
  result.appendAttributedString(op1)
  result.appendAttributedString(op2)
  return result
}