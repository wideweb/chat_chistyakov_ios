//
//  CCRegisterViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 01.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCRegisterViewController: CCBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  var user: QBUUser?
  
  private var textFieldDelegate: SimpleTextFieldDelegate!
  
  private var isReisterEnabled: Bool { return
    self.nameField.text?.characters.count > 0 &&
//    self.passField.text?.characters.count > 0 &&
//    self.confirmPassField.text?.characters.count > 0 &&
    self.phoneField.text?.characters.count > 0
  }
  
  @IBOutlet private weak var nameField: UITextField!
  @IBOutlet private weak var passField: UITextField!
  @IBOutlet private weak var confirmPassField: UITextField!
  @IBOutlet private weak var phoneField: UITextField!
  @IBOutlet private weak var activity: UIActivityIndicatorView!
  @IBOutlet private weak var registerButton: UIButton!
  @IBOutlet private weak var scrollView: UIScrollView!
  @IBOutlet private weak var nameTitleLabel: UILabel!
  @IBOutlet private weak var passTitleLabel: UILabel!
  @IBOutlet private weak var confirmPassTitleLabel: UILabel!
  @IBOutlet private weak var phoneTitleLabel: UILabel!
  
  
  
  //  MARK: -
  //  MARK: VLC
  override func viewDidLoad() {
    CCLog.log("\(#file) \(#function)")
    
    super.viewDidLoad()
    self.configureInterface()
    
    // Do any additional setup after loading the view.
    textFieldDelegate = SimpleTextFieldDelegate(view: self.view)
    textFieldDelegate.delegate = self
    textFieldDelegate.addTextFields([
      self.nameField,
      self.passField,
      self.confirmPassField,
      self.phoneField
      ])

    self.updateRegisterButton()
  }

  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    self.scrollView.contentSize = CGSize(width: 320, height: 250)
  }
  
  
  func configureInterface() {
    let attributes1 = [
      NSForegroundColorAttributeName : UIColor(netHex: 0x5ed1e9),
      NSFontAttributeName : UIFont.bebasNeueLightFontOfSize(20),
      NSStrokeColorAttributeName : UIColor(netHex: 0x5ed1e9),
      NSStrokeWidthAttributeName : 3
    ]
    let attributes2 = [
      NSForegroundColorAttributeName : UIColor(netHex: 0x5ed1e9),
      NSFontAttributeName : UIFont.bebasNeueBoldFontOfSize(20)
    ]
    
    self.nameTitleLabel.attributedText = NSAttributedString(string: "U S E R  ", attributes: attributes2) + NSAttributedString(string: "N A M E", attributes: attributes1)
    self.passTitleLabel.attributedText = NSAttributedString(string: "P A S S W O R D", attributes: attributes1)
    self.confirmPassTitleLabel.attributedText = NSAttributedString(string: "C O N F IR M  ", attributes: attributes1) + NSAttributedString(string: "P A S S W O R D", attributes: attributes2)
    self.phoneTitleLabel.attributedText = NSAttributedString(string: "P H O N E  ", attributes: attributes2) + NSAttributedString(string: "N U M B E R", attributes: attributes1)
    
    
    self.nameField.layer.borderWidth = 2.5
    self.passField.layer.borderWidth = 2.5
    self.confirmPassField.layer.borderWidth = 2.5
    self.phoneField.layer.borderWidth = 2.5
    
    //  REGISTER BUTTON
    registerButton.setAttributedTitle(NSAttributedString(string: "REGISTER", attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0xde9ae8),
      NSFontAttributeName : UIFont.bebasNeueLightFontOfSize(45),
      NSStrokeWidthAttributeName : 5,
      NSStrokeColorAttributeName : UIColor(netHex: 0xde9ae8)
      ]), forState: .Normal)
    registerButton.layer.borderColor = UIColor(netHex: 0xde9ae8).CGColor
    
    let attributes3 = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.helveticaNeueCyrLightFontOfSize(13)
    ]
    
    self.nameField.attributedPlaceholder = NSAttributedString(string: "Type Username Here", attributes: attributes3)
    self.passField.attributedPlaceholder = NSAttributedString(string: "Password Here", attributes: attributes3)
    self.confirmPassField.attributedPlaceholder = NSAttributedString(string: "Confirm Password Here", attributes: attributes3)
    self.phoneField.attributedPlaceholder = NSAttributedString(string: "Cell Phone Number Here", attributes: attributes3)
    
    self.passField.hidden = true
    self.confirmPassField.hidden = true
    self.phoneField.userInteractionEnabled = false
    self.passTitleLabel.hidden = true
    self.confirmPassTitleLabel.hidden = true
    
    self.phoneField.text = self.user?.login
    
  }
  
  
  func updateRegisterButton() {
    self.registerButton.enabled = self.isReisterEnabled
  }
  
  
  
  //  MARK: -
  //  MARK: actions
  @IBAction func registerTapped(sender: UIButton) {
    CCLog.log("\(#file) \(#function)")
    
    self.nameField.resignFirstResponder()
    self.passField.resignFirstResponder()
    self.confirmPassField.resignFirstResponder()
    self.phoneField.resignFirstResponder()
    
    guard let name = nameField.text else { return }
    guard let pass = passField.text else { return }
    guard let confirmedPass = confirmPassField.text else { return }
    guard let phone = phoneField.text else { return }
    
    if name == "" { MessageCenter.sharedInstance.showError("Not entered Username"); return }
    if pass == "" { MessageCenter.sharedInstance.showError("Not entered Password"); return }
    if confirmedPass == "" { MessageCenter.sharedInstance.showError("Not entered Confirmation Password"); return }
    if phone == "" { MessageCenter.sharedInstance.showError("Not entered Phone"); return }
    
    if pass.compare(confirmedPass) != NSComparisonResult.OrderedSame {
      MessageCenter.sharedInstance.showError("Password and confirmation do not match")
      return
    }
    
    
    sender.hidden = true
    self.activity.startAnimating()
    
    QMAPI.instance().signUp(name, userPassword: pass, userPhone: phone) { (error: NSError?) -> () in
      let message = error?.localizedDescription ?? "SUCCESS"
      NSLog("SIGNUP _\(message)")
      
      
      sender.hidden = false
      self.activity.stopAnimating()
      
      if let error = error {
        MessageCenter.sharedInstance.showError(error.localizedDescription)
      } else {
        self.performSegue(Segue.segueRegisterToList)
      }
    }
    
  }
  
  
  @IBAction func updateTapped(sender: UIButton) {
    self.nameField.resignFirstResponder()
    
    guard let name = nameField.text else { return }
    guard let phone = phoneField.text else { return }
    
    if name == "" { MessageCenter.sharedInstance.showError("Not entered Username"); return }
    
    sender.hidden = true
    self.activity.startAnimating()
    
    QMAPI.instance().updateCurrentUser([
      .FullName : name,
      .Phone : phone
    ]) { (error: NSError?) -> () in
      let message = error?.localizedDescription ?? "SUCCESS"
      NSLog("SIGNUP _\(message)")
      
      sender.hidden = false
      self.activity.stopAnimating()
      
      if let error = error {
        MessageCenter.sharedInstance.showError(error.localizedDescription)
      } else {
        self.performSegue(Segue.segueRegisterToList)
      }
    }
    
  }


}



extension CCRegisterViewController : SimpleTextFieldDelegateDelegate {
  
  
  func excludingKeyboardWillShow(frame: CGRect) -> Bool {
    self.scrollView.contentSize.height = self.scrollView.frame.size.height + frame.size.height
    return false
  }
  
  
  func excludingKeyboardWillHide() -> Bool {
    self.scrollView.contentSize.height = self.scrollView.frame.size.height
    return false
  }
  

  func textField(textField: UITextField, didChangeText text: String) -> Bool {
    self.updateRegisterButton()
    return true
  }
  
  
}