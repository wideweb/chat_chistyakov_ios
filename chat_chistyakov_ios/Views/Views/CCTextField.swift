//
//  CCTextField.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 10.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

//@IBDesignable
class CCTextField: UITextField {
  
  
  @IBInspectable var borderColor: UIColor = UIColor.whiteColor() { didSet { self.updateView() } }
  
  
  var horizontalInset: CGFloat { return 13 }
  
  private var placeholderFont: UIFont? {
    var range = NSMakeRange(0, 0)
    return self.attributedPlaceholder?.attribute(NSFontAttributeName, atIndex: 0, effectiveRange: &range) as? UIFont
  }
  
  
  func updateView() {
//    let borderColor = UIColor.whiteColor()
    let textColor = UIColor.whiteColor()
    let bgColor = UIColor.clearColor()
    let font = UIFont.helveticaNeueCyrLightFontOfSize(20)
    
    self.borderStyle = .None
    
    self.layer.borderWidth = 1
    self.layer.borderColor = borderColor.CGColor

    self.layer.cornerRadius = 2
    
    self.backgroundColor = bgColor
    self.textColor = textColor
    
    self.tintColor = textColor
    
    self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [
      NSForegroundColorAttributeName : textColor.colorWithAlphaComponent(0.7),
      NSFontAttributeName : font
      ])
    
    self.font = font
    
  }
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.updateView()
  }

  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    self.updateView()
  }
  
  
  override func textRectForBounds(bounds: CGRect) -> CGRect {
    let rect = CGRectInset(super.textRectForBounds(bounds), self.horizontalInset, 0)
    if let font = self.font { return self.textRectForBounds(rect, withFont: font) }
    return rect
  }
  
  
  override func editingRectForBounds(bounds: CGRect) -> CGRect {
    let rect = CGRectInset(super.editingRectForBounds(bounds), self.horizontalInset, 0)
    if let font = self.font { return self.textRectForBounds(rect, withFont: font) }
    return rect
  }
  
  
  override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
    let rect = super.placeholderRectForBounds(bounds)
    if let font = self.placeholderFont ?? self.font { return self.textRectForBounds(rect, withFont: font) }
    return rect
  }
  
  
  func textRectForBounds(bounds: CGRect, withFont font: UIFont) -> CGRect {
    let inset = fabs( font.descender ) + fabs( font.ascender - font.capHeight )
    return CGRectInset(bounds, 0, inset)
  }
  
  
//  override func prepareForInterfaceBuilder() {
//    super.prepareForInterfaceBuilder()
//    
//    self.updateView()
//  }
  

}
