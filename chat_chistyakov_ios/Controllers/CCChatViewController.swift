//
//  CCChatViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 12.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit


class CCChatViewController: QMChatViewController {

  
  
  //  MARK: -
  //  MARK: Properties
  var dialog: QBChatDialog?
  var recipient: QBUUser?
  
  
  
  private var sectionHeaderTitles: [String : Int] = [:]
  private var unreadMessages: [QBChatMessage] = []
  private var storedMessages: [QBChatMessage] {
    return QMAPI.instance().chatService.messagesMemoryStorage.messagesWithDialogID(self.dialog?.ID) as? [QBChatMessage] ?? []
  }
  
  private var statusTimer: NSTimer?
  
  
  @IBOutlet private var statusLabel: UILabel?
  @IBOutlet private var bgView: UIImageView?
  
  
  //  MARK: -
  //  MARK: Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configureInterface()
    
    self.senderID = QMAPI.instance().currentUser().ID
    let user = QMAPI.instance().currentUser()
    self.senderDisplayName = user.fullName ?? user.login
    
    self.timeIntervalBetweenSections = 300
    self.heightForSectionHeader = 40
    
    let nibOut = UINib(nibName: "CCOutcomingChatCell", bundle: nil)
    let reuseOut = CCOutcomingChatCell.cellReuseIdentifier()
    self.collectionView?.registerNib(nibOut, forCellWithReuseIdentifier: reuseOut)
    
    let nibIn = UINib(nibName: "CCIncomingChatCell", bundle: nil)
    let reuseIn = CCIncomingChatCell.cellReuseIdentifier()
    self.collectionView?.registerNib(nibIn, forCellWithReuseIdentifier: reuseIn)
    
    self.loadDialog()
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.setupNavBar()
    
    self.retrieveMessages()
//    self.startUpdatingStatus()
  }
  
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    self.loadCachedMessages()
    
    QMAPI.instance().chatService.addDelegate(self)
    QMAPI.instance().chatService.chatAttachmentService.delegate = self
    
  }
  
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    
    self.stopUpdatingStatus()
  }
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    self.bgView?.frame = self.view.bounds
  }
  
  
  
  //  MARK: -
  //  MARK: Interface
  func setupNavBar() {
    self.navigationItem.titleView = self.customNavigationTitleView()
    
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "bg_chat_nav"), forBarMetrics: .Default)
    self.navigationController?.navigationBar.translucent = false

    self.navigationController?.navigationBar.titleTextAttributes = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.shadowFontOfSize(30)
    ]
    
    if self.navigationController?.viewControllers.count > 1 {
      let spacer = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
      spacer.width = -10
      self.navigationItem.leftBarButtonItems = [spacer, self.backButton()]
    }
    
  }
  
  
  func customNavigationTitleView() -> UIView {
    let user = QMAPI.instance().user(UInt(dialog?.recipientID ?? 0))
    let titleText = user?.fullName ?? user?.login ?? "CHAT"
    let titleAttributedText = NSAttributedString(string: titleText, attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0xfffefe),
      NSFontAttributeName : UIFont.shadowFontOfSize(53 / 2),
      NSKernAttributeName : -2.43 / 3,
      NSParagraphStyleAttributeName : {
        let style = NSMutableParagraphStyle()
        style.alignment = .Center
        style.lineBreakMode = .ByTruncatingTail
        return style
        }()
      ])
    let textRect = titleAttributedText.boundingRectWithSize(CGSize(width: 10000, height: 30), options: NSStringDrawingOptions(rawValue: 0), context: nil).width
    
    let avatarWidth: CGFloat = 30
    let avatarOffset: CGFloat = 11
    let titleHeight: CGFloat = 30
    let statusHeight: CGFloat = 10
    let width = min( UIScreen.mainScreen().bounds.width - 100, textRect + (avatarWidth + avatarOffset) * 2)
    let titleWidth: CGFloat = width - (avatarWidth + avatarOffset) * 2
    let height: CGFloat = titleHeight + statusHeight + 10 + 8
    
    let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
    view.backgroundColor = UIColor.clearColor()
    
    let avatarView = CCAvatarImageView(frame: CGRect(x: width - avatarWidth, y: 8, width: avatarWidth, height: avatarWidth))
    let avatarPlaceholder = UIImage(named: "contact_placeholder")
    avatarView.image = avatarPlaceholder
    avatarView.contentMode = .ScaleAspectFit
    view.addSubview(avatarView)
    
    
    if let urlString = QMAPI.instance().publicImageUrlWithID(user?.blobID ?? 0), let url = NSURL(string: urlString) {
      avatarView.sd_setImageWithURL(url, placeholderImage: avatarPlaceholder, completed: { (image: UIImage?, _, _, responseUrl: NSURL?) in
        if urlString == responseUrl?.absoluteString && image != nil {
          avatarView.contentMode = .ScaleAspectFit
          avatarView.image = image
        }
        else {
          avatarView.image = avatarPlaceholder
          avatarView.contentMode = .ScaleAspectFit
        }
      })
    }
    
    
    if let userID = self.dialog?.recipientID {
      self.statusLabel = UILabel(frame: CGRect(x: avatarWidth + avatarOffset, y: 0, width: titleWidth, height: statusHeight))
      if let statusLabel = self.statusLabel {
        statusLabel.font = UIFont.helveticaNeueCyrMediumFontOfSize(10)
        statusLabel.textColor = UIColor.blackColor()
        statusLabel.textAlignment = .Center
        statusLabel.text = ""//QMAPI.instance().isOnlineUserWithID(UInt(userID)) ? "Online" : "Offline"
        view.addSubview(statusLabel)
      }
    }
      
    let titleLabel = UILabel(frame: CGRect(x: avatarWidth + avatarOffset, y: statusHeight, width: titleWidth, height: titleHeight))
    titleLabel.attributedText = titleAttributedText
    titleLabel.layer.shadowColor = UIColor(netHex: 0x000000).CGColor
    titleLabel.layer.shadowOpacity = 0.5
    titleLabel.layer.shadowOffset = CGSize(width: 4 / 2, height: 1)
    titleLabel.layer.shadowRadius = 1
    view.addSubview(titleLabel)
    
//    titleLabel.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.3)
    
    return view
  }
  
  
  func backButton() -> UIBarButtonItem {
    if let nc = self.navigationController { return UIBarButtonItem.backButton(nc, action: Selector("popViewControllerAnimated:")) }
    else { return UIBarButtonItem() }
    
  }
  
  
  func configureInterface() {
    self.collectionView?.backgroundColor = UIColor.clearColor()
    
    let topSeparatorView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: 2))
    topSeparatorView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.15)
    self.view.addSubview(topSeparatorView)
    
    //  background
    bgView = UIImageView(frame: self.view.bounds)
    if let bgView = self.bgView {
      bgView.contentMode = .ScaleAspectFill
      bgView.image = UIImage(named: "bg_chat")
      self.view.addSubview(bgView)
      self.view.sendSubviewToBack(bgView)
    }
    
    //  toolbar
    let sendButton = UIButton(type: UIButtonType.Custom)
    sendButton.setImage(UIImage(named: "send"), forState: .Normal)
    self.inputToolbar?.contentView?.rightBarButtonItem = sendButton
    self.inputToolbar?.contentView?.rightBarButtonItemWidth = 40
    self.inputToolbar?.contentView?.leftBarButtonItem = nil
    self.inputToolbar?.contentView?.leftBarButtonItemWidth = 50
    //    if let layer = self.inputToolbar?.contentView?.rightBarButtonItem?.layer {
    //      layer.shadowColor = UIColor.blackColor().CGColor
    //      layer.shadowOpacity = 0.5
    //      layer.shadowRadius = 2
    //      layer.shadowOffset = CGSize(width: 1, height: 1)
    //    }
    self.inputToolbar?.setShadowImage(UIImage.imageWithColor(UIColor(netHex: 0xf2f2f2).colorWithAlphaComponent(0.4), size: CGSize(width: 1000, height: 1)), forToolbarPosition: .Any)
    
    self.configureToolbar(false)
    
    if let constraint = super.valueForKey("toolbarHeightConstraint") as? NSLayoutConstraint { constraint.constant = 49 }
    
    self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    
  }
  
  
  func configureToolbar(active: Bool) {
    let bgColor = /*active*/ false ? UIColor(netHex: 0xf0f6).colorWithAlphaComponent(0.41) : UIColor(netHex: 0xeaf1f2).colorWithAlphaComponent(0.11)
    self.inputToolbar?.setBackgroundImage(UIImage.imageWithColor(bgColor, size: CGSizeMake(1000, 49)), forToolbarPosition: .Any, barMetrics: .Default)
    
    self.inputToolbar?.contentView?.textView?.alpha = active ? 1 : 0.6
    
  }
  
  
  
  //  MARK: -
  //  MARK: QB helpers
  func startUpdatingStatus() {
    self.stopUpdatingStatus()
    
    self.updateRecipientStatus()
    self.statusTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: Selector("updateRecipientStatus"), userInfo: nil, repeats: true)
  }
  
  
  func stopUpdatingStatus() {
    self.statusTimer?.invalidate()
    self.statusTimer = nil
  }
  
  
  func updateRecipientStatus() {
    let status = QMAPI.instance().isOnlineUserWithID(UInt(dialog?.recipientID ?? 0))
    if status { self.statusLabel?.text = "online" }
    else if let lastActivityTime = QMAPI.instance().user(UInt(dialog?.recipientID ?? 0))?.lastRequestAt {
      let formatter = NSDateFormatter()
      formatter.dateFormat = "dd.MM.yy HH:mm"
      self.statusLabel?.text = formatter.stringFromDate(lastActivityTime)
    }
    else { self.statusLabel?.text = "offline" }
  }
  
  
  func loadDialog() {
    if let _ = self.dialog { return }
    
    if let recipient = self.recipient {
      QMAPI.instance().createPrivateChatDialogIfNeeded(recipient) { (chatDialog, error) -> () in
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          if let error = error {
            NSLog("\(error)")
            self.navigationController?.popViewControllerAnimated(true)
            
          } else if let dialog = chatDialog {
            self.dialog = dialog
            self.setupNavBar()
            self.retrieveMessages()
            self.loadCachedMessages()
            
          } else {
            NSLog("unknown error")
            self.navigationController?.popViewControllerAnimated(true)
            
          }
          
        })
      }
    }
    
    self.dialog?.onBlockedMessage = { error in MessageCenter.sharedInstance.showError(error?.localizedDescription ?? "") }
    
  }
  
  
  func retrieveMessages() {
    if self.storedMessages.count > 0 && self.totalMessagesCount == 0 {
      self.updateDataSourceWithMessages(self.storedMessages)
      self.refreshMessagesShowingProgress(false)
    } else {
      if self.totalMessagesCount == 0 { /*MessageCenter.sharedInstance.showLoading("Refreshing...")*/ }
      QMAPI.instance().cachedMessagesWithDialogID(self.dialog?.ID, block: { (collection: [AnyObject]!) -> Void in
        if collection.count > 0 {
          self.insertMessagesToTheBottomAnimated(collection as? [QBChatMessage] ?? [])
        }
        
        self.refreshMessagesShowingProgress(false)
      })
    }
    
  }
  
  
  func loadCachedMessages() {
    if self.storedMessages.count > 0 && Int(self.totalMessagesCount) != self.storedMessages.count {
      self.insertMessagesToTheBottomAnimated(self.storedMessages)
    }
  }
  
  
  func refreshMessagesShowingProgress(showingProgress: Bool) {
    if showingProgress /*&& self.isSendingAttachment*/ {
      /*MessageCenter.sharedInstance.showLoading("Refreshing...")*/
    }
    
    QMAPI.instance().chatService.messagesWithChatDialogID(self.dialog?.ID) { (response: QBResponse!, messages: [AnyObject]!) -> Void in
      if response.success {
        if messages.count > 0 { self.insertMessagesToTheBottomAnimated(messages as! [QBChatMessage]) }
        /*if self.isSendingAttachment { MessageCenter.sharedInstance.hideLoading() }*/
      } else {
        MessageCenter.sharedInstance.showError(response.error?.error?.localizedDescription ?? "Can not refresh messages")
      }
    }
    
  }
  
  
  func loadEarlierMessages() {
    QMAPI.instance().chatService.loadEarlierMessagesWithChatDialogID(self.dialog?.ID).continueWithBlock { (task:BFTask) -> AnyObject? in
      if task.result?.count > 0 { self.insertMessagesToTheTopAnimated(task.result as? [QBChatMessage] ?? []) }
      return nil
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: Utilities
  func sendReadStatusForMessage(message: QBChatMessage) {
    guard let readIDs = message.readIDs else { return }
    
    if message.senderID != self.senderID && !readIDs.contains(self.senderID) {
      QMAPI.instance().chatService.readMessage(message, completion: { (error: NSError?) -> Void in
        if let error = error { MessageCenter.sharedInstance.showError(error.localizedDescription) }
        
      })
    }
    
  }
  
  
  func readMessages(messages: [QBChatMessage]) {
    if QBChat.instance().isConnected() {
      QMAPI.instance().chatService.readMessages(messages, forDialogID: self.dialog?.ID, completion: { (error: NSError?) -> Void in
        if let error = error { MessageCenter.sharedInstance.showError(error.localizedDescription) }
      })
      
    } else {
      self.unreadMessages = messages
    }
    
  }
  
  
  //  MARK: -
  //  MARK: Cell classes
  override func viewClassForItem(item: QBChatMessage?) -> AnyClass? {
    guard let item = item else { return nil }
    
    if item.senderID != self.senderID {
      if item.attachments?.count > 0 || item.attachmentStatus != .NotLoaded { return QMChatAttachmentIncomingCell.self }
      else { return CCIncomingChatCell.self }
    }
    else {
      if item.attachments?.count > 0 || item.attachmentStatus != .NotLoaded { return QMChatAttachmentOutgoingCell.self }
      else { return CCOutcomingChatCell.self }
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: String builder
  override func attributedStringForItem(messageItem: QBChatMessage?) -> NSAttributedString? {
    guard let messageItem = messageItem else { return nil }
    guard let _ = messageItem.text else { return nil }
    
    let color = messageItem.senderID == self.senderID ? UIColor.whiteColor() : UIColor.blackColor()
    let font = UIFont.larsseitFontOfSize(15)
    let attributes: [String : AnyObject] = [
      NSForegroundColorAttributeName : color,
      NSFontAttributeName : font,
      NSKernAttributeName : -1.7 / 3
    ]
    let attrStr = NSAttributedString(string: messageItem.text!, attributes: attributes)
    
    return attrStr
  }
  
  
  override func topLabelAttributedStringForItem(messageItem: QBChatMessage?) -> NSAttributedString? {
    guard let messageItem = messageItem else { return nil }
    
    if messageItem.senderID == self.senderID { return nil }
    
    let color = UIColor.redColor()
    let font = UIFont(name: "Helvetica", size: 14)!
    let attributes: [String : AnyObject] = [
      NSForegroundColorAttributeName : color,
      NSFontAttributeName : font
    ]
    let attrStr = NSAttributedString(string: messageItem.senderNick ?? "", attributes: attributes)
    
    return attrStr
  }
  
  
  override func bottomLabelAttributedStringForItem(messageItem: QBChatMessage?) -> NSAttributedString? {
    guard let messageItem = messageItem else { return nil }
    
    let color = messageItem.senderID == self.senderID ? UIColor.whiteColor() : UIColor.blackColor()
    let font = UIFont.helveticaNeueCyrRomanFontOfSize(18 / 2)
    let attributes: [String : AnyObject] = [
      NSForegroundColorAttributeName : color,
      NSFontAttributeName : font,
      NSKernAttributeName : -0.73 / 3
    ]
    
    var string = ""
    if let dateSent = messageItem.dateSent {
      let formatter = NSDateFormatter(); formatter.dateFormat = "HH:mm"
      string = formatter.stringFromDate(dateSent) ?? "" }
    
    let attrStr = NSAttributedString(string: string, attributes: attributes)
    
    return attrStr
  }
  
  
  
  //  MARK: -
  //  MARK: Section headers
  override func collectionView(collectionView: QMChatCollectionView!, sectionHeaderAtIndexPath indexPath: NSIndexPath!) -> UICollectionReusableView! {
    let headerView = super.collectionView(collectionView, sectionHeaderAtIndexPath: indexPath)
    
    if let headerView = headerView as? QMHeaderCollectionReusableView {
      guard let title = self.sectionHeaderTitleAtIndexPath(indexPath.section, title: headerView.headerLabel?.text) else {
        headerView.hidden = true
        return headerView }
      let attributedTitle = NSAttributedString(string: title, attributes: [
        NSForegroundColorAttributeName : UIColor.whiteColor(),
        NSFontAttributeName : UIFont.larsseitFontOfSize(23 / 2),
        NSKernAttributeName : -0.97 / 3
        ])
      
      headerView.hidden = false
      headerView.headerLabel?.attributedText = attributedTitle
      headerView.headerLabel?.backgroundColor = UIColor.clearColor()
      
      headerView.layoutIfNeeded()
      let textRect = attributedTitle.boundingRectWithSize(CGSize(width: 10000, height: headerView.headerLabel?.frame.height ?? 15), options: NSStringDrawingOptions(rawValue: 0), context: nil)
      let frame = CGRectInset(textRect, -10, -5)
      let kBGViewTag = 1234
      let isNewBG = headerView.viewWithTag(kBGViewTag) == nil
      let bgView = headerView.viewWithTag(kBGViewTag) ?? UIView(frame: frame)
      bgView.frame = frame
      bgView.tag = kBGViewTag
      bgView.backgroundColor = UIColor(netHex:
      0x58627a
//      0x505e79
//      0x040736
      )
//      .colorWithAlphaComponent(0.62)
      bgView.center = headerView.headerLabel?.center ?? CGPointZero
      bgView.layer.shadowColor = UIColor.blackColor().CGColor
      bgView.layer.shadowOffset = CGSize(width: 1, height: 6 / 2)
      bgView.layer.shadowOpacity = 0.3
      bgView.layer.shadowRadius = 6 / 2
      bgView.layer.masksToBounds = false
      bgView.clipsToBounds = false
//      bgView.layer.borderWidth = 1
//      bgView.layer.borderColor = UIColor(netHex: 0x58627a).CGColor
      
      if isNewBG {
        bgView.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(bgView)
        let CX = NSLayoutConstraint(item: bgView, attribute: NSLayoutAttribute.CenterX, relatedBy: .Equal, toItem: headerView, attribute: .CenterX, multiplier: 1, constant: 0)
        let CY = NSLayoutConstraint(item: bgView, attribute: NSLayoutAttribute.CenterY, relatedBy: .Equal, toItem: headerView, attribute: .CenterY, multiplier: 1, constant: 0)
        let W = NSLayoutConstraint(item: bgView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: frame.width)
        let H = NSLayoutConstraint(item: bgView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: frame.height)
        headerView.addConstraints([CX, CY, W, H])
      } else {
        if let W = headerView.constraints.filter({ $0.firstAttribute == NSLayoutAttribute.Width }).first { W.constant = frame.width }
        if let H = headerView.constraints.filter({ $0.firstAttribute == NSLayoutAttribute.Height }).first { H.constant = frame.height }
      }
      headerView.sendSubviewToBack(bgView)
    }
    
    return headerView
  }
  
  
  override func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
    let empty = CGSize(width: 0, height: 0)
    return self.sectionHeaderTitleAtIndexPath(section) == nil ? empty : super.collectionView(collectionView, layout: collectionViewLayout, referenceSizeForFooterInSection: section)
  }
  
  
  private func sectionHeaderTitleAtIndexPath(section: Int, title: String? = nil) -> String? {
//    return title?.lowercaseString.capitalizedString
//    if let title = title?.characters.split(isSeparator: { $0 == " " }).map(String.init).first {
//      if let titleSection = self.sectionHeaderTitles[title] {
//        if section > titleSection {
//          self.sectionHeaderTitles[title] = section
//          self.collectionView?.performBatchUpdates({ () -> Void in }, completion: nil) }
//      }
//      else {
//        self.sectionHeaderTitles[title] = section }
//      
//    }
    return ((self.sectionHeaderTitles as NSDictionary).allKeysForObject(section) as? [String])?.first?.lowercaseString.capitalizedString
    
  }
  
  
  override func updateDataSourceWithMessages(messages: [QBChatMessage]!) -> [NSObject : AnyObject]! {
    let result = super.updateDataSourceWithMessages(messages)
    if let sections = super.valueForKey("chatSections") as? [QMChatSection] {
      let titles = sections.map({ QMDateUtils.formattedStringFromDate($0.lastMessageDate) })
        .map({ $0.stringByReplacingOccurrencesOfString(" " + ($0.componentsSeparatedByString(" ").last ?? ""), withString: "")  })
      if titles.count > 0 {
        for i in 0..<titles.count {
          self.sectionHeaderTitles[titles[i]] = i
        }
      }
    }
    
    return result
  }
  
  
  func updateHeaders() {
    guard let collectionView = self.collectionView else { return }
    let count = super.numberOfSectionsInCollectionView(collectionView)
//    let allHeaders = super.collec
  }
  
  
  
  //  MARK: -
  //  MARK: Collection view DataSource
  override func collectionView(collectionView: QMChatCollectionView!, dynamicSizeAtIndexPath indexPath: NSIndexPath!, var maxWidth: CGFloat) -> CGSize {
    let item = self.messageForIndexPath(indexPath)
    let viewClass = self.viewClassForItem(item)
    var size = CGSizeZero
    
    
    if viewClass == QMChatAttachmentIncomingCell.self {
      size = CGSizeMake(min(200, maxWidth), 200) }
      
    else if viewClass == QMChatAttachmentOutgoingCell.self {
      let attributedString = self.bottomLabelAttributedStringForItem(item)
      let bottomLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSizeMake(min(200, maxWidth), CGFloat.max), limitedToNumberOfLines: 0)
      size = CGSizeMake(min(200, maxWidth), 200 + bottomLabelSize.height)
      
    }
      
    else {
      let attributedString = self.attributedStringForItem(item)
      if viewClass == CCOutcomingChatCell.self { maxWidth -= 20 + 35 }
      else if viewClass == CCIncomingChatCell.self { maxWidth -= 20 + 30 + 20 }
      
      size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSizeMake(maxWidth, CGFloat.max), limitedToNumberOfLines: 0)
      
      size.height += 8 + 4
      if viewClass == CCOutcomingChatCell.self { size.width += 20 + 35; size.height += (item.readIDs?.count > 1 ? 0 : 11)/**/ }
      else if viewClass == CCIncomingChatCell.self { size.width += 20 + 30 + 20; size.height += 2/**/ }
      
    }
    
    return size
  }
  
  
  override func collectionView(collectionView: QMChatCollectionView!, minWidthAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
    guard let item = self.messageForIndexPath(indexPath) else { return 0 }
    
    let attributedString = item.senderID == self.senderID ? self.bottomLabelAttributedStringForItem(item) : self.topLabelAttributedStringForItem(item)
    let size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSizeMake(1000, 10000), limitedToNumberOfLines: 1)
 
    return max(size.width, 31)
  }
  
  
  override func collectionView(collectionView: QMChatCollectionView!, configureCell cell: UICollectionViewCell!, forIndexPath indexPath: NSIndexPath!) {
    super.collectionView(collectionView, configureCell: cell, forIndexPath: indexPath)
    
    if let cell = cell as? CCChatCell {
      var user: QBUUser?
      
      if let _ = cell as? CCIncomingChatCell { user = QMAPI.instance().user(UInt(self.dialog?.recipientID ?? 0)) }
      else { user = QMAPI.instance().currentUser() }
    
      let avatarPlaceholder = UIImage(named: "contact_placeholder")
      cell.avatarView.image = avatarPlaceholder
      cell.avatarView.contentMode = .Bottom
      
      
      if let urlString = QMAPI.instance().publicImageUrlWithID(user?.blobID ?? 0), let url = NSURL(string: urlString) {
        cell.avatarView.sd_setImageWithURL(url, placeholderImage: avatarPlaceholder, completed: { (image: UIImage?, _, _, responseUrl: NSURL?) in
          if urlString == responseUrl?.absoluteString && image != nil {
            cell.avatarView.contentMode = .ScaleAspectFit
            cell.avatarView.image = image
            cell.avatarView.layer.borderWidth = 0
          }
          else {
            cell.avatarView.image = avatarPlaceholder
            cell.avatarView.contentMode = .Bottom
          }
        })
      }
    }
    
  }
  
  
  //  MARK: -
  //  MARK: Tool bar actions
  override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: UInt, senderDisplayName: String!, date: NSDate!) {
    let message = QBChatMessage.markableMessage()
    
    message.text = text
    message.senderID = senderID
    message.deliveredIDs = [NSNumber(unsignedInteger: self.senderID)]
    message.readIDs = [NSNumber(unsignedInteger: self.senderID)]
    message.dialogID = self.dialog?.ID
    message.dateSent = date
    
    QMAPI.instance().chatService.sendMessage(message, toDialogID: self.dialog?.ID, saveToHistory: true, saveToStorage: true) { (error: NSError?) -> Void in
      if let error = error {
        MessageCenter.sharedInstance.showError(error.localizedRecoverySuggestion ?? error.localizedDescription)
      }
      else {
//        self.insertMessageToTheBottomAnimated(message)
      }
      
    }
    
    self.finishSendingMessageAnimated(true)
    
  }
  
  
  
  //  MARK: -
  //  MARK: ChatCollectionViewDelegate
  override func collectionView(collectionView: QMChatCollectionView!, layoutModelAtIndexPath indexPath: NSIndexPath!) -> QMChatCellLayoutModel {
    let layoutModel = super.collectionView(collectionView, layoutModelAtIndexPath: indexPath)
    return layoutModel
  }
  
  
  override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
    
    let lastSection = collectionView.numberOfSections() - 1
    if indexPath.section == lastSection && indexPath.item == collectionView.numberOfItemsInSection(lastSection) - 1 {
      self.loadEarlierMessages()
    }
    
    let itemMessage = self.messageForIndexPath(indexPath)
    self.sendReadStatusForMessage(itemMessage)
    
    if let cell = cell as? CCOutcomingChatCell {
      cell.read = itemMessage.readIDs?.count > 1
    }
    
    
  }

  
  override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    switch action {
    case #selector(NSObject.copy(_:)) :
      let message = self.messageForIndexPath(indexPath)
      UIPasteboard.generalPasteboard().string = message.text ?? ""
    default: break
    }
    
  }
  
  
  override func scrollViewWillBeginDragging(scrollView: UIScrollView) {
    if scrollView.panGestureRecognizer.translationInView(scrollView.superview).y < 0 {}
    else { self.inputToolbar?.contentView?.textView?.resignFirstResponder() }
  }
  

}



//  MARK: -
//  MARK: QMChatServiceDelegate
extension CCChatViewController: QMChatServiceDelegate {
  
  
  func chatService(chatService: QMChatService!, didAddMessageToMemoryStorage message: QBChatMessage!, forDialogID dialogID: String!) {
    if self.dialog?.ID == dialogID {
      self.insertMessageToTheBottomAnimated(message)
    }
    
  }
  
  
  func chatService(chatService: QMChatService!, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog!) {
    if self.dialog?.ID == chatDialog.ID {
      self.dialog = chatDialog
      
      if self.dialog?.type == QBChatDialogType.Private {
        self.title = self.dialog?.name
      }
    }
    
  }
  
  
  func chatService(chatService: QMChatService!, didUpdateMessage message: QBChatMessage!, forDialogID dialogID: String!) {
    if self.dialog?.ID == dialogID && message.senderID == self.senderID {
      self.updateMessage(message)
    }
    
  }
  
  
}



//  MARK: -
//  MARK: QMChatConnectionDelegate
extension CCChatViewController: QMChatConnectionDelegate {
  
  
  func refreshAndReadMessages() {
    self.refreshMessagesShowingProgress(true)
    
    self.readMessages(self.unreadMessages)
    self.unreadMessages = []
  }
  
  
  func chatServiceChatDidConnect(chatService: QMChatService!) {
    self.refreshAndReadMessages()
  }
  
  
  func chatServiceChatDidReconnect(chatService: QMChatService!) {
    self.refreshAndReadMessages()
  }

  
}



//  MARK: -
//  MARK: QMChatAttachmentServiceDelegate
extension CCChatViewController: QMChatAttachmentServiceDelegate {
  
  
  func chatAttachmentService(chatAttachmentService: QMChatAttachmentService!, didChangeAttachmentStatus status: QMMessageAttachmentStatus, forMessage message: QBChatMessage!) {
    
    if message.dialogID == self.dialog?.ID {
      self.updateMessage(message)
    }
    
  }
  
  
  func chatAttachmentService(chatAttachmentService: QMChatAttachmentService!, didChangeLoadingProgress progress: CGFloat, forChatAttachment attachment: QBChatAttachment!) {
    
  }
  
  
  func chatAttachmentService(chatAttachmentService: QMChatAttachmentService!, didChangeUploadingProgress progress: CGFloat, forMessage message: QBChatMessage!) {
    
  }
  
  
}



//  MARK: -
//  MARK: UITextViewDelegate
extension CCChatViewController {
  
  
  override func textViewDidBeginEditing(textView: UITextView) {
    super.textViewDidBeginEditing(textView)
    self.configureToolbar(true)
  }
  
  
  override func textViewDidEndEditing(textView: UITextView) {
    super.textViewDidEndEditing(textView)
    self.configureToolbar(false)
  }
  
  
}