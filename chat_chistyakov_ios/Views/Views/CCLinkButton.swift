//
//  CCLinkButton.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 10.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit


@IBDesignable
class CCLinkButton: UIButton {

  
  func updateView() {
    let titleColor = UIColor.whiteColor()
    let font = UIFont.helveticaNeueCyrLightFontOfSize(12)
    
    
    if let title = self.attributedTitleForState(.Normal)?.string {
      self.setAttributedTitle(NSAttributedString(string: title, attributes: [
        NSForegroundColorAttributeName : titleColor,
        NSFontAttributeName : font,
        NSUnderlineStyleAttributeName : NSUnderlineStyle.StyleSingle.rawValue
        ]), forState: .Normal)
    }
    
    
  }
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.updateView()
  }

  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    self.updateView()
  }
  
  
//  override func prepareForInterfaceBuilder() {
//    super.prepareForInterfaceBuilder()
//    
//    self.updateView()
//  }
  

}
