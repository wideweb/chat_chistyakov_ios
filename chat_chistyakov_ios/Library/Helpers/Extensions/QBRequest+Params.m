//
//  QBRequest+(Params).m
//  chat_chistyakov_ios
//
//  Created by Admin on 29.03.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

#import "QBRequest+Params.h"

@implementation QBRequest (Params)

- (NSString*) paramsDescription {
  return self.parameters.description;
}


- (NSString*) description {
  return @"custom request description";
}


@end
