//
//  CCChatTableViewCell.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 14.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCChatTableViewCell: UITableViewCell {

  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var nameLabel: UILabel!
  @IBOutlet private weak var lastMessageLabel: UILabel!
  @IBOutlet private weak var unreadMessagesCountLabel: UILabel!
  @IBOutlet private weak var unreadMessagesCountBorderView: UIView!
  
  
  //  MARK: -
  //  MARK: methods
  func setData(dialog: QBChatDialog) {
    let recipientName = QMAPI.instance().user(UInt(dialog.recipientID))?.login ?? "--"
    
    self.nameLabel.text = recipientName
    self.unreadMessagesCountLabel.text = dialog.unreadMessagesCount < 1000 ? "\(dialog.unreadMessagesCount)" : "999+"

    self.lastMessageLabel.text = dialog.lastMessageText
//    if let dialogID = dialog.ID { self.lastMessageLabel.text = QMAPI.instance().lastMessageFromDialogID(dialogID)?.text }
//    else { self.lastMessageLabel.text = "" }
    
  }
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    unreadMessagesCountBorderView.backgroundColor = UIColor.clearColor()
    unreadMessagesCountBorderView.layer.borderWidth = 1
    unreadMessagesCountBorderView.layer.borderColor = UIColor.whiteColor().CGColor
    unreadMessagesCountBorderView.layer.cornerRadius = 25
    
    selectionStyle = .None
  }
  
  
}
