//
//  CCLogger.m
//  chat_chistyakov_ios
//
//  Created by Admin on 21.04.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

#import "CCLog.h"
#import <CocoaLumberjack/CocoaLumberjack.h>


static const DDLogLevel ddLogLevel = DDLogLevelVerbose;


@implementation CCLog


+ (void) setupLogger {
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    
    DDFileLogger *fileLogger = [[DDFileLogger alloc] init];
    fileLogger.rollingFrequency = 60*60*24;
    fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
    [DDLog addLogger:fileLogger];
}


+ (void) log:(NSString *)string {
    DDLogVerbose(@"%@", string);
}


+ (NSData*) loggedData {
  NSUInteger maximumLogFilesToReturn = 2;
  NSMutableArray *errorLogFiles = [NSMutableArray arrayWithCapacity:maximumLogFilesToReturn];
  DDFileLogger *logger = [[DDFileLogger alloc] init];
  NSArray *sortedLogFileInfos = [logger.logFileManager sortedLogFileInfos];
  for (int i = 0; i < MIN(sortedLogFileInfos.count, maximumLogFilesToReturn); i++) {
    DDLogFileInfo *logFileInfo = [sortedLogFileInfos objectAtIndex:i];
    NSData *fileData = [NSData dataWithContentsOfFile:logFileInfo.filePath];
    [errorLogFiles addObject:fileData];
  }
  NSMutableData *errorLogData = [NSMutableData data];
  for (NSData *errorLogFileData in errorLogFiles) {
    [errorLogData appendData:errorLogFileData];
  }
  return errorLogData;
}


@end
