//
//  QBUUserExtensions.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 19.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit



private enum CustomDataKeys: String {
  case Color = "color"
}



extension QBUUser {

  
  //  check user activity at last 60 sec
  var isOnline: Bool {
    guard let lastRequestDate = self.lastRequestAt else { return false }
    let time = NSDate().timeIntervalSinceDate(lastRequestDate)
    return time < 60
  }
  
  
  var colorCode: String? {
    get {
      return self.customData?.toDictionary()[CustomDataKeys.Color.rawValue] as? String }
    set {
      var dictionary = self.customData?.toDictionary() ?? [:]
      dictionary[CustomDataKeys.Color.rawValue] = newValue ?? ""
      self.customData = dictionary.toString()
    }
  }
  
  
}



extension String {

  
  func toDictionary() -> [String: AnyObject] {
    if let data = self.dataUsingEncoding(NSUTF8StringEncoding) {
      do {
        let json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.init(rawValue: 0)) as? [String: AnyObject]
        return json ?? [:]
      } catch {  }
    }
    return [:]
  }
  
  
}



extension Dictionary {// where Key: String, Value: AnyObject {

  
  func toString() -> String {
    do {
      guard let obj = self as? AnyObject else { return "" }
      let data = try NSJSONSerialization.dataWithJSONObject(obj, options: NSJSONWritingOptions.init(rawValue: 0))
      let string = String(data: data, encoding: NSUTF8StringEncoding)
      return string ?? ""
    } catch {  }
    return ""
  }
  
  
}