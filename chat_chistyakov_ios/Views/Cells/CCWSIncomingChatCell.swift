//
//  CCWSIncomingChatCell.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 24.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCWSIncomingChatCell: CCChatCell {
  
  
  var read: Bool = false {
    didSet { self.updateReadStatus() }
  }
  
  
  @IBOutlet private weak var readStatusHeight: NSLayoutConstraint!
  @IBOutlet private weak var readStatusLabel: UILabel!
  @IBOutlet private weak var readStatusImageView: UIImageView!
  
  override class func layoutModel() -> QMChatCellLayoutModel {
    var defaultModel = super.layoutModel()
    
    defaultModel.avatarSize = CGSize(width: 0, height: 0)
    defaultModel.containerInsets = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
    defaultModel.topLabelHeight = 0
    defaultModel.bottomLabelHeight = 10
    defaultModel.spaceBetweenTextViewAndBottomLabel = -5
    
    return defaultModel
  }
  
  
  override func customizeView() {
    super.customizeView()
    
    let bgColor = UIColor(netHex: 0xdafefd)
    self.containerView?.bgColor = bgColor
    self.containerView.highlightColor = bgColor.colorWithAlphaComponent(0.6)
    self.containerView.layer.cornerRadius = 10
    self.containerView.layer.masksToBounds = true
    self.containerView?.layer.shadowOpacity = 0.0
    
    self.readStatusLabel.attributedText = NSAttributedString(string: "not read", attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0x000000).colorWithAlphaComponent(0.72),
      NSFontAttributeName : UIFont.larsseitFontOfSize(33.1 / 3),
      NSKernAttributeName : -0.83 / 3
      ])
    
  }
  
  
  func updateReadStatus() {
    if self.read {
      self.readStatusImageView.image = UIImage(named: "read_ws")
//      self.readStatusLabel.hidden = true
//      self.readStatusHeight.constant = 0
    } else {
      self.readStatusImageView.image = UIImage(named: "not_read_ws")
//      self.readStatusLabel.hidden = false
//      self.readStatusHeight.constant = 11
    }
  }
  
  
}
