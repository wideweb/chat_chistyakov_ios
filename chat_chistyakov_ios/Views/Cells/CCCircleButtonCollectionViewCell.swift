//
//  CCCircleButtonCollectionViewCell.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 16.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCCircleButtonCollectionViewCell: UICollectionViewCell {
  
  
  
  @IBOutlet private weak var button: UIButton!
  
  
  override var bounds: CGRect { didSet { self.updateView() } }
  
  
  
  func setData(button: UIButton) {
    self.resetData(true)
    
    let targets = button.allTargets()
    let controlEvents = button.allControlEvents()
    
    for target in targets {
      guard let actionsForTarget = button.actionsForTarget(target, forControlEvent: controlEvents) else { continue }
      guard let action = actionsForTarget.first else { continue }
      
      self.button.addTarget(target, action: Selector(action), forControlEvents: controlEvents)
    }
    
    let image = button.imageForState(.Normal)
    self.button.setAttributedTitle(button.attributedTitleForState(.Normal), forState: .Normal)
    self.button.setImage(image, forState: .Normal)
    self.button.backgroundColor = button.backgroundColor
    self.button.enabled = button.enabled
    self.button.alpha = button.alpha
    self.button.tag = button.tag
    
  }
  
  
  func resetData(enabled: Bool = false) {
    self.button.removeTarget(nil, action: nil, forControlEvents: .AllEvents)
    self.button.setImage(nil, forState: .Normal)
    self.button.setAttributedTitle(nil, forState: .Normal)
    self.button.enabled = enabled
    self.button.alpha = enabled ? 1.0 : 0.5
    self.button.backgroundColor = UIColor.clearColor()
    self.button.tag = 0
  }
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  
  private func updateView() {
    button.layer.cornerRadius = self.frame.size.width / 2
  }

  
}
