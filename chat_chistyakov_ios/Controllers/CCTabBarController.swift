//
//  CCTabBarController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 18.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCTabBarController: UITabBarController {

  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupNavBar()
   
    self.tabBar.shadowImage = UIImage.imageWithColor(UIColor.whiteColor().colorWithAlphaComponent(0.1), size: CGSize(width: 1000, height: 1))
    
  }

  
  func setupNavBar() {
    if self.navigationController?.viewControllers.count > 1 {
      let spacer = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
      spacer.width = -10
      self.navigationItem.leftBarButtonItems = [spacer, self.logoutButton()]
    }
    
  }
  
  
  func logoutButton() -> UIBarButtonItem {
    return UIBarButtonItem.backButton(self, action: Selector("logoutTapped:"))
    
  }
  
  
  @IBAction func logoutTapped(sender: AnyObject) {
    self.logout()
    self.navigationController?.popToRootViewControllerAnimated(true)
    
  }
  
  
  func logout() {
    QMAPI.instance().logout { () -> () in NSLog("Logged out") }
    
  }

  
}
