//
//  CCSecureTextField.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 14.03.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit


class CCSecureTextField: CCTextField {
  
  
  private var actualText: String? = ""
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    self.addTarget(self, action: Selector("editingDidBegin"), forControlEvents: .EditingDidBegin)
    self.addTarget(self, action: Selector("editingDidChange"), forControlEvents: .EditingChanged)
    self.addTarget(self, action: Selector("editingDidFinish"), forControlEvents: .EditingDidEnd)
  }
  
  
  override var text: String? {
    get {
      if self.editing || self.secureTextEntry { return super.text }
      else { return self.actualText }
    }
    set { self.actualText = newValue }
  }
  
  
  func editingDidBegin() {
    self.secureTextEntry = true
    self.text = self.actualText
  }
  
  
  func editingDidChange() {
    self.actualText = self.text
  }
  
  
  func editingDidFinish() {
    self.secureTextEntry = false
    self.actualText = self.text
    self.text = self.customPlaceholder()
  }
  
  
  func customPlaceholder() -> String? {
    return self.text.map({ _ in "*" })
  }
  
  
}
