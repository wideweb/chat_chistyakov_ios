//
//  CCWhiteContactsListViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 17.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCWhiteContactsListViewController: CCBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  private var newContacts: [QBUUser] = [] { didSet { self.updateNewContactsView() } }
  private var allContacts: [QBUUser] = []
  
  private var dataSource: QMFriendsListDataSource!
  private var isContactListLoading = false
  private var isContactListSearching: Bool { return dataSource.isSearching }
  private var visibleFlipBarIndexPath: NSIndexPath? {
    didSet {
      if let prevIndexPath = oldValue where oldValue != self.visibleFlipBarIndexPath, let cell = self.getCellForIndexPath(prevIndexPath) { cell.closeChatOptions() }
    }
  }
  private var visibleHorizontalBarIndexPath: NSIndexPath? {
    didSet {
      if let prevIndexPath = oldValue where oldValue != self.visibleHorizontalBarIndexPath, let cell = self.getCellForIndexPath(prevIndexPath) { cell.closeEditOptions() }
    }
  }
  
  
  @IBOutlet private weak var newCollectionView: UICollectionView?
  @IBOutlet private weak var allCollectionView: UICollectionView?
  @IBOutlet private weak var searchBar: UISearchBar!
//  @IBOutlet private weak var newContactsViewHeightConstraint: NSLayoutConstraint!
  
  
  @IBOutlet private weak var newContactsTitleLabel: UILabel?
  @IBOutlet private weak var contactsTitleLabel: UILabel?
  
  
  //  MARK: -
  //  MARK: VLC
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configureController()
    self.configureInterface()
    
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.setupNavBar()
    
    self.dataSource.reloadDataSource()
    
  }
  
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)

    MessageCenter.sharedInstance.hideLoading(self.view)
  }
  
  
  func configureController() {
    self.setupNotifications()
    Singletone.sharedInstance.setupData()
    
    dataSource = QMFriendsListDataSource()
    dataSource.delegate = self
    
    let cellNib = UINib(nibName: "CCWhiteContactCollectionViewCell", bundle: nil)
    self.newCollectionView?.registerNib(cellNib, forCellWithReuseIdentifier: kReusableCellID)
    self.allCollectionView?.registerNib(cellNib, forCellWithReuseIdentifier: kReusableCellID)
    
    self.newCollectionView?.emptyDataSetSource = self
    self.newCollectionView?.emptyDataSetDelegate = self
    
    self.allCollectionView?.emptyDataSetSource = self
    self.allCollectionView?.emptyDataSetDelegate = self
    
    self.newCollectionView?.dataSource = self
    self.newCollectionView?.delegate = self
    
    self.allCollectionView?.dataSource = self
    self.allCollectionView?.delegate = self
    
    self.searchBar.delegate = dataSource
        
  }
  
  
  func configureInterface() {
    //  Search bar
    self.searchBar.searchBarStyle = .Minimal
    if let textField = self.searchBar.valueForKey("_searchField") as? UITextField {
      textField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [
        NSForegroundColorAttributeName : UIColor(netHex: 0x979797),
        NSFontAttributeName : UIFont.helveticaNeueCyrItalicFontOfSize(16),
        NSBaselineOffsetAttributeName : -1
        ])
      textField.borderStyle = .None
      textField.backgroundColor = UIColor.clearColor()
//      textField.leftView = UIImageView(image: UIImage(named: "search_ws"))
    }
    else { self.searchBar.placeholder = "Search" }
    self.searchBar.setImage(UIImage(named: "search_ws"), forSearchBarIcon: .Search, state: .Normal)
    self.searchBar.barTintColor = UIColor.whiteColor()
    
    self.newContactsTitleLabel?.attributedText = NSAttributedString(string: "NEW", attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0xe92020),
      NSFontAttributeName : UIFont.pragmaticaBoldFontOfSize(21/2),
      NSKernAttributeName : 7.14/2
      ])
    self.contactsTitleLabel?.attributedText = NSAttributedString(string: "CONTACTS", attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0xb73299),
      NSFontAttributeName : UIFont.pragmaticaBoldFontOfSize(21/2),
      NSKernAttributeName : 7.14/2
      ])
    
    
    self.updateNewContactsView(false)
    
  }
  
  
  func updateNewContactsView(animated: Bool = true) {
    let height: CGFloat = 130
    let isVisible = true//self.newContacts.count > 0

    if animated {
      UIView.animateWithDuration(0.3, animations: { 
//        self.newContactsViewHeightConstraint.constant = isVisible ? 0 : -height
        self.newCollectionView?.superview?.alpha = isVisible ? 1 : 0
        self.view.layoutIfNeeded()
        }, completion: { (_) in
          
      })
    }
    else {
//      self.newContactsViewHeightConstraint.constant = isVisible ? 0 : -height
      self.newCollectionView?.superview?.alpha = isVisible ? 1 : 0
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: Other
  func setupNavBar() {
    UIApplication.sharedApplication().statusBarStyle = .Default
    self.navigationController?.navigationBar.tintColor = UIColor(netHex: 0x2ecbde)
    self.navigationController?.navigationBar.translucent = false
    self.navigationController?.navigationBar.opaque = true
    self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
    
//    let navigationItem = self.navigationItem
//    let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
//    titleLabel.textAlignment = .Center
//    navigationItem.titleView = UIImageView(image: UIImage(named: "contacts_title_ws"))
//    navigationItem.titleView = titleLabel
//    titleLabel.attributedText = NSAttributedString(string: "Contacts", attributes: [
//      NSForegroundColorAttributeName : UIColor(netHex: 0xf5e9f3),
//      NSFontAttributeName : UIFont.harlowSolidFontOfSize(33),
//      NSStrokeWidthAttributeName : -2.0,
//      NSStrokeColorAttributeName : UIColor(netHex: 0xdf59a8)
//    ])
  
    let addItem = self.addButton()
    let paramsItem = self.settingButton()
    let spacer = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
    spacer.width = -10
    
    navigationItem.rightBarButtonItems = [spacer, addItem]
    navigationItem.leftBarButtonItems = [paramsItem]
    
    
//    if self.navigationController?.viewControllers.count > 1 {
//      self.navigationItem.leftBarButtonItems = [spacer, UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)]//self.logoutButton()]
//      self.navigationController?.interactivePopGestureRecognizer?.enabled = false
//    }
   
    self.title = ""
    
  }
  
  
  func logoutButton() -> UIBarButtonItem {
    let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 40))
    backButton.setImage(UIImage(named: "left_arrow_ws"), forState: .Normal)
    backButton.contentEdgeInsets = UIEdgeInsets(top: -1/2, left: -4/2, bottom: 1/2, right: 4/2)
    backButton.addTarget(self, action: #selector(CCWhiteContactsListViewController.logoutTapped(_:)), forControlEvents: .TouchUpInside)
    
    return UIBarButtonItem(customView: backButton)
    
  }
  
  
  func addButton() -> UIBarButtonItem {
    let addButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    addButton.setAttributedTitle(NSAttributedString(string: "Add", attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0x2ecbde),
      NSFontAttributeName : UIFont.helveticaNeueCyrMediumFontOfSize(33/2)
      ]), forState: .Normal)
    addButton.titleLabel?.textAlignment = .Right
    addButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5/2, bottom: 0, right: -5/2)
    addButton.addTarget(self, action: #selector(CCWhiteContactsListViewController.addTapped(_:)), forControlEvents: .TouchUpInside)
    return UIBarButtonItem(customView: addButton)
  }
  
  
  
  func settingButton() -> UIBarButtonItem {
    let settingButton = UIBarButtonItem(image: UIImage(named: "menu"), style: .Plain, target: self, action: #selector(CCWhiteContactsListViewController.paramsTapped(_:)))
    settingButton.tintColor = UIColor(netHex: 0x2ecbde)
    
    return settingButton
  }
  
  
  @IBAction func logoutTapped(sender: AnyObject) {
    self.logout()
    UIApplication.sharedApplication().statusBarStyle = .LightContent
    self.navigationController?.popToRootViewControllerAnimated(true)
    
  }
  
  
  func logout() {
    QMAPI.instance().logout { () -> () in NSLog("Logged out") }
    
  }

  
  
  //  MARK: -
  //  MARK: actions
  @IBAction func addTapped(sender: AnyObject) {
    self.performSegueWithIdentifier(kSegueContactsToAddID, sender: nil)
  }
  
  
  @IBAction func paramsTapped(sender: AnyObject) {
    self.showMenu()
  }
  
  
  @IBAction func searchTapped(sender: AnyObject) {
    if self.dataSource.isSearching {
      self.dataSource.isSearching = false
      self.searchBar.hidden = true
//      self.searchBarTopConstraint.constant = -self.searchBar.bounds.size.height 
    } else {
      self.dataSource.isSearching = true
      self.searchBar.hidden = false
//      self.searchBarTopConstraint.constant = 0
    }
    
  }
  
  
  // - MARK: PopMenu
  override func changeStyle() {
    UIApplication.sharedApplication().statusBarStyle = .LightContent
    
    if let controller = self.navigationController?.viewControllers.filter({ $0 as? CCContactsListViewController != nil }).first {
      self.navigationController?.popToViewController(controller, animated: true)
    }
    else { self.performSegue(Segue.segueStyleToDefault) }
  }
  
  
  
  // - MARK: Other
  func getCellForIndexPath(indexPath: NSIndexPath) -> CCContactCellInterface? {
    let row = indexPath.row
    let section = indexPath.section
    
    let collectionView: UICollectionView?
    switch section {
    case 0: collectionView = self.newCollectionView
    case 1: collectionView = self.allCollectionView
    default: collectionView = nil
    }
    
    return collectionView?.dequeueReusableCellWithReuseIdentifier(kReusableCellID, forIndexPath: NSIndexPath(forRow: row, inSection: 0)) as? CCContactCellInterface
  }
  
  
  
  // - MARK: Notifications
  func setupNotifications() {
    let notificationCenter = NSNotificationCenter.defaultCenter()
    
    notificationCenter.addObserver(self, selector: #selector(CCWhiteContactsListViewController.handleOutsideTouch(_:)), name: kUserTouchNotificationName, object: nil)
    
  }
  
  
  func handleOutsideTouch(notification: NSNotification) {
    if let wPoint = (notification.object as? NSValue)?.CGPointValue() {
      let cPoint = self.searchBar.convertPoint(wPoint, fromView: UIApplication.sharedApplication().keyWindow)
      if !self.searchBar.bounds.contains(cPoint) {
        self.searchBar.resignFirstResponder()
      }
    }
    
  }
  
  
}



//  MARK: -
//  MARK: QMFriendsListDataSourceDelegate
extension CCWhiteContactsListViewController: QMFriendsListDataSourceDelegate {
  
  
  func didStartLoading() {
    self.isContactListLoading = true
    self.newCollectionView?.reloadEmptyDataSet()
    self.allCollectionView?.reloadEmptyDataSet()
  }
  
  
  func didReloadDataSource() {
    let number = dataSource.contactList.count
    
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
//      if !self.isContactListSearching { self.searchBar.userInteractionEnabled = !separatorsHidden }
      
//      if self.isContactListSearching {
//        self.searchBar.superview?.alpha = 1 }
//      else if number != 0 {
//        self.searchBar.superview?.alpha = 0.5 }
//      else {
//        self.searchBar.superview?.alpha = 0 }
    }
    
    self.isContactListLoading = false
    
    var historyUsers = [(NSDate, QBUUser)]()
    for user in self.dataSource.contactList {
      if let chat = QMAPI.instance().privateDialogWithUser(user.ID), let lastMessageDate = chat.lastMessageDate {
        historyUsers += [(lastMessageDate, user)] }
    }
    
    self.newContacts = historyUsers.sort({ $0.0.0.compare($0.1.0) == .OrderedDescending }).map({ $0.1 })
    self.allContacts = self.dataSource.contactList
    
    self.newCollectionView?.reloadData()
    self.allCollectionView?.reloadData()
  }
  
  
  func saveUserChatHistory() {
    
  }
  
  
}



//  MARK: -
//  MARK: CCContactTableViewCellDelegate
extension CCWhiteContactsListViewController: CCContactCellDelegate {
  
  
  func didTapChatWithUser(user: QBUUser) {
    MessageCenter.sharedInstance.showLoading("Loading...", view: self.view)
    
    QMAPI.instance().chatWithUser(user) { (chatDialog, error) -> () in
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        MessageCenter.sharedInstance.hideLoading(self.view)
        if let error = error { MessageCenter.sharedInstance.showError(error.localizedDescription) }
        else { self.performSegueWithIdentifier(kSegueContactsToChatID, sender: chatDialog) }
      })
      
    }
    
  }
  
  
  func didTapRemoveWithUser(user: QBUUser) {
    QMAPI.instance().removeUserFromContactList(user) { (success) -> () in
      if success {
        self.dataSource.reloadDataSource()
        MessageCenter.sharedInstance.showSuccess("Contact removed") }
      else { MessageCenter.sharedInstance.showError("Contact not removed") }
    }
    
  }
  
  
  func didTapBlockWithUser(user: QBUUser) {
    
  }
  
  
  func willShowEditOptionsForCell(cell: CCContactCellInterface) {
    guard let cell = cell as? UICollectionViewCell else { return }
    if let indexPath = self.newCollectionView?.indexPathForCell(cell) {
      self.visibleHorizontalBarIndexPath = NSIndexPath(forRow: indexPath.row, inSection: 0)
    }
    else if let indexPath = self.allCollectionView?.indexPathForCell(cell) {
      self.visibleHorizontalBarIndexPath = NSIndexPath(forRow: indexPath.row, inSection: 1)
    }
    
  }
  
  
  func willShowChatOptionsForCell(cell: CCContactCellInterface) {
    guard let cell = cell as? UICollectionViewCell else { return }
    if let indexPath = self.newCollectionView?.indexPathForCell(cell) {
      self.visibleFlipBarIndexPath = NSIndexPath(forRow: indexPath.row, inSection: 0)
    }
    else if let indexPath = self.allCollectionView?.indexPathForCell(cell) {
      self.visibleFlipBarIndexPath = NSIndexPath(forRow: indexPath.row, inSection: 1)
    }
    
  }
  
  
  func mainView() -> UIView? {
    return self.view
  }
  
  
}



// - MARK: UICollectionViewDataSource
extension CCWhiteContactsListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
    
  }
  
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if collectionView == self.newCollectionView { return self.newContacts.count }
    else if collectionView == self.allCollectionView { return self.allContacts.count }
    else { return 0 }
    
  }
  
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(kReusableCellID, forIndexPath: indexPath)
    return cell
  }
  
  
  func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
    var contact: QBUUser? = nil
    if collectionView == self.newCollectionView { contact = self.newContacts[indexPath.row] }
    else if collectionView == self.allCollectionView { contact = self.allContacts[indexPath.row] }
    
    if let user = contact, let cell = cell as? CCWhiteContactCollectionViewCell {
      cell.delegate = self
      cell.setContact(user, isNew: collectionView == self.newCollectionView)
    }
    
  }
  
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? CCContactCellInterface {
      cell.toggleChatOptions()
    }
    
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return CGSize(width: 160 / 2, height: 185 / 2)
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 7/2
  }
  
  
//  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
//    return 38/2
//  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 15/2, bottom: 0, right: 15/2)
  }
  
  
}



//  MARK: -
//  MARK: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
extension CCWhiteContactsListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
  
  
  func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
    let attributes = [
      NSForegroundColorAttributeName : UIColor.blackColor(),
      NSFontAttributeName : UIFont.helveticaNeueCyrLightFontOfSize(15)
    ]
    
    if self.isContactListLoading { return NSAttributedString() }
    if self.isContactListSearching { return NSAttributedString(string: "No contacts", attributes: attributes) }
    if scrollView == self.newCollectionView { return NSAttributedString(string: "No unread messages", attributes: attributes) }
    else { return NSAttributedString(string: "Click to add a new contact", attributes: attributes) }
  }
  
  
  func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
    if self.isContactListLoading { return nil }
    if self.isContactListSearching { return nil }
    
    let s = 90
    
    if scrollView == self.newCollectionView { return nil }
    else { return UIImage(named: "import_contacts")?.resizeTo(CGSize(width: s, height: s)) }
    
  }
  
  
  
  func customViewForEmptyDataSet(scrollView: UIScrollView!) -> UIView! {
    if !self.isContactListLoading { return nil }
    
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .White)
    activity.startAnimating()
    return activity
  }
  
  
  func emptyDataSetDidTapView(scrollView: UIScrollView!) {
    if scrollView == self.newCollectionView { return  }
    else { self.performSegueWithIdentifier(kSegueContactsToAddID, sender: nil) }
    
  }
  
  
  func verticalOffsetForEmptyDataSet(scrollView: UIScrollView!) -> CGFloat {
    if scrollView == self.newCollectionView { return 0 }
    else { return -40 }
    
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension CCWhiteContactsListViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == kSegueContactsToChatID {
      let controller = segue.destinationViewController as! CCWhiteChatViewController
      switch sender {
      case let dialog  as QBChatDialog: controller.dialog = dialog
      case let user as QBUUser: controller.recipient = user
      default: break
      }
      
    }
    else if segue.identifier == kSegueListToDetailID {
      let controller = segue.destinationViewController as! CCContactDetailViewController
      let indexPath = sender as! NSIndexPath
      let contact = self.dataSource.contactList[indexPath.row]
      controller.contact = contact
    }
    
  }
  
  
}