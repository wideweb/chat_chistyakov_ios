//
//  CCOutcomingChatCell.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 18.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCOutcomingChatCell: CCChatCell {

  
  var read: Bool = false {
    didSet { self.updateReadStatus() }
  }
  
  
  @IBOutlet private weak var readStatusHeight: NSLayoutConstraint!
  @IBOutlet private weak var readStatusLabel: UILabel!
  
  
  override func customizeView() {
    super.customizeView()

    let bgColor = UIColor(netHex:
      0x58627a
      //      0x505e79
      //      0x040736
    )
    //      .colorWithAlphaComponent(0.62)
    
    //    self.containerView?.layer.borderWidth = 1
    //    self.containerView?.layer.borderColor = UIColor(netHex: 0x58627a).CGColor
    self.containerView?.bgColor = bgColor
    self.containerView.highlightColor = bgColor.colorWithAlphaComponent(0.5)
    
    self.readStatusLabel.attributedText = NSAttributedString(string: "not read", attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0x000000).colorWithAlphaComponent(0.72),
      NSFontAttributeName : UIFont.larsseitFontOfSize(33.1 / 3),
      NSKernAttributeName : -0.83 / 3
      ])

  }
  
  
  func updateReadStatus() {
    if self.read {
      self.readStatusLabel.hidden = true
      self.readStatusHeight.constant = 0
    } else {
      self.readStatusLabel.hidden = false
      self.readStatusHeight.constant = 11
    }
  }
  
  
}
