//
//  QMUsersService.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation



extension QMUsersService {

  
  func searchUsersWithPhoneNumbers(phoneNumbers: [String]) -> BFTask {
    return self.searchUsersWithPhoneNumbers(phoneNumbers, page: QBGeneralResponsePage(currentPage: 1, perPage: 100))
  }
  
  
  func searchUsersWithPhoneNumbers(phoneNumbers: [String], page: QBGeneralResponsePage) -> BFTask {
    return self.loadFromCache().continueWithBlock({ (task: BFTask) -> AnyObject? in
      let source = BFTaskCompletionSource()
      
      QBRequest.usersWithPhoneNumbers(phoneNumbers, page: page,
        successBlock: { (response: QBResponse, page: QBGeneralResponsePage?, users: [QBUUser]?) -> Void in
          self.usersMemoryStorage.addUsers(users)
          source.setResult(users)
          
        }, errorBlock: { (response: QBResponse) -> Void in
          guard let error = response.error?.error else { return }
          source.setError(error)
          
      })
      
      return source.task
    })
  }
  
  
  func searchUsersWithLogins(logins: [String]) -> BFTask {
    return self.searchUsersWithLogins(logins, page: QBGeneralResponsePage(currentPage: 1, perPage: 100))
  }
  
  
  func searchUsersWithLogins(logins: [String], page: QBGeneralResponsePage) -> BFTask {
    return self.loadFromCache().continueWithBlock({ (task: BFTask) -> AnyObject? in
      let source = BFTaskCompletionSource()
      
      QBRequest.usersWithLogins(logins, page: page,
        successBlock: { (response: QBResponse, page: QBGeneralResponsePage?, users: [QBUUser]?) -> Void in
          self.usersMemoryStorage.addUsers(users)
          source.setResult(users)
          
        }, errorBlock: { (response: QBResponse) -> Void in
          guard let error = response.error?.error else { return }
          source.setError(error)
          
      })
      
      return source.task
    })
  }
  
  
}