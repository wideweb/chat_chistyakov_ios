//
//  CCSigninViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 01.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCSigninViewController: CCBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  private var textFieldDelegate: SimpleTextFieldDelegate!
  
  private var isLoginEnabled: Bool { return nameField.text?.characters.count > 7 /*&& passField.text?.characters.count > 0*/ }
  
  
  @IBOutlet private weak var loginTitleLabel: UILabel!
  @IBOutlet private weak var nameField: UITextField!
  @IBOutlet private weak var passField: UITextField!
  @IBOutlet private weak var checkBox: CCCheckBox!
  @IBOutlet private weak var signInButton: CCRectButton!
  @IBOutlet private weak var orLabel: UILabel!
  @IBOutlet private weak var signUpButton: CCRectButton!
  @IBOutlet private weak var activity: UIActivityIndicatorView!
  @IBOutlet private weak var mainViewCenterYConstraint: NSLayoutConstraint!
  @IBOutlet private weak var rememberMeTitleLabel: UILabel!
  
  
  
  //  MARK: -
  //  MARK: VLC
  override func viewDidLoad() {
    CCLog.log("\(#file) \(#function)")
    
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.configureInterface()
    
    textFieldDelegate = SimpleTextFieldDelegate(view: self.view)
    textFieldDelegate.delegate = self
    textFieldDelegate.addTextFields([self.nameField, self.passField])
    
    self.updateLoginButton()
  
    if let phone = QMAPI.instance().savedLogin {
      //  autologin
      self.nameField.text = phone
      QMAPI.instance().autoLogIn { (error: NSError?) in self.signInCompletion(error) }
    }
    
  }
  
  
  override func viewWillAppear(animated: Bool) {
    CCLog.log("\(#file) \(#function)")
    
    super.viewWillAppear(animated)

    self.navigationController?.navigationBarHidden = true
  }
  
  
  override func viewWillDisappear(animated: Bool) {
    CCLog.log("\(#file) \(#function)")
    
    super.viewWillDisappear(animated)
    
    self.navigationController?.navigationBarHidden = false
  }
  
  
  func configureInterface() {
    self.loginTitleLabel.font = UIFont.bebasNeueBookFontOfSize(51)
    self.orLabel.font = UIFont.bebasNeueBoldFontOfSize(15)
    self.rememberMeTitleLabel.font = UIFont.helveticaNeueCyrLightFontOfSize(13)
    
    self.nameField.layer.borderWidth = 2
    self.passField.layer.borderWidth = 2
    
    //  LOGIN TITLE LABEL
    self.loginTitleLabel.textColor = UIColor(netHex: 0xe8e46a)
    
    //  LOGIN BUTTON
    signInButton.setAttributedTitle(NSAttributedString(string: "LOGIN", attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0xf3c35c),
      NSFontAttributeName : UIFont.bebasNeueLightFontOfSize(45),
      NSStrokeWidthAttributeName : 2,
      NSStrokeColorAttributeName : UIColor(netHex: 0xf3c35c)
      ]), forState: .Normal)
    signInButton.layer.borderColor = UIColor(netHex: 0xf3c35c).CGColor
    signInButton.highlightedBgColor = UIColor(netHex: 0xe6d331).colorWithAlphaComponent(0.27)
    
    //  REGISTER BUTTON
    signUpButton.setAttributedTitle(NSAttributedString(string: "REGISTER", attributes: [
      NSForegroundColorAttributeName : UIColor(netHex: 0xde9ae8),
      NSFontAttributeName : UIFont.bebasNeueLightFontOfSize(45),
      NSStrokeWidthAttributeName : 5,
      NSStrokeColorAttributeName : UIColor(netHex: 0xde9ae8)
      ]), forState: .Normal)
    signUpButton.layer.borderColor = UIColor(netHex: 0xde9ae8).CGColor
    
    self.nameField.attributedPlaceholder = NSAttributedString(string: "Phone", attributes: [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.helveticaNeueCyrLightFontOfSize(13)
      ])
    self.nameField.keyboardType = .PhonePad
    
//    self.passField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [
//      NSForegroundColorAttributeName : UIColor.whiteColor(),
//      NSFontAttributeName : UIFont.helveticaNeueCyrLightFontOfSize(13)
//      ])
    self.passField.hidden = true
    self.signUpButton.hidden = true
    self.orLabel.hidden = true
    self.rememberMeTitleLabel.hidden = true
    
  }
  
  
  func updateLoginButton() {
    self.signInButton.enabled = self.isLoginEnabled
  }
  
  
  
  // - MARK: Navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue == Segue.segueSignInToSignUp {
      guard let user = sender as? QBUUser else { return }
      let controller = segue.destinationViewController as! CCRegisterViewController
      controller.user = user
    }
    
  }
  
  
  // - MARK: Sign in completion
  func signInCompletion(error: NSError?) {
    let message = error?.localizedDescription ?? "SUCCESS"
    NSLog("LOGIN _\(message)")
    
  
    if let error = error {
      MessageCenter.sharedInstance.showError(error.localizedDescription)
    } else {
      self.performSegue(Segue.segueLoginToWSList)
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: actions
  @IBAction func signinTapped(sender: UIButton) {
    CCLog.log("\(#file) \(#function)")
    
    self.nameField.resignFirstResponder()
    self.passField.resignFirstResponder()
    
    guard let phone = nameField.text else { return }
//    guard let pass = passField.text else { return }
    let rememberMe = true//checkBox.checkState == .MPCheckBoxStateChecked
    
    if phone == "" { MessageCenter.sharedInstance.showError("Not entered Phone"); return }
//    if pass == "" { MessageCenter.sharedInstance.showError("Not entered Password"); return }
    
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
      sender.hidden = true
      self.signUpButton.enabled = false
      
      self.activity.startAnimating()
    }
  
    QMAPI.instance().logout { () -> () in
      QMAPI.instance().loginWithTwitterDigits(phone) { (error: NSError?) -> () in
        if rememberMe { QMAPI.instance().saveUser(phone) }
        else { QMAPI.instance().clearUser() }
        
        self.signInButton.hidden = false
        self.signUpButton.enabled = true
        self.activity.stopAnimating()
        
        if let user = QMAPI.instance().currentUser() {
          if let _ = user.fullName {
            self.signInCompletion(error)
          } else {
            self.performSegue(Segue.segueSignInToSignUp, sender: user)
          }
        } else {
          self.signInCompletion(error)
        }
      }
    }

  }

  
  @IBAction func signUpTapped(sender: AnyObject) {
    CCLog.log("\(#file) \(#function)")
    
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
      self.performSegue(Segue.segueSignInToSignUp)
    }
   
  }
  

}



extension CCSigninViewController : SimpleTextFieldDelegateDelegate {

  
  func excludingKeyboardWillShow(frame: CGRect) -> Bool {
    self.changeMainViewPositionAnimatically(frame.size.height)
    return true
  }
  
  
  func excludingKeyboardWillHide() -> Bool {
    self.changeMainViewPositionAnimatically(62)
    return true
  }
  
  
  private func changeMainViewPositionAnimatically(height: CGFloat) {
    self.view.layoutIfNeeded()
    self.mainViewCenterYConstraint.constant = -height / 2
    UIView.animateWithDuration(0.7) { () -> Void in
      self.view.layoutIfNeeded()
    }
  }
  
  
  func textField(textField: UITextField, didChangeText text: String) -> Bool {
    self.updateLoginButton()
    return true
  }
  
  
}