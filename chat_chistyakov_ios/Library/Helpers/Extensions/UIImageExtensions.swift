//
//  UIImageExtensions.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 14.03.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit


extension UIImage {

  
  class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
    let rect = CGRectMake(0, 0, size.width, size.height)
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    color.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
  
  
  func resizeTo(size: CGSize) -> UIImage? {
    let image = self
    
    UIGraphicsBeginImageContext(size)
    image.drawInRect(CGRectMake(0, 0, size.width, size.height))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage
  }
  
  
  func imageWithBorder(color: UIColor) -> UIImage? {
    let size = self.size
    let rect = CGRect(origin: CGPointZero, size: size)
    
    UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
    let context = UIGraphicsGetCurrentContext()
    
    CGContextScaleCTM(context, 1, -1)
    CGContextTranslateCTM(context, 0, -size.height)
    
    CGContextClipToMask(context, rect, self.CGImage)
    CGContextSetFillColorWithColor(context, UIColor.whiteColor().CGColor)
    CGContextFillRect(context, rect)
    CGContextSetStrokeColorWithColor(context, color.CGColor)
    CGContextStrokeRectWithWidth(context, rect, 2)
    
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
  
  
  func imageWithCornerRadius(radius: CGFloat) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(self.size, false, 0)
    
    let rect = CGRect(origin: CGPointZero, size: self.size)
    let path = UIBezierPath(roundedRect: rect, cornerRadius: radius)
    let context = UIGraphicsGetCurrentContext()
    
    CGContextSaveGState(context)
    // Clip the drawing area to the path
    path.addClip()
    
    // Draw the image into the context
    self.drawInRect(rect)
    CGContextRestoreGState(context)
    
    let roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return roundedImage
  }
  
  
  func resizeImage(newHeight: CGFloat) -> UIImage {
//    let scale = newHeight / self.size.height
    let newWidth = self.size.width

    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
    self.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage
  }
  
  
  
  //  MARK: -
  //  MARK: Crop
  func simpleCrop(cropOrigin: CGPoint, cropSize: CGSize) -> UIImage? {
    guard cropOrigin.x >= 0 && cropOrigin.y >= 0 else { return nil }
    guard 0...self.size.width * self.scale ~= cropOrigin.x + cropSize.width &&
      0...self.size.height * self.scale ~= cropOrigin.y + cropSize.height else { return nil }
    guard let imageRef = CGImageCreateWithImageInRect(self.CGImage, CGRect(origin: cropOrigin, size: cropSize)) else { return nil }
    let image = UIImage(CGImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
    return image
  }
  
  
  func cropByHeightFromTop(cropHeight: CGFloat) -> UIImage? {
    let cropOrigin = CGPointZero
    let cropSize = CGSize(width: self.size.width, height: cropHeight)
    return self.simpleCrop(cropOrigin, cropSize: cropSize)
  }

  
  // - MARK: Overlaying
  func overlayWithColor(color: UIColor) -> UIImage {
    let colorImage = UIImage.imageWithColor(color, size: self.size)
    UIGraphicsBeginImageContextWithOptions(self.size, false, 0)
    self.drawInRect(CGRect(origin: CGPointZero, size: self.size))
    colorImage.drawInRect(CGRect(origin: CGPointZero, size: self.size))
    let result = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return result
  }
  

}
