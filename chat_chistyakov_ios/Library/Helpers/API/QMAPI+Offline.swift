//
//  QMAPI+Offline.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 23.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


var reachability: Reachability = {
  let reach = Reachability(hostName: "www.google.com")
  
  reach.startNotifier()
  return reach
}()


private var timer: NSTimer?



extension QMAPI: QMContactListServiceDelegate {

  
  // - MARK: Users
  func contactsPath(login: String) -> String {
    let manager = NSFileManager.defaultManager()
    let url = manager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    return url.URLByAppendingPathComponent(login).path!
  }
  
  
  func saveContacts(contacts: [QBUUser], forUser login: String) {
    if !reachability.isReachable() { return }
    let filePath = contactsPath(login)
    NSKeyedArchiver.archiveRootObject(contacts, toFile: filePath)
  }
  
  
  func getContactsForUser(login: String) -> [QBUUser] {
    let contacts = NSKeyedUnarchiver.unarchiveObjectWithFile(self.contactsPath(login)) as? [QBUUser]
    return contacts ?? []
  }
  
  
  func contactListService(contactListService: QMContactListService!, contactListDidChange contactList: QBContactList!) {
    if reachability.isReachable() && self.authService.isAuthorized {
      guard let login = self.currentUser().login else { return }
      self.saveContacts(QMAPI.instance().friends, forUser: login)
    }
    
  }
  
  
  func syncGetContacts() -> [QBUUser] {
    if reachability.isReachable() && self.authService.isAuthorized {
      return self.friends }
    else {
      return self.getContactsForUser(self.currentUser()?.login ?? self.savedLogin ?? "") }
  }
  
  
  func asyncGetContacts(completion: QMAPIUsersUserCompletion) {
    if reachability.isReachable() && self.authService.isAuthorized { self.safeFriends(completion) }
    else { completion(users: self.getContactsForUser(self.currentUser()?.login ?? self.savedLogin ?? ""), error: nil) }
  }
  
  
  /*SAFE UPDATE*/
  //  var timer: NSTimer?
  func update(triggerTimer: NSTimer? = nil) {
    let reloadTime = 60.0
    
    if timer == nil { //  if timer is empty - immidiately update and start reload
      let ids = self.contactListService.contactListMemoryStorage.userIDsFromContactList() as? [UInt] ?? []
//      self.safeUsersWithIDs(ids, completion: { (users, error) in })
      self.forceGetUsersWithIDs(ids)
      timer = NSTimer.scheduledTimerWithTimeInterval(reloadTime, target: self, selector: #selector(QMAPI.update), userInfo: ["mutable" : NSMutableDictionary(dictionary: ["fire":false])], repeats: false)
    } else if let _ = triggerTimer { // if timer not empty and it triggers after reload, then update
      timer = nil
      self.update()
    } else { // if update call at reloading time, then setting 'fire after reload'
      (timer?.userInfo as? [String : NSMutableDictionary])?["mutable"]?["fire"] = true
    }
  }
  
  
  func forceGetUsersWithIDs(ids: [UInt]) {
    let stringIDs = ids.map({ "\($0)" })
    let page = QBGeneralResponsePage(currentPage: 1, perPage: UInt(min(ids.count, 100)))
    QBRequest.usersWithIDs(stringIDs, page: page, successBlock: { (response: QBResponse, page: QBGeneralResponsePage?, users: [QBUUser]?) in
      self.usersService.usersMemoryStorage.addUsers(users)
      for user in users ?? [] { QMUsersCache.instance().deleteUser(user) }
      QMUsersCache.instance().insertOrUpdateUsers(users)
      QMUsersCache.instance().save({ })
      Singletone.sharedInstance.loadCachedContacts()

    }) { (response: QBResponse) in
      
    }
  }
  /**/
  
}