//
//  CCQBAPI.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


public enum UpdateUserParameter {
  case FullName
  case Phone
  case BlobID
  case Color
}


let userColors = [
  "0x009cb8",
  "0xff9a00",
  "0xffd800",
  "0x261215",
  "0xff5251",
  "0xee0000",
  "0xff3d1c",
  "0xff0093",
  "0xfff200",
  "0x004bb7",
  "0x00c5ff",
  "0xff2c05",
  "0x7f52a6",
  "0x370c30",
  "0x5fe000",
  "0x00acf2"
  
]


//  MARK: -
//  MARK: Auth
extension QMAPI {
  
  
  func signUp(userLogin: String, userPassword: String, userPhone: String, completion: QMAPIAuthSimpleCompletion?) {
    let newUser = QBUUser()
    
    newUser.login = userLogin
    newUser.password = userPassword
    newUser.phone = userPhone
    newUser.colorCode = userColors[Int(rand()) % userColors.count]
    
    self.signUp(newUser, completion: completion)
  }
  
  
  func updateCurrentUser(parameters: [UpdateUserParameter : AnyObject], completion: QMAPIAuthSimpleCompletion?) {
    let updateParameters = QBUpdateUserParameters()
    
    if let fullName = parameters[.FullName] as? String { updateParameters.fullName = fullName }
    if let phone = parameters[.Phone] as? String { updateParameters.phone = phone }
    if let blobID = parameters[.BlobID] as? Int { updateParameters.blobID = blobID }
    if let color = parameters[.Color] as? String {
      let tmp = QBUUser()
      tmp.customData = self.currentUser().customData
      tmp.colorCode = color
      updateParameters.customData = tmp.customData }
    
    QBRequest.updateCurrentUser(updateParameters, successBlock: { (response: QBResponse, user: QBUUser?) in
      if response.success {
        completion?(response.error?.error)
      }
      else {
        let error = self.customHandleErrorResponse(response)
        completion?(error)
      }
      
    }) { (response: QBResponse) in
      if response.success {
        completion?(response.error?.error)
      }
      else {
        let error = self.customHandleErrorResponse(response)
        completion?(error)
      }
    }
    
  }
  
  
  func updateCurrentUserAvatar(avatar: UIImage, statusBlock: ((Float)->())?, completion: ((NSError?)->())?) {
    guard let avatarData = UIImageJPEGRepresentation(avatar, 1) else {
      completion?(NSError(domain: "avatar", code: -109, userInfo: [
        NSLocalizedDescriptionKey : "Image type error"
        ]))
      return }
    QBRequest.TUploadFile(avatarData, fileName: "MyAvatar", contentType: "image/jpeg", isPublic: true, successBlock: { (response: QBResponse, blob: QBCBlob) in
        let blobID = blob.ID
        self.updateCurrentUser([.BlobID : blobID], completion: { (error: NSError?) in
          if response.success {
            completion?(response.error?.error)
          }
          else {
            let error = self.customHandleErrorResponse(response)
            completion?(error)
          }
        })
      }, statusBlock: { (request: QBRequest, status: QBRequestStatus?) in
        statusBlock?(status?.percentOfCompletion ?? 0)
      }) { (response: QBResponse) in
        if response.success {
          completion?(response.error?.error)
        }
        else {
          let error = self.customHandleErrorResponse(response)
          completion?(error)
        }
      }
    
  }
  
  
  func publicImageUrlWithID(blobID: Int) -> String? {
    return QBCBlob.publicUrlForID(UInt(blobID))
  }
  
  
}
