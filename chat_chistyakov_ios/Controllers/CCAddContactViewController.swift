//
//  CCAddContactViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 04.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCAddContactViewController: CCBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  private var textFieldDelegate: SimpleTextFieldDelegate!
  
  private var isAddEnabled: Bool { return self.nameField.text?.characters.count > 0 || self.phoneField.text?.characters.count > 0 }
  
  @IBOutlet private weak var nameField: UITextField!
  @IBOutlet private weak var phoneField: UITextField!
  @IBOutlet private weak var activity: UIActivityIndicatorView!
  @IBOutlet private weak var addButton: UIButton!
  @IBOutlet private weak var orLabel: UILabel!
  @IBOutlet private weak var phoneTitleLabel: UILabel!
  @IBOutlet private weak var nameTitleLabel: UILabel!
  
  
  
  //  MARK: -
  //  MARK: VLC
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.configureInterface()
    
    textFieldDelegate = SimpleTextFieldDelegate(view: self.view)
    textFieldDelegate.delegate = self
    textFieldDelegate.addTextFields([
      self.nameField,
      self.phoneField
      ])
    
    self.updateAddButton()
  }

  
  func configureInterface() {
//    self.addButton.titleLabel?.font = UIFont.bebasNeueBoldFontOfSize(38)
    
    let attributesTitle = [
      NSForegroundColorAttributeName : UIColor(netHex: 0xf0e26c),
      NSFontAttributeName : UIFont.helveticaNeueCyrLightFontOfSize(22),
      NSStrokeColorAttributeName : UIColor(netHex: 0xf0e26c),
      NSStrokeWidthAttributeName : -1
    ]
    self.phoneTitleLabel.attributedText = NSAttributedString(string: "Enter a phone number", attributes: attributesTitle)
    self.nameTitleLabel.attributedText = NSAttributedString(string: "Enter contact's Username", attributes: attributesTitle)
    
    
    self.orLabel.font = UIFont.bebasNeueBoldFontOfSize(18)
    
    self.nameField.font = UIFont.helveticaNeueCyrLightFontOfSize(22)
    self.phoneField.font = UIFont.helveticaNeueCyrLightFontOfSize(22)
    
    let attributes = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.helveticaNeueCyrLightFontOfSize(14)]
    self.nameField.attributedPlaceholder = NSAttributedString(string: "User Name Here", attributes: attributes)
    self.phoneField.attributedPlaceholder = NSAttributedString(string: "User Phone Here", attributes: attributes)
    
  }
  

  func updateAddButton() {
    let enabled = self.isAddEnabled
    self.addButton.enabled = enabled
    self.addButton.alpha = enabled ? 1 : 0.1
  }
  
  
  
  //  MARK: -
  //  MARK: actions
  @IBAction func addTapped(sender: UIButton) {
    
    let completion: QMAPIUsersUserCompletion = { (users: [QBUUser], error: NSError?) -> Void in
      if let error = error {
        sender.hidden = false
        self.activity.stopAnimating()
        
        NSLog("ERROR _\(error)")
        MessageCenter.sharedInstance.showError(error.localizedDescription)
      }
      else {
        if let user = users.first {
          QMAPI.instance().addUserToContactList(user, completion: { (success) -> () in
            sender.hidden = false
            self.activity.stopAnimating()
            
            if success {
              NSLog("Request was sended")
              self.navigationController?.popViewControllerAnimated(true) }
            else {
              NSLog("Request not sended")
              MessageCenter.sharedInstance.showError("Failed to send contact request") }
            
          })
        }
        else {
          sender.hidden = false
          self.activity.stopAnimating()
          
          let message = "User not found"
          NSLog("ERROR _\(message)")
          MessageCenter.sharedInstance.showError(message)
        } }
    }
    
    if let name = self.nameField.text {
      if name != "" {
        sender.hidden = true
        self.activity.startAnimating()
        
        QMAPI.instance().searchUsersWithLogins([name], completion: completion)
        
        return
      }
    }
    
    if let phone = self.phoneField.text {
      if phone != "" {
        sender.hidden = true
        self.activity.startAnimating()
        
        QMAPI.instance().searchUsersWithPhoneNumbers([phone], completion: completion)
        
        return
      }
    }
  
  }
  
  
}



extension CCAddContactViewController : SimpleTextFieldDelegateDelegate {
  
  
  func textField(textField: UITextField, didChangeText text: String) -> Bool {
    self.updateAddButton()
    return true
  }
  
  
}
