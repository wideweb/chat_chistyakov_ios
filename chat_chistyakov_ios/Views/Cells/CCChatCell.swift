//
//  CCChatCell.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 18.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCChatCell: QMChatCell {
  
  
  override class func layoutModel() -> QMChatCellLayoutModel {
    var defaultModel = super.layoutModel()
    
    defaultModel.avatarSize = CGSize(width: 30, height: 30)
    defaultModel.containerInsets = UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 0)
    defaultModel.topLabelHeight = 0
    defaultModel.bottomLabelHeight = 10
    defaultModel.spaceBetweenTextViewAndBottomLabel = 0
    
    return defaultModel
  }
  
  
  func customizeView() {
    self.avatarView.backgroundColor = UIColor.whiteColor()
    self.avatarView.layer.shadowRadius = 0
    self.avatarView.layer.shadowOpacity = 0
    self.avatarView.layer.borderWidth = 2.5
    self.avatarView.layer.borderColor = UIColor.whiteColor().CGColor
    
    self.containerView?.bgColor = UIColor.lightGrayColor()
    
    self.containerView?.layer.shadowColor = UIColor.blackColor().CGColor
    self.containerView?.layer.shadowOffset = CGSize(width: 1, height: 6 / 2)
    self.containerView?.layer.shadowOpacity = 0.3
    self.containerView?.layer.shadowRadius = 6 / 2
    
//    self.textView.lineBreakMode = .ByCharWrapping
//    self.bottomLabel?.backgroundColor = UIColor.greenColor().colorWithAlphaComponent(0.3)
//    self.textView?.backgroundColor = UIColor.blueColor().colorWithAlphaComponent(0.3)
//    self.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
  }
  
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    self.customizeView()
  }
  
  
}
