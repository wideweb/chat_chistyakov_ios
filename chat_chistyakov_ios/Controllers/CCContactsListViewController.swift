//
//  CCContactsListViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 02.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit


let kSegueContactsToAddID = "segueContactsToAdd"
let kSegueContactsToChatID = "segueContactsToChat"
let kSegueListToDetailID = "segueListToDetail"
let kSegueStyleToWhiteID = "segueStyleToWhite"

let kReusableCellID = "cell"


class CCContactsListViewController: CCBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  private var dataSource: QMFriendsListDataSource!
  private var isContactListLoading = false
  private var isContactListSearching: Bool { return dataSource.isSearching }
  private var visibleFlipBarIndexPath: NSIndexPath? {
    didSet {
      if let prevIndexPath = oldValue where oldValue != self.visibleFlipBarIndexPath, let cell = self.tableView?.cellForRowAtIndexPath(prevIndexPath) as? CCContactTableViewCell { cell.closeFlipButtons(true) }
    }
  }
  private var visibleHorizontalBarIndexPath: NSIndexPath? {
    didSet {
      if let prevIndexPath = oldValue where oldValue != self.visibleHorizontalBarIndexPath, let cell = self.tableView?.cellForRowAtIndexPath(prevIndexPath) as? CCContactTableViewCell { cell.closeHorizontalButtons(true) }
    }
  }
  
  
  @IBOutlet private weak var tableView: UITableView?
  @IBOutlet private weak var searchBar: UISearchBar!
  @IBOutlet private weak var searchBarTopConstraint: NSLayoutConstraint!
  
  @IBOutlet private weak var separatorView0: UIView?
  @IBOutlet private weak var separatorView1: UIView?
  
  
  //  MARK: -
  //  MARK: VLC
  override func viewDidLoad() {
    CCLog.log("\(#file) \(#function)")
    
    super.viewDidLoad()
    self.configureController()
    self.configureInterface()
    
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.setupNavBar()
    
    self.dataSource.reloadDataSource()
    
    self.navigationItem.hidesBackButton = true
    
  }
  
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    
    MessageCenter.sharedInstance.hideLoading(self.view)
  }

  
  func configureController() {
    Singletone.sharedInstance.setupData()
    
    self.view.clipsToBounds = true
    
    dataSource = QMFriendsListDataSource()
    dataSource.delegate = self
    
    self.tableView?.emptyDataSetSource = self
    self.tableView?.emptyDataSetDelegate = self
    
    self.searchBar.delegate = dataSource
    
  }
  
  
  func configureInterface() {
    //  Search bar
    if let textField = self.searchBar.valueForKey("_searchField") as? UITextField {
      textField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [
        NSForegroundColorAttributeName : UIColor(netHex: 0x000000).colorWithAlphaComponent(0.68),
        NSFontAttributeName : UIFont.helveticaNeueCyrRomanFontOfSize(14),
        NSKernAttributeName : -0.28 / 2,
        NSBaselineOffsetAttributeName : -1
        ])
    }
    else { self.searchBar.placeholder = "Search" }

  }
  
  
  
  //  MARK: -
  //  MARK: Other
  func setupNavBar() {
    let navigationItem = self.navigationItem
    
    navigationItem.title = "Contacts"
//    self.navigationController?.navigationBar.setTitleVerticalPositionAdjustment(6, forBarMetrics: .Default)
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.helveticaNeueCyrRomanFontOfSize(18)
    ]
    
    let addItem = self.addButton()
    let paramsItem = self.settingButton()
    
    navigationItem.rightBarButtonItems = [addItem]
    navigationItem.leftBarButtonItems = [paramsItem]
    
//    if self.navigationController?.viewControllers.count > 1 {
//      let spacer = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
//      spacer.width = -10
//      self.navigationItem.leftBarButtonItems = [spacer, UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)]//self.logoutButton()]
//      self.navigationController?.interactivePopGestureRecognizer?.enabled = false
//    }
    
  }
  
  
  func logoutButton() -> UIBarButtonItem {
    return UIBarButtonItem.backButton(self, action: #selector(CCContactsListViewController.logoutTapped(_:)))
    
  }
  
  
  func addButton() -> UIBarButtonItem {
    let addItem = UIBarButtonItem(image: UIImage(named: "add_contact"), style: .Plain, target: self, action: #selector(CCContactsListViewController.addTapped(_:)))
    addItem.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5)
    return addItem
  }
  
  
  func settingButton() -> UIBarButtonItem {
    let settingButton = UIBarButtonItem(image: UIImage(named: "menu"), style: .Plain, target: self, action: #selector(CCContactsListViewController.paramsTapped(_:)))
    settingButton.tintColor = UIColor.whiteColor()
    
    return settingButton
  }
  
  
  @IBAction func logoutTapped(sender: AnyObject) {
    self.logout()
    self.navigationController?.popToRootViewControllerAnimated(true)
    
  }
  
  
  func logout() {
    QMAPI.instance().logout { () -> () in NSLog("Logged out") }
    
  }

  
  override func longTapShareView(view: UIView!, didSelectShareTo item: YLShareItem!, withIndex index: UInt) {
    self.logout()
  }
  

  
  //  MARK: -
  //  MARK: actions
  @IBAction func addTapped(sender: AnyObject) {
    performSegue(Segue.segueContactsToAdd)
  }
  
  
  @IBAction func paramsTapped(sender: AnyObject) {
    self.showMenu()
//    UIImagePickerControllerHelper.sharedInstance.showImagePickerInViewController(self) { (image: UIImage?) in
//      NSLog("chosed image: \(image)")
//    }
//    performSegueWithIdentifier(kSegueStyleToWhiteID, sender: nil)
  }
  
  
  @IBAction func searchTapped(sender: AnyObject) {
    if self.dataSource.isSearching {
      self.dataSource.isSearching = false
      self.searchBar.hidden = true
      self.searchBarTopConstraint.constant = -self.searchBar.bounds.size.height }
    else {
      self.dataSource.isSearching = true
      self.searchBar.hidden = false
      self.searchBarTopConstraint.constant = 0
    }
    
  }
  
  
  // - MARK: PopMenu
  override func changeStyle() {
    if let controller = self.navigationController?.viewControllers.filter({ $0 as? CCWhiteContactsListViewController != nil }).first {
      self.navigationController?.popToViewController(controller, animated: true)
    }
    else { self.performSegue(Segue.segueStyleToWhite) }
    
  }
  
  
}



//  MARK: -
//  MARK: QMFriendsListDataSourceDelegate
extension CCContactsListViewController: QMFriendsListDataSourceDelegate {
  
  
  func didStartLoading() {
    self.isContactListLoading = true
    self.tableView?.reloadEmptyDataSet()
  }
  
  
  func didReloadDataSource() {
    let number = dataSource.contactList.count
    
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
      let separatorsHidden = number == 0
      self.separatorView0?.hidden = separatorsHidden
      self.separatorView1?.hidden = separatorsHidden
      if !self.isContactListSearching { self.searchBar.userInteractionEnabled = !separatorsHidden }
      
      if self.isContactListSearching {
        self.searchBar.superview?.alpha = 1 }
      else if number != 0 {
        self.searchBar.superview?.alpha = 0.5 }
      else {
        self.searchBar.superview?.alpha = 0 }
    }

    self.isContactListLoading = false
    self.tableView?.reloadData()
  }
  
  
}



//  MARK: -
//  MARK: CCContactTableViewCellDelegate
extension CCContactsListViewController: CCContactCellDelegate {
  
  
  func didTapChatWithUser(user: QBUUser) {
    MessageCenter.sharedInstance.showLoading("Loading...", view: self.view)
    
    QMAPI.instance().chatWithUser(user) { (chatDialog, error) -> () in
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        MessageCenter.sharedInstance.hideLoading(self.view)
        if let error = error { MessageCenter.sharedInstance.showError(error.localizedDescription) }
        else { self.performSegue(Segue.segueContactsToChat, sender: chatDialog) }
      })
      
    }
 
  }
  
  
  func didTapRemoveWithUser(user: QBUUser) {
    QMAPI.instance().removeUserFromContactList(user) { (success) -> () in
      if success {
        self.dataSource.reloadDataSource()
        MessageCenter.sharedInstance.showSuccess("Contact removed") }
      else { MessageCenter.sharedInstance.showError("Contact not removed") }
    }
    
  }
  
  
  func didTapBlockWithUser(user: QBUUser) {
    if QMAPI.instance().isUserInBlockList(user.ID) { QMAPI.instance().unblockUser(user.ID) }
    else { QMAPI.instance().blockUser(user.ID) }
    
  }
  
  
  func willShowEditOptionsForCell(cell: CCContactCellInterface) {
    guard let cell = cell as? UITableViewCell else { return }
    if let indexPath = self.tableView?.indexPathForCell(cell) {
      self.visibleHorizontalBarIndexPath = indexPath
    }
  }
  
  
  func willShowChatOptionsForCell(cell: CCContactCellInterface) {
    guard let cell = cell as? UITableViewCell else { return }
    if let indexPath = self.tableView?.indexPathForCell(cell) {
      self.visibleFlipBarIndexPath = indexPath
    }
  }
  
  
}


//  MARK: -
//  MARK: UITableViewDataSource, UITableViewDelegate
extension CCContactsListViewController: UITableViewDataSource, UITableViewDelegate {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSource.contactList.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    let contact = dataSource.contactList[indexPath.row]
    
    if let cell = cell as? CCContactTableViewCell {
      cell.delegate = self
      cell.setContact(contact)
    }

  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = tableView.cellForRowAtIndexPath(indexPath) as? CCContactTableViewCell {
      cell.toggleFlip()
    }
    
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 159 / 2
  }
  
  
}



//  MARK: -
//  MARK: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
extension CCContactsListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
  
  
  func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
    let attributes = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.helveticaNeueCyrLightFontOfSize(15)
    ]
    
    if self.isContactListLoading { return NSAttributedString() }
    if self.isContactListSearching { return NSAttributedString(string: "No contacts", attributes: attributes) }
    return NSAttributedString(string: "Click to add a new contact", attributes: attributes)
  }
  
  
  func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
    if self.isContactListLoading { return nil }
    if self.isContactListSearching { return nil }
    
    let s = 90
    return UIImage(named: "import_contacts")?.resizeTo(CGSize(width: s, height: s))
  }
  
  
  
  func customViewForEmptyDataSet(scrollView: UIScrollView!) -> UIView! {
    if !self.isContactListLoading { return nil }
    
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .White)
    activity.startAnimating()
    return activity
  }
  
  
  func emptyDataSetDidTapView(scrollView: UIScrollView!) {
     performSegue(Segue.segueContactsToAdd)
    
  }
  
  
  func verticalOffsetForEmptyDataSet(scrollView: UIScrollView!) -> CGFloat {
    return -40
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension CCContactsListViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue == Segue.segueContactsToChat {
      let controller = segue.destinationViewController as! CCChatViewController
      switch sender {
      case let dialog  as QBChatDialog: controller.dialog = dialog
      case let user as QBUUser: controller.recipient = user
      default: break
      }
      
    }
    else if segue == Segue.segueListToDetail {
      let controller = segue.destinationViewController as! CCContactDetailViewController
      let indexPath = sender as! NSIndexPath
      let contact = self.dataSource.contactList[indexPath.row]
      controller.contact = contact
    }
    
  }
  
  
}