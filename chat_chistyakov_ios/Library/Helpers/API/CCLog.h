//
//  CCLogger.h
//  chat_chistyakov_ios
//
//  Created by Admin on 21.04.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCLog : NSObject


+ (void) setupLogger;

+ (void) log: (NSString* _Nonnull) string;

+ (NSData* _Nullable) loggedData;

@end
