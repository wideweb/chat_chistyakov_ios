//
//  AppDelegate.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 01.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    // Override point for customization after application launch.
    UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
    
    //  QuickBlox init
    let appID: UInt = 34632
    let authKey = "SBqkG6mUKMFdHAv"
    let authSecret = "vHgFKZsEOZ66apf"
    let accountKey = "CVb4Ey8v29gbp3ioekKa"
    
    QMAPI.setup(appID, registerKey: authKey, registerSecret: authSecret, accountKey: accountKey)
    
    UIFont.enumerateAvailableFonts()

    Flurry.startSession("VWSZH6V6TMSNXKGC8MT2")
    Flurry.setLogLevel(FlurryLogLevelNone)
    
    Fabric.with([Crashlytics.self, Digits.self])
    
    CCLog.setupLogger()
    
    QMAPI.instance().registerForRemoteNotifications()
    return QMAPI.instance().application(application, didFinishLaunchingWithOptions: launchOptions)
  }

  func applicationWillResignActive(application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(application: UIApplication) {
    QMAPI.instance().applicationDidEnterBackground(application)
  }

  func applicationWillEnterForeground(application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(application: UIApplication) {
    QMAPI.instance().applicationDidBecomeActive(application)
  }

  func applicationWillTerminate(application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
  

  func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
    if notificationSettings.types != .None {
      application.registerForRemoteNotifications()
    }
    
  }
  
  
  func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
//    QMAPI.instance().application(application, didReceiveRemoteNotification: userInfo)
    NSLog("Remote: \(userInfo)")

  }
  
  
  func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
    NSLog("Remote: \(userInfo)")
  }
  

  func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    QMAPI.instance().didRegisterForRemoteNotification(deviceToken)
    NSLog("Pushes registered")
  }
 
  
  func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
    NSLog("Pushes error: \(error)")
  }
  

}

