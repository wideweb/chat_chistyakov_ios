//
//  CCBaseContactsListViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 17.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

let kSegueContactsToAddID = "segueContactsToAdd"
let kSegueContactsToChatID = "segueContactsToChat"
let kSegueListToDetailID = "segueListToDetail"
let kSegueStyleToWhiteID = "segueStyleToWhite"

let kReusableCellID = "cell"


class CCBaseContactsListViewController: CCBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  private var dataSource: QMFriendsListDataSource!
  private var isContactListLoading = false
  private var isContactListSearching: Bool { return dataSource.isSearching }
  private var userWithEditOptions: UInt? {
    didSet {
      if let prevUser = oldValue where oldValue != self.userWithEditOptions, let cell = self.getCellForUserID(prevUser) {
        cell.closeEditOptions() }
    }
  }
  private var userWithChatOptions: UInt? {
    didSet {
      if let prevUser = oldValue where oldValue != self.userWithChatOptions, let cell = self.getCellForUserID(prevUser) {
        cell.closeChatOptions() }
    }
  }
  
  
  //    @IBOutlet private weak var tableView: UITableView?
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
  
  
  //  MARK: -
  //  MARK: VLC
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configureController()
    self.configureInterface()
    
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.setupNavBar()
    
    self.dataSource.reloadDataSource()
  }
  
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    
    MessageCenter.sharedInstance.hideLoading(self.view)
  }
  
  
  func configureController() {
    self.setupEmptyDataSet()
    
    Singletone.sharedInstance.setupData()
    
    dataSource = QMFriendsListDataSource()
    dataSource.delegate = self
    
    self.searchBar.delegate = dataSource
    
  }
  
  
  func configureInterface() {
    assertionFailure("Error: Function \(#function) not overrided")
  }
  
  
  
  //  MARK: -
  //  MARK: Other
  func setupNavBar() {
    // TODO: abstract
    let navigationItem = self.navigationItem
    
    navigationItem.title = "Contacts"
    //    self.navigationController?.navigationBar.setTitleVerticalPositionAdjustment(6, forBarMetrics: .Default)
    
    let addItem = UIBarButtonItem(image: UIImage(named: "add_contact"), style: .Plain, target: self, action: #selector(CCWhiteContactsListViewController.addTapped(_:)))
    addItem.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5)
    navigationItem.rightBarButtonItems = [addItem]
    
    if self.navigationController?.viewControllers.count > 1 {
      let spacer = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
      spacer.width = -10
      self.navigationItem.leftBarButtonItems = [spacer, self.logoutButton()]
    }
    
  }
  
  
  func logoutButton() -> UIBarButtonItem {
    return UIBarButtonItem.backButton(self, action: #selector(CCBaseContactsListViewController.logoutTapped(_:)))
    
  }
  
  
  @IBAction func logoutTapped(sender: AnyObject) {
    self.logout()
    self.navigationController?.popToRootViewControllerAnimated(true)
    
  }
  
  
  func logout() {
    QMAPI.instance().logout { () -> () in NSLog("Logged out") }
    
  }
  
  
  
  //  MARK: -
  //  MARK: actions
  @IBAction func addTapped(sender: AnyObject) {
    performSegueWithIdentifier(kSegueContactsToAddID, sender: nil)
  }
  
  
  @IBAction func searchTapped(sender: AnyObject) {
    if self.dataSource.isSearching {
      self.dataSource.isSearching = false
      self.searchBar.hidden = true
      self.searchBarTopConstraint.constant = -self.searchBar.bounds.size.height }
    else {
      self.dataSource.isSearching = true
      self.searchBar.hidden = false
      self.searchBarTopConstraint.constant = 0
    }
    
  }
  
  
  
  // - MARK: Override
  func setupEmptyDataSet() {
    assertionFailure("Error: Function \(#function) not overrided")
    //  self.tableView.emptyDataSource = self
  }
  
  
  func reloadEmptyDataSet() {
    assertionFailure("Error: Function \(#function) not overrided")
    //  self.tableView.reloadEmptyDataSet()
  }
  
  
  func reloadData() {
    assertionFailure("Error: Function \(#function) not overrided")
    // self.tableView.reloadData()
  }
  
  
  func getCellForUserID(userID: UInt) -> CCContactCellInterface? {
    assertionFailure("Error: Function \(#function) not overrided")
    return nil
  }
  
  
}



//  MARK: -
//  MARK: QMFriendsListDataSourceDelegate
extension CCBaseContactsListViewController: QMFriendsListDataSourceDelegate {
  
  
  func didStartLoading() {
    self.isContactListLoading = true
    self.reloadEmptyDataSet()
  }
  
  
  func didReloadDataSource() {
    let number = dataSource.contactList.count
    
    // TODO: abstract
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
      let separatorsHidden = number == 0
//      self.separatorView0?.hidden = separatorsHidden
//      self.separatorView1?.hidden = separatorsHidden
      if !self.isContactListSearching { self.searchBar.userInteractionEnabled = !separatorsHidden }
      
      if self.isContactListSearching {
        self.searchBar.superview?.alpha = 1 }
      else if number != 0 {
        self.searchBar.superview?.alpha = 0.5 }
      else {
        self.searchBar.superview?.alpha = 0 }
    }
    
    self.isContactListLoading = false
    self.reloadData()
  }
  
  
}



//  MARK: -
//  MARK: CCContactTableViewCellDelegate
// TODO: abstract
extension CCBaseContactsListViewController: CCContactCellDelegate {
  
  
  func didTapChatWithUser(user: QBUUser) {
    MessageCenter.sharedInstance.showLoading("Loading...", view: self.view)
    
    QMAPI.instance().chatWithUser(user) { (chatDialog, error) -> () in
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        MessageCenter.sharedInstance.hideLoading(self.view)
        if let error = error { MessageCenter.sharedInstance.showError(error.localizedDescription) }
        else { self.performSegueWithIdentifier(kSegueContactsToChatID, sender: chatDialog) }
      })
      
    }
    
  }
  
  
  func didTapRemoveWithUser(user: QBUUser) {
    QMAPI.instance().removeUserFromContactList(user) { (success) -> () in
      if success {
        self.dataSource.reloadDataSource()
        MessageCenter.sharedInstance.showSuccess("Contact removed") }
      else { MessageCenter.sharedInstance.showError("Contact not removed") }
    }
    
  }
  
  
  func didTapBlockWithUser(user: QBUUser) {
  
  }
  
  
  func willShowEditOptionsForCell(cell: CCContactCellInterface) {
    if let userID = cell.getUserID() {
      self.userWithEditOptions = userID
    }
  }
  
  
  func willShowChatOptionsForCell(cell: CCContactCellInterface) {
    if let userID = cell.getUserID() {
      self.userWithChatOptions = userID
    }
  }
  
  
}


//  MARK: -
//  MARK: UITableViewDataSource, UITableViewDelegate
// TODO: abstract
extension CCBaseContactsListViewController: UITableViewDataSource, UITableViewDelegate {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSource.contactList.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(kReusableCellID, forIndexPath: indexPath)
    return cell
  }
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    let contact = dataSource.contactList[indexPath.row]
    
    if let cell = cell as? CCContactTableViewCell {
      cell.delegate = self
      cell.setContact(contact)
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = tableView.cellForRowAtIndexPath(indexPath) as? CCContactTableViewCell {
      cell.toggleFlip()
    }
    
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 159 / 2
  }
  
  
}



//  MARK: -
//  MARK: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
extension CCBaseContactsListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
  
  
  func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
    let attributes = [
      NSForegroundColorAttributeName : UIColor.whiteColor(),
      NSFontAttributeName : UIFont.helveticaNeueCyrLightFontOfSize(15)
    ]
    
    if self.isContactListLoading { return NSAttributedString() }
    if self.isContactListSearching { return NSAttributedString(string: "No contacts", attributes: attributes) }
    return NSAttributedString(string: "Click to add a new contact", attributes: attributes)
  }
  
  
  func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
    if self.isContactListLoading { return nil }
    if self.isContactListSearching { return nil }
    
    let s = 90
    return UIImage(named: "import_contacts")?.resizeTo(CGSize(width: s, height: s))
  }
  
  
  
  func customViewForEmptyDataSet(scrollView: UIScrollView!) -> UIView! {
    if !self.isContactListLoading { return nil }
    
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .White)
    activity.startAnimating()
    return activity
  }
  
  
  func emptyDataSetDidTapView(scrollView: UIScrollView!) {
    self.performSegueWithIdentifier(kSegueContactsToAddID, sender: nil)
    
  }
  
  
  func verticalOffsetForEmptyDataSet(scrollView: UIScrollView!) -> CGFloat {
    return -40
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension CCBaseContactsListViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == kSegueContactsToChatID {
      let controller = segue.destinationViewController as! CCChatViewController
      switch sender {
      case let dialog  as QBChatDialog: controller.dialog = dialog
      case let user as QBUUser: controller.recipient = user
      default: break
      }
      
    }
    else if segue.identifier == kSegueListToDetailID {
      let controller = segue.destinationViewController as! CCContactDetailViewController
      let indexPath = sender as! NSIndexPath
      let contact = self.dataSource.contactList[indexPath.row]
      controller.contact = contact
    }
    
  }
  
  
}