//
//  CCSearchBar.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 04.03.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCSearchBar: UISearchBar {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.configureBar()
    
  }
  
  
  func configureBar() {
    self.backgroundImage = UIImage()
    self.translucent = true
    
    if let textField = self.valueForKey("_searchField") as? UITextField {
      textField.clearButtonMode = .Always
      textField.enablesReturnKeyAutomatically = false
      textField.returnKeyType = .Done

      textField.contentVerticalAlignment = .Center
      textField.defaultTextAttributes = [
        NSForegroundColorAttributeName : UIColor(netHex: 0x000000).colorWithAlphaComponent(0.68),
        NSFontAttributeName : UIFont.helveticaNeueCyrLightFontOfSize(14),
        NSKernAttributeName : -0.28 / 2,
        NSBaselineOffsetAttributeName : -1
      ]
    }
    
  }
  
  
}
