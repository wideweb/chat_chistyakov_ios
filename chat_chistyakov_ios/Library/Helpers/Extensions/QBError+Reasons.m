//
//  QBError+Reasons.m
//  chat_chistyakov_ios
//
//  Created by Admin on 28.03.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

#import "QBError+Reasons.h"

@implementation QBError (Reasons)


- (NSString*) reasonsText {
  NSMutableString *text = @"".mutableCopy;
  NSDictionary *reasons = self.reasons;
  NSObject *errors = reasons[@"errors"];
  
  if([errors isKindOfClass:[NSDictionary class]]) {
    NSDictionary *dict = (NSDictionary*) errors;
    
    for (NSObject *key in dict.allKeys) {
      if(![key isKindOfClass:[NSString class]]) { continue; }
      NSString *keyStr = (NSString*) key;
      
      NSArray *value = dict[key];
      for(NSObject *reason in value) {
        if(![reason isKindOfClass:[NSString class]]) { continue; }
        NSString *reasonStr = (NSString*) reason;
        
        [text appendString:@"- "];
        [text appendString:keyStr];
        [text appendString:@" "];
        [text appendString:reasonStr];
        [text appendString:@";\n"];
      }
    }
  }
  
  if(text.length > 2) { [text replaceCharactersInRange:NSMakeRange(text.length-2, 2) withString:@"."]; }
  
  return text;
}


@end
