//
//  QMDialogsDataSource.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

protocol QMDialogsDataSourceDelegate {
  func didReloadDataSource()
}

class QMDialogsDataSource: NSObject {

  
  
  //  MARK: -
  //  MARK: properties
  var delegate: QMDialogsDataSourceDelegate?
  var dialogs: [QBChatDialog] = []
  
  
  
  //  MARK: -
  //  MARK: methods
  override init() {
    super.init()
    
    QMAPI.instance().chatService.addDelegate(self)
    QMAPI.instance().usersService.addDelegate(self)
    
    self.reloadDataSource()
  }
  
  
  func reloadDataSource() {
    dialogs = QMAPI.instance().dialogs()
    delegate?.didReloadDataSource()
  }
  
  
}



//  MARK: -
//  MARK: QMChatServiceDelegate, QMChatConnectionDelegate
extension QMDialogsDataSource: QMChatServiceDelegate, QMChatConnectionDelegate {
  
  
  func chatService(chatService: QMChatService!, didAddChatDialogsToMemoryStorage chatDialogs: [AnyObject]!) {
    self.reloadDataSource()
  }
  
  
  func chatService(chatService: QMChatService!, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog!) {
    self.reloadDataSource()
  }
  
  
  func chatService(chatService: QMChatService!, didReceiveNotificationMessage message: QBChatMessage!, createDialog dialog: QBChatDialog!) {
    self.reloadDataSource()
  }
  
  
  func chatService(chatService: QMChatService!, didAddMessagesToMemoryStorage messages: [QBChatMessage]!, forDialogID dialogID: String!) {
    self.reloadDataSource()
  }
  
  
  func chatService(chatService: QMChatService!, didAddMessageToMemoryStorage message: QBChatMessage!, forDialogID dialogID: String!) {
    self.reloadDataSource()
  }
  
  
  func chatService(chatService: QMChatService!, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String!) {
    self.reloadDataSource()
  }
  
  
}



//  MARK: -
//  MARK: QMUsersServiceDelegate
extension QMDialogsDataSource: QMUsersServiceDelegate {
  
  
  func usersService(usersService: QMUsersService!, didAddUsers user: [QBUUser]!) {
    self.reloadDataSource()
  }
  
  
}