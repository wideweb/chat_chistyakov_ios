//
//  SimpleTextFieldDelegate.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 10.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit



//  MARK: -
//  MARK: Protocol
protocol SimpleTextFieldDelegateDelegate {
  func excludingKeyboardWillShow(frame: CGRect) -> Bool
  func excludingKeyboardWillHide() -> Bool
  
  func textField(textField: UITextField, didChangeText text: String) -> Bool
}


extension SimpleTextFieldDelegateDelegate {
  func excludingKeyboardWillShow(frame: CGRect) -> Bool { return false }
  func excludingKeyboardWillHide() -> Bool { return false }
  
  func textField(textField: UITextField, didChangeText text: String) -> Bool { return false }
}



//  MARK: -
//  MARK: Class
class SimpleTextFieldDelegate: NSObject {

  
  var delegate: SimpleTextFieldDelegateDelegate?
  private weak var view: UIView?
  
  
  init(view: UIView) {
    super.init()
    
    self.view = view
    
    self.setupKeyboardNotifications()
  }

  
  func addTextFields(textFields: [UITextField]) {
    textFields.forEach({ self.addTextField($0) })
  }
  
  
  func addTextField(textField: UITextField) {
    textField.delegate = self
    textField.addTarget(self, action: Selector("textFieldDiDChange:"), forControlEvents: .EditingChanged)
  }
  
  
}



extension SimpleTextFieldDelegate: UITextFieldDelegate {

  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  
  func textFieldDiDChange(textField: UITextField) {
    self.delegate?.textField(textField, didChangeText: textField.text ?? "")
  }
  
  
}



//  MARK: -
//  MARK: keyboard appearance
extension SimpleTextFieldDelegate {
  
  
  func setupKeyboardNotifications() {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
  }
  
  
  func keyboardWillShow(notification: NSNotification) {
    let keyboardFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
    
    if self.delegate?.excludingKeyboardWillShow(keyboardFrame) == true { return }

    UIView.animateWithDuration(0.7) { () -> Void in
      self.view?.frame.size.height = UIScreen.mainScreen().bounds.size.height - keyboardFrame.size.height
      self.view?.layoutIfNeeded()
    }
    
  }
  
  
  func keyboardWillHide(notification: NSNotification) {
    if self.delegate?.excludingKeyboardWillHide() == true { return }
    
    UIView.animateWithDuration(0.7) { () -> Void in
      self.view?.frame.size.height = UIScreen.mainScreen().bounds.size.height
    }
    
  }
  
  
}