//
//  CCWhiteContactCollectionViewCell.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 18.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit


class CCWhiteContactCollectionViewCell: UICollectionViewCell {
  
  enum Mode {
    case Normal, EditOptions, ChatOptions
  }
  
  var delegate: CCContactCellDelegate? {
    didSet { NSLog("New delegate: \(delegate)") }
  }
  
  private var user: QBUUser!
  private var _mode: Mode = .Normal
  
  private var textAttributes: [String: AnyObject] = [:]
  
  @IBOutlet private var popover: Popover?
  @IBOutlet private weak var imageView: UIImageView!
  @IBOutlet private weak var nameLabel: UILabel!
  @IBOutlet private weak var unreadImageView: UIImageView!
  @IBOutlet private weak var unreadCountLabel: UILabel!
  @IBOutlet private weak var unreadImageMaskView: UIImageView!
  
  @IBOutlet private weak var deleteButton: UIButton!
  @IBOutlet private weak var editButton: UIButton!
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.configureView()
  }

  
  func configureView() {
    self.setupNotifications()
    self.resetMode()
    
    //  Gesture
    let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(CCWhiteContactCollectionViewCell.handleLongPress(_:)))
    longPressGesture.minimumPressDuration = 0.5
    self.addGestureRecognizer(longPressGesture)
  
    //  Buttons
    self.deleteButton.backgroundColor = UIColor(netHex: 0xde7ec3)
    self.editButton.backgroundColor = UIColor(netHex: 0xfcf456)
    
    self.editButton.titleLabel?.font = UIFont.helveticaNeueCyrMediumFontOfSize(12)
    self.editButton.tintColor = UIColor(netHex: 0x020202)
    
    self.editButton.setTitle("Edit", forState: .Normal)
    
    self.hideEditOptions(false)
    
    //  Image
    self.imageView.contentMode = .Bottom
    self.imageView.image = UIImage(named: "")
    self.imageView.layer.masksToBounds = true
    
    //  Labels
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 3
    paragraphStyle.alignment = .Center
    
    self.textAttributes = [
      NSFontAttributeName : UIFont.helveticaNeueCyrMediumFontOfSize(12),
      NSParagraphStyleAttributeName : paragraphStyle
    ]
    
  }
  
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    self.deleteButton.layer.cornerRadius = self.deleteButton.bounds.height / 2
    self.editButton.layer.cornerRadius = self.editButton.bounds.height / 2
    self.imageView.layer.cornerRadius = self.imageView.bounds.height / 2
    
  }
  
  
  func setMode(mode: Mode, animated: Bool = true) {
    self.updateViewMode(self._mode, toMode: mode, animated: animated)
    self._mode = mode
    
  }
  
  
  func updateViewMode(fromMode: Mode, toMode: Mode, animated: Bool = true) {
    switch fromMode {
    case .Normal: break
    case .ChatOptions: self.hidePopover()
    case .EditOptions: self.hideEditOptions(animated)
    }
    
    if fromMode == toMode { return }
    
    switch toMode {
    case .Normal: break
    case .ChatOptions: self.showPopover()
    case .EditOptions: self.showEditOptions()
    }
    
  }
  
  
  func setContact(user: QBUUser, isNew: Bool) {
    self.resetMode()
    
    self.user = user
    
    self.textAttributes[NSForegroundColorAttributeName] = QMAPI.instance().isOnlineUserWithID(user.ID) ? UIColor(netHex: 0x000000) : UIColor(netHex: 0x999999)
    self.nameLabel.attributedText = NSAttributedString(string: user.fullName ?? user.login ?? "", attributes: self.textAttributes)
    
    let avatarPlaceholder = UIImage(named: "")
    imageView.image = avatarPlaceholder
    imageView.contentMode = .Bottom
    
    if let urlString = QMAPI.instance().publicImageUrlWithID(user.blobID), let url = NSURL(string: urlString) {
      self.imageView.sd_setImageWithURL(url, placeholderImage: avatarPlaceholder, completed: { (image: UIImage?, _, _, responseUrl: NSURL?) in
        if urlString == responseUrl?.absoluteString && image != nil {
          self.imageView.contentMode = .ScaleAspectFit
          self.imageView.image = image
        }
        else {
          self.imageView.image = avatarPlaceholder
          self.imageView.contentMode = .Bottom
        }
      })
    }
    
    
    var userColor = UIColor.blueColor()
    if let colorCodeString = user.colorCode {
      userColor = UIColor(hex: colorCodeString)
    }
    
    self.unreadCountLabel.textColor = userColor
    self.unreadImageMaskView.tintColor = userColor
    self.imageView.backgroundColor = userColor
    
    
    if let chat = QMAPI.instance().privateDialogWithUser(user.ID)
      where chat.unreadMessagesCount > 0 {
      self.unreadImageView.hidden = !isNew
      self.unreadCountLabel.hidden = !isNew
      self.unreadImageMaskView.hidden = !isNew
      self.unreadCountLabel.text = "\(chat.unreadMessagesCount)"
      
    } else {
      self.unreadImageView.hidden = true
      self.unreadCountLabel.hidden = true
      self.unreadImageMaskView.hidden = true }
    
  }
  
  
  func resetMode() {
    self.setMode(.Normal, animated: false)
    
  }
  
  
  
  
  
  //  MARK: -
  //  MARK: Actions
  @IBAction func chatTapped(sender: AnyObject) {
    self.setMode(.Normal)
    self.delegate?.didTapChatWithUser(self.user)
    
  }
  
  
  @IBAction func removeTapped(sender: AnyObject) {
    self.setMode(.Normal)
    self.delegate?.didTapRemoveWithUser(self.user)
    
  }
  
  
  @IBAction func handleLongPress(sender: AnyObject) {
    if let recognizer = sender as? UILongPressGestureRecognizer {
      if recognizer.state != .Began { return }
    }
    self.openEditOptions()
    
  }
  
  
  
  // - MARK: Other
  override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
    let inside = super.pointInside(point, withEvent: event)
    
    if !inside {
        self.closeEditOptions()
    }
    
    return inside
  }
  
  
}



// - MARK: Edit options
extension CCWhiteContactCollectionViewCell {
  
  
  func showEditOptions() {
    self.sendEditOptionsOpenNotification()
    
    let startTransform = CGAffineTransformMakeScale(0.1, 0.1)
    let endTransform = CGAffineTransformIdentity
    
    self.deleteButton.transform = startTransform
    self.editButton.transform = startTransform
    
    self.deleteButton.hidden = false
    self.editButton.hidden = false
    
    UIView.animateWithDuration(0.3) { 
      self.deleteButton.transform = endTransform
      self.editButton.transform = endTransform
    }
  }
  
  
  func hideEditOptions(animated: Bool = true) {
    let completion = { ()->() in
      self.deleteButton.hidden = true
      self.editButton.hidden = true
    }
    
    if !animated {
      completion()
    } else {
      let startTransform = CGAffineTransformIdentity
      let endTransform = CGAffineTransformMakeScale(0.1, 0.1)
      
      self.deleteButton.transform = startTransform
      self.editButton.transform = startTransform
      
      UIView.animateWithDuration(0.3, animations: {
        self.deleteButton.transform = endTransform
        self.editButton.transform = endTransform
      }) { (_) in
        completion()
      }
    }
  
  }
  
  
}


// - MARK: Popover
extension CCWhiteContactCollectionViewCell {
  
  
  func showPopover() {
    let aView = self.createPopoverView()
    let mainView = self.delegate!.mainView()!
    
    let popoverSize = aView.frame.size
    let arrowSize = CGSize(width: 15, height: 15)
    
    let animationTime = 0.3
    
    let cellFrame = mainView.convertRect(self.frame, fromView: mainView)
    let allowDownDirection = cellFrame.origin.y + cellFrame.size.height + popoverSize.height + arrowSize.height < mainView.frame.size.height
    let options: [PopoverOption] = [
      .ArrowSize(arrowSize),
      .CornerRadius(popoverSize.height / 2),
      .AnimationIn(animationTime),
      .AnimationOut(animationTime),
      .Type(allowDownDirection ? PopoverType.Down : PopoverType.Up),
      .BlackOverlayColor(UIColor.blackColor().colorWithAlphaComponent(0.1))
    ]
    
    popover = Popover(options: options)
    popover?.show(aView, fromView: self.imageView, inView: mainView)

  }
  
  
  func hidePopover() {
    self.popover?.dismiss()
  }
  
  
  func createPopoverView() -> UIView {
    let containerHeight: CGFloat = 122/2
    let buttonSize = CGSize(width: 76/2, height: 76/2)
    let buttonInset: CGFloat = (38+44)/2
    
    let chatButton = self.createChatOptionsButton(buttonSize, image: UIImage(named: "make_chat"), selector: #selector(CCWhiteContactCollectionViewCell.chatTapped(_:)))
    let videoButton = self.createChatOptionsButton(buttonSize, image: UIImage(named: "make_video_call"), selector: nil)
    let callButton = self.createChatOptionsButton(buttonSize, image: UIImage(named: "make_call"), selector: nil)
    
    let buttonsView = UIView(frame: CGRect(x: 0, y: 0, width: 3 * buttonSize.width + 2 * buttonInset, height: buttonSize.height))
    
    buttonsView.addSubview(chatButton)
    buttonsView.addSubview(videoButton)
    buttonsView.addSubview(callButton)
    
    videoButton.frame.origin = CGPoint(x: 0, y: 0)
    callButton.frame.origin = CGPoint(x: buttonSize.width + buttonInset, y: 0)
    chatButton.frame.origin = CGPoint(x: 2 * (buttonSize.width + buttonInset), y: 0)
    
//    let mainView = self.delegate!.mainView()!
    let containerWidth = buttonsView.bounds.width + 44
    let containerView = UIView(frame: CGRect(x: 0, y: 0, width: containerWidth, height: containerHeight))
    
    containerView.addSubview(buttonsView)
    
    buttonsView.center = containerView.center

    videoButton.enabled = false
    callButton.enabled = false
    
    return containerView
  }
  
  
  func createChatOptionsButton(size: CGSize, image: UIImage?, selector: Selector?) -> UIButton {
    let buttonColor = UIColor(netHex: 0x2ecbde)
    let button = UIButton(frame: CGRect(origin: CGPointZero, size: size))
    button.setImage(image, forState: .Normal)
//    button.layer.borderWidth = 1
//    button.layer.borderColor = buttonColor.CGColor
//    button.layer.cornerRadius = size.height / 2
    button.tintColor = buttonColor
    
    if let selector = selector {
      button.addTarget(self, action: selector, forControlEvents: .TouchUpInside)
    }
    
    return button
  }
  
  
}



// - MARK: Notifications
extension CCWhiteContactCollectionViewCell {
  
  
  var editOptionsOpenNotificationName: String { return "editOptionsOpenNotification" }
  var chatOptionsOpenNotificationName: String { return "chatOptionsOpenNotification" }
  
  
  func setupNotifications() {
    let notificationCenter = NSNotificationCenter.defaultCenter()
    
    notificationCenter.addObserver(self, selector: #selector(CCWhiteContactCollectionViewCell.handleEditOptionsOpenNotification(_:)), name: self.editOptionsOpenNotificationName, object: nil)
    notificationCenter.addObserver(self, selector: #selector(CCWhiteContactCollectionViewCell.handleChatOptionsOpenNotification(_:)), name: self.chatOptionsOpenNotificationName, object: nil)
    notificationCenter.addObserver(self, selector: #selector(CCWhiteContactCollectionViewCell.handleOutsideTouch(_:)), name: kUserTouchNotificationName, object: nil)
    
  }
  
  
  func sendEditOptionsOpenNotification(isEmpty: Bool = false) {
    let notificationCenter = NSNotificationCenter.defaultCenter()
    notificationCenter.postNotificationName(self.editOptionsOpenNotificationName, object: isEmpty ? nil : self)
  }
  
  
  func sendChatOptionsOpenNotification() {
    let notificationCenter = NSNotificationCenter.defaultCenter()
    notificationCenter.postNotificationName(self.chatOptionsOpenNotificationName, object: self)
  }
  
  
  func handleEditOptionsOpenNotification(notification: NSNotification) {
    if notification.name == self.editOptionsOpenNotificationName && notification.object !== self {
      self.closeEditOptions()
    }
  }
  
  
  func handleChatOptionsOpenNotification(notification: NSNotification) {}
  
  
  func handleOutsideTouch(notification: NSNotification) {
    if let wPoint = (notification.object as? NSValue)?.CGPointValue() {
      let cPoint = self.convertPoint(wPoint, fromView: UIApplication.sharedApplication().keyWindow)
      if !self.bounds.contains(cPoint) {
        self.closeEditOptions()
      }
    }
    
  }
  
  
}



extension CCWhiteContactCollectionViewCell: CCContactCellInterface {
  
  
  func setUser(user: QBUUser) {
//    self.setContact(user)
  }
  
  
  func getUserID() -> UInt? {
    return nil
  }
  
  
  func openEditOptions() {
    self.setMode(.EditOptions)
  }
  
  
  func closeEditOptions() {
    self.setMode(.Normal)
  }
  
  
  func openChatOptions() {
    self.setMode(.ChatOptions)
  }
  
  
  func closeChatOptions() {
    self.setMode(.Normal)
  }
  
  
  func toggleChatOptions() {
    switch self._mode {
    case .Normal: self.setMode(.ChatOptions)
    case .EditOptions: self.setMode(.Normal)
    case .ChatOptions: self.setMode(.Normal)
    }
  }
  
  
}



//  - MARK: touch outside
let kUserTouchNotificationName = "userTouchNotificationName"

class TapCatchView: UIView {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.clearColor()
  }
  
  
  override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
    let inside = super.pointInside(point, withEvent: event)
    if inside && event?.type == .Touches {
      if let wPoint = UIApplication.sharedApplication().keyWindow?.convertPoint(point, fromView: self) {
        NSNotificationCenter.defaultCenter().postNotificationName(kUserTouchNotificationName, object: NSValue(CGPoint: wPoint))
      }
    }
    
    return false
  }
  
  
}