//
//  NSObject+Properties.h
//  chat_chistyakov_ios
//
//  Created by Admin on 21.04.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Properties)

- (NSString*) logProperties;

@end
