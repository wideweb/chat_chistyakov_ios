//
//  CCContactCellProtocols.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 18.05.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


protocol CCContactCellDelegate {
  func didTapChatWithUser(user: QBUUser)
  func didTapRemoveWithUser(user: QBUUser)
  func didTapBlockWithUser(user: QBUUser)
  
  func willShowEditOptionsForCell(cell: CCContactCellInterface)
  func willShowChatOptionsForCell(cell: CCContactCellInterface)
  
  func mainView() -> UIView?
}

extension CCContactCellDelegate {
  func mainView() -> UIView? { return nil }
}


protocol CCContactCellInterface {
  func getUserID() -> UInt?
  func setUser(user: QBUUser)
  
  func openEditOptions()
  func closeEditOptions()
  
  func openChatOptions()
  func closeChatOptions()
  func toggleChatOptions()
  
}