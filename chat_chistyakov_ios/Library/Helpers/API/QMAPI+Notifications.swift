//
//  QMAPI+Notifications.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


typealias QMAPINotificationsMessageCompletion = (notification: QBChatMessage, error: NSError?)->()


extension QMAPI {

  
  func sendContactRequestSendNptification(user: QBUUser, completion: QMAPINotificationsMessageCompletion?) {
    let notification = self.notificationFor(user)
    self.send(notification, completion: completion)
  }
  
  
  func send(notification: QBChatMessage, completion: QMAPINotificationsMessageCompletion?) {
    let dialog = self.chatService.dialogsMemoryStorage.privateChatDialogWithOpponentID(notification.recipientID)
    assert(dialog != nil, "Dialog not found")
    
    self.chatService.sendMessage(notification, toDialog: dialog, saveToHistory: true, saveToStorage: true) { (error: NSError?) -> Void in

      completion?(notification: notification, error: error)
    }
    
  }
  
  
  private func notificationFor(user: QBUUser) -> QBChatMessage {
    let notification = QBChatMessage()
    
    notification.recipientID = user.ID
    notification.senderID = self.currentUser().ID
    notification.text = NSLocalizedString("_contact_request", comment: "")
    notification.dateSent = NSDate()
    
    return notification
  }
  
  
  
  //  MARK: -
  //  MARK: Push notifications
  func registerForRemoteNotifications() {
    let application = UIApplication.sharedApplication()
    application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil))
    application.registerForRemoteNotifications()
    
  }
  
  
  func didRegisterForRemoteNotification(deviceToken: NSData) {
    let deviceID = UIDevice.currentDevice().identifierForVendor?.UUIDString
    
    let subscription = QBMSubscription()
    subscription.notificationChannel = QBMNotificationChannelAPNS
    subscription.deviceToken = deviceToken
    subscription.deviceUDID = deviceID
    
    QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse, subscriptions: [QBMSubscription]?) -> Void in
        NSLog("SUBSCRIPTION SUCCESS")
      }) { (response: QBResponse) -> Void in
        NSLog("SUBSCRIPTION ERROR")
    }
  }
  

}