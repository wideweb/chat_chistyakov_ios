//
//  CCContactDetailViewController.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 16.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCContactDetailViewController: CCBaseViewController {

  
  
  private enum Mode {
    case Read
    case Edit
  }
  
  
  //  MARK: -
  //  MARK: properties
  var contact: QBUUser!
  
  
  private var mode = Mode.Read { didSet { self.updateData() } }
  private var items: [NSIndexPath : UIButton] = [:]
  private var cellSize = CGSizeZero { didSet { self.collectionView.reloadData() } }
  private var kCellInset: CGFloat = 20
  private var kColumnCount: Int = 3
  private let colors = [
    UIColor.cyanColor(),
    UIColor.yellowColor(),
    UIColor.greenColor(),
    UIColor.redColor(),
    UIColor.purpleColor(),
    UIColor.blueColor(),
    UIColor.magentaColor(),
    UIColor.lightGrayColor(),
    UIColor.blackColor()
  ]
  private var userColorIndex = 0
  
  
  @IBOutlet private weak var collectionView: UICollectionView!
  @IBOutlet private weak var avatarView: UIImageView!
  @IBOutlet private weak var nameField: UITextField!
  @IBOutlet private weak var surnameField: UITextField!
  @IBOutlet private weak var phoneLabel: UILabel!
  @IBOutlet private weak var fullNameView: UIView!
  @IBOutlet private weak var changeButton: UIButton!
  
  
  
  //  MARK: -
  //  MARK: Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupNavBar()
    
    self.updateData()
  }
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    let width = (self.collectionView.frame.size.width - self.kCellInset) / CGFloat(self.kColumnCount) - self.kCellInset
    cellSize = CGSize(width: width, height: width)
  }

  
  func setupNavBar() {
    navigationItem.title = "INFO"
  }
  
  
  func updateData() {
    //  Resources
    let alphaRedColor = UIColor.redColor().colorWithAlphaComponent(0.3)
    let alphaBlackColor = UIColor.blackColor().colorWithAlphaComponent(0.3)
    let whiteColor = UIColor.whiteColor()
    let blockButtonFont1 = UIFont.systemFontOfSize(12)
    let blockButtonFont2 = UIFont.systemFontOfSize(10)
    let colors = self.colors
    let userColor = colors[self.userColorIndex]
    
    //  Data
    let userBlocked = QMAPI.instance().isUserInBlockList(contact.ID)
    let fullnameParts = (self.contact.fullName ?? "").characters.split(isSeparator: { $0 == " " }).map(String.init)
    let name = fullnameParts[safe: 0] ?? ""
    let surname = fullnameParts[safe: 1] ?? ""
    
    self.nameField.text = name
    self.surnameField.text = surname
    self.phoneLabel.text = self.contact.phone
    
    let blockButtonTitle =  NSMutableAttributedString()
    blockButtonTitle.appendAttributedString(NSAttributedString(string: userBlocked ? "Unblock" : "Block", attributes: [
      NSForegroundColorAttributeName : whiteColor,
      NSFontAttributeName : blockButtonFont1
      ]))
    blockButtonTitle.appendAttributedString(NSAttributedString(string: "\nContact", attributes: [
      NSForegroundColorAttributeName : whiteColor,
      NSFontAttributeName : blockButtonFont2
      ]))
    
    //  View colors
    self.fullNameView.backgroundColor = userColor
    
    //  Menu buttons
    let chatButton = self.createButtonWith(self, action: Selector("chatTapped:"), image: UIImage(named: "make_chat"))
    let callButton = self.createButtonWith(self, action: Selector("callTapped:"), image: UIImage(named: "make_call"), enabled: false)
    let camButton = self.createButtonWith(self, action: Selector("camTapped:"), image: UIImage(named: "make_video_call"), enabled: false)
    let blockButton = self.createButtonWith(self, action: Selector("blockTapped:"), image: UIImage(named: "block"), title: blockButtonTitle, bgColor: alphaBlackColor)
    let removeButton = self.createButtonWith(self, action: Selector("removeTapped:"), image: UIImage(named: "remove"), bgColor: alphaRedColor)
  
    var colorButtons: [UIButton] = []
    for i in 0..<colors.count {
      let colorButton = self.createButtonWith(self, action: Selector("colorTapped:"), image: nil, bgColor: colors[i])
      colorButton.tag = i
      colorButtons = colorButtons + [colorButton]
    }
  
    
    //  Mode
    self.items = [:]
    switch self.mode {
    case .Read:
      self.changeButton.hidden = false
      
      self.nameField.userInteractionEnabled = false
      self.surnameField.userInteractionEnabled = false
      
      self.items[NSIndexPath(forItem: 0, inSection: 0)] = chatButton
      self.items[NSIndexPath(forItem: 1, inSection: 0)] = callButton
      self.items[NSIndexPath(forItem: 2, inSection: 0)] = camButton
      self.items[NSIndexPath(forItem: 1, inSection: 2)] = blockButton
      self.items[NSIndexPath(forItem: 2, inSection: 2)] = removeButton
    case .Edit:
      self.changeButton.hidden = true
      
      self.nameField.userInteractionEnabled = true
      self.surnameField.userInteractionEnabled = true
      self.nameField.becomeFirstResponder()
      
      for i in 0..<colorButtons.count { self.items[NSIndexPath(forItem: i % 3, inSection: Int(i / 3))] = colorButtons[i] }
    }
    
    
    self.collectionView.reloadData()
  }
  
  
  
  //  MARK: -
  //  MARK: Actions
  @IBAction func chatTapped(sender: AnyObject) {
    QMAPI.instance().createPrivateChatDialogIfNeeded(self.contact) { (chatDialog, error) -> () in
      self.performSegue(Segue.segueDetailToChat, sender: chatDialog)
    }
  }
  
  
  @IBAction func callTapped(sender: AnyObject) {
    
  }
  
  
  @IBAction func camTapped(sender: AnyObject) {
    
  }
  
  
  @IBAction func blockTapped(sender: AnyObject) {
    if QMAPI.instance().isUserInBlockList(contact.ID) { QMAPI.instance().unblockUser(contact.ID) }
    else { QMAPI.instance().blockUser(contact.ID) }
    self.updateData()
  }
  
  
  @IBAction func removeTapped(sender: AnyObject) {
    QMAPI.instance().removeUserFromContactList(self.contact) { (success) -> () in
      if success {
        self.navigationController?.popViewControllerAnimated(true)
        MessageCenter.sharedInstance.showSuccess("Contact removed") }
      else { MessageCenter.sharedInstance.showError("Contact not removed") }
    }
    
  }
  
  
  @IBAction func changeTapped(sender: AnyObject) {
    self.mode = .Edit
  }
  
  
  @IBAction func colorTapped(sender: UIButton) {
    self.userColorIndex = sender.tag
    self.mode = .Read
  }
  
  
  //  MARK: -
  //  MARK: Utilities
  private func createButtonWith(target: NSObject, action: Selector, image: UIImage?, title: NSAttributedString = NSAttributedString(), bgColor: UIColor = UIColor.clearColor(), enabled: Bool = true) -> UIButton {
    let button = UIButton()
    button.addTarget(target, action: action, forControlEvents: .TouchUpInside)
    button.setImage(image, forState: .Normal)
    button.backgroundColor = bgColor
    button.setAttributedTitle(title, forState: .Normal)
    button.enabled = enabled
    button.alpha = enabled ? 1 : 0.5
    return button
  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDataSource
extension CCContactDetailViewController : UICollectionViewDataSource {
  
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 3
  }
  
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.kColumnCount
  }
  
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)!
    
    if let cell = cell as? CCCircleButtonCollectionViewCell {
      if let button = self.items[indexPath] { cell.setData(button) }
      else { cell.resetData() }
    }
    
    return cell
  }
  

}



//  MARK: -
//  MARK: UICollectionViewDelegateFlowLayout
extension CCContactDetailViewController : UICollectionViewDelegateFlowLayout {
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return self.cellSize
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: self.kCellInset, left: self.kCellInset, bottom: 0, right: self.kCellInset)
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension CCContactDetailViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue == Segue.segueDetailToChat {
      let controller = segue.destinationViewController as! CCChatViewController
      controller.dialog = sender as! QBChatDialog
    }
    
  }
  
  
}
