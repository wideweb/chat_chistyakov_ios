//
//  CCCheckBox.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 10.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import UIKit

class CCCheckBox: MPCheckBox {

  
  func updateView() {
    let tintColor = UIColor(netHex: 0xdad7da)
    
    
    self.setBorderColor(tintColor)
    self.setCheckColor(tintColor)
    
    self.layer.cornerRadius = 2
    
  }
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.updateView()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    self.updateView()
  }
  
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    if let view = self.subviews.first {
      view.frame.origin.x = (self.frame.size.width - view.frame.size.width) / 2
      view.frame.origin.y = (self.frame.size.height - view.frame.size.height) / 2
    }
  }
  

}
