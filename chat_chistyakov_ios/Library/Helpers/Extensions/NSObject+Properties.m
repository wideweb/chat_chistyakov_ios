//
//  NSObject+Properties.m
//  chat_chistyakov_ios
//
//  Created by Admin on 21.04.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

#import "NSObject+Properties.h"
#import <objc/runtime.h>

@implementation NSObject (Properties)

- (NSString*) logProperties {
  NSMutableDictionary *props = @{}.mutableCopy;
  
  @autoreleasepool {
    unsigned int numberOfProperties = 0;
    objc_property_t *propertyArray = class_copyPropertyList([self class], &numberOfProperties);
    for (NSUInteger i = 0; i < numberOfProperties; i++) {
      objc_property_t property = propertyArray[i];
      NSString *name = [[NSString alloc] initWithUTF8String:property_getName(property)];
      props[name] = [self valueForKey:name];
    }
    free(propertyArray);
  }
  return props.description;
}

@end
