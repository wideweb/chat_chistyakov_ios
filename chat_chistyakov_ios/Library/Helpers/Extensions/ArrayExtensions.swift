//
//  ArrayExtensions.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 17.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation



extension Array {
  
  
  subscript (safe index: Int) -> Element? {
    return self.indices.contains(index) ? self[index] : nil
  }
  
  
}