//
//  QBRequest+(Params).h
//  chat_chistyakov_ios
//
//  Created by Admin on 29.03.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickBlox/QuickBlox.h>

@interface QBRequest (Params)

- (NSString*) paramsDescription;

- (NSString*) description;

@end
