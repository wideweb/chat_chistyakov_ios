//
//  QBAPI.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation
//import Quickblox


private let kContactListCacheNameKey = "chat-contacts"

private let kPresenceTime: NSTimeInterval = 30


class QMAPI: QMServicesManager {
  
  
  
  //  MARK: -
  //  MARK: class
  class func setup(appID: UInt, registerKey: String, registerSecret: String, accountKey: String) {
    QBSettings.setApplicationID(appID)
    QBSettings.setAuthKey(registerKey)
    QBSettings.setAuthSecret(registerSecret)
    QBSettings.setAccountKey(accountKey)
    
    QBSettings.setLogLevel(QBLogLevel.Nothing)
    
  }


  
  //  MARK: -
  //  MARK: object
  var contactListService: QMContactListService!
  var privacyList: QBPrivacyList?
  
  
  private var presenceTimer: NSTimer!
  private var pushNotification: NSDictionary? {
    didSet { dispatch_async(dispatch_get_main_queue(), { () -> Void in QMAPI.instance().handlePushNotification() }) }
  }
  private var deviceToken: NSData?
  
  
  
  //  MARK: -
  //  MARK: methods
  override init() {
    super.init()
    
    QMContactListCache.setupDBWithStoreNamed(kContactListCacheNameKey)
    contactListService = QMContactListService(serviceManager: self, cacheDataSource: self)
    contactListService.addDelegate(self)
    
    self.presenceTimer = NSTimer.scheduledTimerWithTimeInterval(kPresenceTime, target: self, selector: Selector("sendPresence"), userInfo: nil, repeats: true)
    
    self.usersService.loadFromCache()
    if let _ = self.currentUser() { self.chatService.loadCachedDialogsWithCompletion { () -> Void in NSLog("Dialogs loaded") } }

  }
  
  
  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
//    let application = UIApplication.sharedApplication()
//    
//    application.applicationIconBadgeNumber = 0
//    if #available(iOS 8.0, *) {
//      application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Sound, .Alert, .Badge], categories: nil))
//      application.registerForRemoteNotifications()
//    } else {
//      application.registerForRemoteNotificationTypes([.Alert, .Sound, .Badge])
//    }
    
    if let launchOptions = launchOptions {
      let notification = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]
      self.pushNotification = notification as? NSDictionary
    }
    
    return true
  }
  
  
  func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
    self.pushNotification = userInfo
    
    
    
  }
  
  
  func applicationDidEnterBackground(application: UIApplication) {
    UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    
  }
  
  
  //  MARK: -
  //  MARK: Push notifications
  func handlePushNotification() {
    return
//    MessageCenter.sharedInstance.showError("Push")
    
    guard let userInfo = self.pushNotification else { return }
    
    let application = UIApplication.sharedApplication()
    
    if application.applicationState == UIApplicationState.Inactive {
      //  PROCESS PUSH WHEN INACTIVE
      guard let topController = UIApplication.topViewController() as? CCBaseViewController else {
        MessageCenter.sharedInstance.showError("Top 1")
        return }
      guard let userID = userInfo["user_id"] as? UInt else {
        MessageCenter.sharedInstance.showError("ID 1")
        return }
      
      QMAPI.instance().safeUser(userID) { (user, error) -> () in
        guard let user = user else {
          MessageCenter.sharedInstance.showError("User 1")
          return }
        
        topController.ccShowChat({ (controller) -> () in
          (controller as? CCChatViewController)?.recipient = user
        })
        
      }
      
      self.pushNotification = nil
    }
    else {
      //  SHOW PUSH WHEN ACTIVE
      guard let topController = UIApplication.topViewController() as? CCBaseViewController else {
        MessageCenter.sharedInstance.showError("Top")
        return }
      guard let userID = userInfo["user_id"] as? UInt else {
        MessageCenter.sharedInstance.showError("ID")
        return }
      
      
      
      MessageCenter.sharedInstance.showPush("", message: "", tapAction: { ()->() in
        QMAPI.instance().safeUser(userID) { (user, error) -> () in
          guard let user = user else {
            MessageCenter.sharedInstance.showError("User")
            return }
          
          topController.ccShowChat({ (controller) -> () in
            (controller as? CCChatViewController)?.recipient = user
          })
          
        }
      })
      
      self.pushNotification = nil
    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: Status
  func sendPresence() {
    if QBChat.instance().isConnected() {
      QBChat.instance().sendPresence()
    }
    
  }
  
  
  func applicationWillResignActive() {
    self.disconectFromChatIfNeeded()
  }
  
  
  func applicationDidBecomeActive(application: UIApplication) {
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
      self.connectChat({ (error) -> () in
        
      })
    }
    
  }
  
  
}



//  MARK: -
//  MARK: QMContactListServiceCacheDataSource
extension QMAPI: QMContactListServiceCacheDataSource {

  
  func cachedContactListItems(block: QMCacheCollection!) {
    QMContactListCache.instance().contactListItems(block)
  }
  
  
  func contactListDidAddUser(user: QBUUser!) {
    QMContactListCache.instance().save(nil)
  }
  
  
}