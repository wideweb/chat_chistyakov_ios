//
//  QMAPI+Users.swift
//  chat_chistyakov_ios
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 wide-web. All rights reserved.
//

import Foundation


private let kDefaultPrivacyList = "public"


typealias QMAPIUsersEmptyCompletion = ()->()
typealias QMAPIUsersSimpleCompletion = (success: Bool)->()
typealias QMAPIUsersUserCompletion = (users: [QBUUser], error: NSError?)->()
typealias QMAPIUsersOneUserCompletion = (user: QBUUser?, error: NSError?)->()


extension QMAPI {

  
  
  //  MARK: -
  //  MARK: users
  func user(userID: UInt) -> QBUUser? {
    return self.usersService.usersMemoryStorage.userWithID(userID)
  }
  
  
  func usersWithIds(ids: [UInt]) -> [QBUUser] {
    return ids.reduce([QBUUser](), combine: { (users, userID) -> [QBUUser] in
      guard let user = self.user(userID) else { return users }
      return users + [user]
    })
  }
  
  
  func safeUsersWithIDs(
    ids: [UInt],
    page: QBGeneralResponsePage = QBGeneralResponsePage(currentPage: 1, perPage: 100),
    completion: QMAPIUsersUserCompletion?
    ) {
      
    let usersIDs = ids.reduce([NSNumber](), combine: { $0 + [NSNumber(unsignedInteger: $1)] })
    
    usersService.getUsersWithIDs(usersIDs, page: page).continueWithBlock { (task: BFTask) -> AnyObject? in
      completion?(users: task.result as? [QBUUser] ?? [], error: task.error)
      return nil
    }
    
  }
  
  
  func safeUser(userID: UInt, completion: QMAPIUsersOneUserCompletion?) {
    self.safeUsersWithIDs([userID]) { (users, error) -> () in
      completion?(user: users.first, error: error)
    }
  }
  
  
  
  //  MARK: -
  //  MARK: Search users
  func searchUsersWithLogins(logins: [String], completion: QMAPIUsersUserCompletion?) {
    self.usersService.searchUsersWithLogins(logins).continueWithBlock { (task: BFTask) -> AnyObject? in
      let users = task.result as? [QBUUser] ?? []
      let error = task.error
      
      completion?(users: users, error: error)
      return nil
    }
    
  }
  
  
  func searchUsersWithPhoneNumbers(phoneNumbers: [String], completion: QMAPIUsersUserCompletion?) {
    var users = [QBUUser]()
    var error: NSError?
    
    let packLength = 100
    let packCount = Int(phoneNumbers.count / packLength) + (phoneNumbers.count % packLength == 0 ? 0 : 1)
    
    let semaphore = dispatch_semaphore_create(0)
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { 
      for i in 0..<packCount {
        let rangeStart = i * packLength
        let rangeEnd = min((i + 1) * packLength, phoneNumbers.count)
        let pack = Array( phoneNumbers[rangeStart..<rangeEnd] )
        
        self.usersService.searchUsersWithPhoneNumbers(pack).continueWithBlock({ (task: BFTask) -> AnyObject? in
          error = task.error
          users += (task.result as? [QBUUser]) ?? []
          
          dispatch_semaphore_signal(semaphore)
          
          return nil
        })
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        if let _ = error { break }
        
      }
      
      dispatch_async(dispatch_get_main_queue(), { 
        completion?(users: users, error: error)
      })
    }
    
    
  }
  
  
  func filterContacts(users: [QBUUser]) -> [QBUUser] {
    let contacts = self.friends
    return users.filter { (user: QBUUser) -> Bool in
      return contacts.reduce(true, combine: { $0 && $1.ID != user.ID })
    }
  }
  
  
  //  MARK: -
  //  MARK: contacts
  func importContacts(completion: QMAPIAuthSimpleCompletion?) {
    CCLog.log("\(#file) \(#function)")
    
    QMAPI.instance().importFriendsFromAddressBook { (users, error) -> () in
      CCLog.log("\(#file) \(#function)  error: \(error)")
      
      let filtered = self.filterContacts(users)
      for user in filtered { self.addUserToContactList(user, completion: nil) }
      
      dispatch_async(dispatch_get_main_queue(), { () -> Void in completion?(error) })
      
    }
    
  }
  
  
  func importFriendsFromAddressBook(completion: QMAPIUsersUserCompletion?) {
    SwiftAddressBook.requestAccessWithCompletion { (success: Bool, error: CFError?) -> Void in
      if success {
        let contactsPhones = swiftAddressBook.allPeople?.reduce([String](), combine: { $0 + ($1.phoneNumbers?.map({ $0.value }) ?? []) }) ?? []
        self.searchUsersWithPhoneNumbers(contactsPhones.map({ cleanPhone($0) }), completion: completion)  }
      
      else {
        completion?(users: [], error: error as NSError?) }
      
    }
    
  }
  
  
  func addUserToContactList(user: QBUUser, completion: QMAPIUsersSimpleCompletion?) {
    self.contactListService.addUserToContactListRequest(user) { (success: Bool) -> Void in
//      self.createPrivateChatDialogIfNeeded(user, completion: { (chatDialog, error) -> () in
//        self.sendContactRequestSendNptification(user, completion: { (notification, error) -> () in
//          completion?(success: success)
//        })
//      })
      completion?(success: success)
    }
    
  }
  
  
  func removeUserFromContactList(user: QBUUser, completion: QMAPIUsersSimpleCompletion?) {
    self.contactListService.removeUserFromContactListWithUserID(user.ID) { (success: Bool) -> Void in
      guard let dialog = self.chatService.dialogsMemoryStorage.privateChatDialogWithOpponentID(user.ID) else { completion?(success: success); return }
      self.deleteChatDialog(dialog, completion: { (error) -> () in
        completion?(success: error == nil)
      })
    }
    
  }
  
  
  func contactItemWithUserID(userID: UInt) -> QBContactListItem {
    return self.contactListService.contactListMemoryStorage.contactListItemWithUserID(userID) ?? QBContactListItem()
  }
  
  
  var friends: [QBUUser] {
    guard let ids = self.contactListService.contactListMemoryStorage.userIDsFromContactList() as? [UInt] else { return [] }
    let allFriends = self.usersWithIds(ids)
    
    if ids.count != allFriends.count { self.update() }
    
    return allFriends
  }
  
  
  var idsOfContactsOnly: [UInt] {
    var IDs = [UInt]()
    
    let contactItems = QBChat.instance().contactList?.contacts ?? []
    let pendingContactItems = QBChat.instance().contactList?.pendingApproval ?? []
    
    IDs = IDs + contactItems.reduce([UInt](), combine: { $0 + [$1.userID] })
    IDs = IDs + pendingContactItems.reduce([UInt](), combine: { $0 + [$1.userID] })
    
    return IDs
  }
  
  
  var contactsOnly: [QBUUser] {
    let IDs = self.idsOfContactsOnly
    let contacts = self.usersWithIds(IDs)
    return contacts
  }
  
  
  func safeFriends(completion: QMAPIUsersUserCompletion?) {
    let ids = self.contactListService.contactListMemoryStorage.userIDsFromContactList() as? [UInt] ?? []
    self.safeUsersWithIDs(ids, completion: completion)
  }
  
  
  //  Get contact status (online/offline)
  func isOnlineUserWithID(userID: UInt) -> Bool {
    return contactItemWithUserID(userID).online
  }
  
  
}



//  MARK: -
//  MARK: Privacy list
extension QMAPI: QBChatDelegate {
  
  
  func setupPrivacyList() {
    QBChat.instance().addDelegate(self)
    QBChat.instance().retrievePrivacyListWithName(kDefaultPrivacyList)
//    QBChat.instance().setDefaultPrivacyListWithName(kDefaultPrivacyList)
//    QBChat.instance().setActivePrivacyListWithName(kDefaultPrivacyList)
  }
  
  
  func blockUser(userID: UInt) {
    self.editBlockList(userID, block: true)
  }
  
  
  func unblockUser(userID: UInt) {
    self.editBlockList(userID, block: false)
  }
  
  
  func isUserInBlockList(userID: UInt) -> Bool {
    guard let list = self.privacyList else { return false }
    NSLog("\(list)")
    for item in list.items { if item.type == USER_ID && item.valueForType == userID && item.action == DENY { return true } }
    return false
  }
  
  
  private func editBlockList(userID: UInt, block: Bool) {
    self.removeItemFromBlockList(userID)
    let item = QBPrivacyItem(type: USER_ID, valueForType: userID, action: block ? DENY : ALLOW)
    
    if let privateList = self.privacyList { privateList.addObject(item) }
    else { self.privacyList = QBPrivacyList(name: kDefaultPrivacyList, items: [item]) }
    
    guard let list = self.privacyList else { return }
//    list.addObject(item)
    
    QBChat.instance().setPrivacyList(list)
//    self.updatePrivacyList()
  }
  
  
  private func removeItemFromBlockList(userID: UInt) {
    guard let list = self.privacyList else { return }
    for item in list.items { if item.type == USER_ID && item.valueForType == userID { list.items.removeObject(item); return } }
  }
  
  
  func updatePrivacyList() {
    QBChat.instance().addDelegate(self)
    QBChat.instance().retrievePrivacyListWithName(kDefaultPrivacyList)
  }
  
  
  func chatDidReceivePrivacyList(privacyList: QBPrivacyList) {
    self.privacyList = privacyList
    QBChat.instance().setPrivacyList(privacyList)
  }
  
  
  func chatDidNotReceivePrivacyListWithName(name: String, error: AnyObject?) {
    NSLog("ERR")
  }
  
  
  func chatDidSetPrivacyListWithName(name: String) {
    QBChat.instance().setDefaultPrivacyListWithName(name)
    QBChat.instance().setActivePrivacyListWithName(name)
  }
  
  
  func chatDidNotSetPrivacyListWithName(name: String, error: AnyObject?) {
    NSLog("ERR")
  }
  
  
  func chatDidSetActivePrivacyListWithName(name: String) {
    NSLog("SUC")
  }
  
  
  func chatDidNotSetActivePrivacyListWithName(name: String, error: AnyObject?) {
    NSLog("ERR")
  }
  
  
  
  //  MARK: -
  //  MARK: New User
  func chatDidReceiveContactAddRequestFromUser(userID: UInt) {
    if (self.contactListService.contactListMemoryStorage.userIDsFromContactList() as? [UInt])?.contains(userID) == false {
      self.safeUser(userID, completion: { (user, error) in
        guard let user = user else { return }
        self.addUserToContactList(user, completion: nil)
      })
    }
    self.contactListService.acceptContactRequest(userID, completion: nil)
    QBChat.instance().confirmAddContactRequest(userID, completion: { (error: NSError?) in
      NSLog("CONFIRMITION ERROR: \(error)")
    })
  }
  
  
}



//  Utilites
func cleanPhone(phone: String) -> String {
  return phone.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
}